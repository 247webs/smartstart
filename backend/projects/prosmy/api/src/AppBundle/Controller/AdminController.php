<?php
namespace AppBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminController extends Controller{
	
	public function adminAction(Request $request){
		$adminEmail = $request->request->get('email');
		$password = $request->request->get('password');
		
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users where users.email = " ."'" .$adminEmail ."'" ." And users.password = " ."'" .$password ."'";
		// $RAW_QUERY = "SELECT * FROM users where users.email = '$adminEmail'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		if(!empty($result)){
			$res  = $result['0'] ? $result['0'] : $result;
			$email = $res['email'];
			$role = $res['role'];
			$id = $res['id'];
			$db_password = $res['password'];
			if($db_password == $password){
				
			$data = ['code' => 200, 'id' => $id, 'message' => 'success'];
			return new JsonResponse($data);
			}else{
				$userData = array('status'=>'400','message'=>'passNotMatch');
				return new JsonResponse($userData);
			}
		}else{
			$data = ['code' => 400, 'message' => 'error'];
			return new JsonResponse($data);			
		}
        return $this->render('admin/admin.html.twig');
	}
	public function change_statusAction(Request $request){
		$userID = $request->request->get('hiddenid');
		$hiddenStatus = $request->request->get('hiddenStatus');
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users where users.id = '$userID'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		if(!empty($result)){
			$res  = $result['0'] ? $result['0'] : $result;
			$status = $res['status'];
			if($status == '1'){
				$em = $this->getDoctrine()->getManager();
				$RAW_QUERY ="UPDATE users SET `status`='0' WHERE `id` = '$userID'";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
				$data = array('status'=>'200','message'=>'success');
				return new JsonResponse($data);
			}else if($status == '0'){
				$em = $this->getDoctrine()->getManager();
				$RAW_QUERY ="UPDATE users SET `status`='1' WHERE `id` = '$userID'";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
				$data = array('status'=>'200','message'=>'success');
				return new JsonResponse($data);
			}
		}else{
			$data = ['code' => 400, 'message' => 'error'];
			return new JsonResponse($data);
		}
	}
	public function usersAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users where role = '1'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = (array)$statement->fetchAll();
		$data = array('status'=>'200','message'=>'success', 'data'=> $result);
		return new JsonResponse($data);
	}

	public function cms_managementAction(Request $request){
		// print_r($_POST);die;
		$userID = $request->request->get('hidden_id');
		$user_id = $request->request->get('hiddenid');
		$page_name = $request->request->get('page_name');
		$html_contanct = $request->request->get('page_contant');
		$page_contant = stripslashes($html_contanct);
		$em = $this->getDoctrine()->getManager();
		if(!empty($page_name)){
			$RAW_QUERY = "SELECT * FROM page_content_cms WHERE page_name = '$page_name'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$result = $statement->fetchAll();
			if(empty($result)) {
				$em = $this->getDoctrine()->getManager();	
				$RAW_QUERY = "INSERT INTO `page_content_cms`(`page_name`, `page_content`) VALUES ('$page_name','$page_contant')";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
				$data = ['code' => 200, 'message' => 'success'];
				return new JsonResponse($data);
			}else{
				$data = ['code' => 400, 'message' => 'unique'];
				return new JsonResponse($data);			
			}
		}else if($userID){
			$page_name = $request->request->get('pageName');
			$page_content = $request->request->get('pageContent');
			
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY ="UPDATE page_content_cms SET `page_name`='$page_name', `page_content`='$page_content' WHERE `id` = '$userID'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$data = array('status'=>'200','message'=>'success');
			return new JsonResponse($data);
		}else if($user_id){
			
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY = "DELETE FROM page_content_cms WHERE id = '$user_id'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$data = array('status'=>'200','message'=>'success');
			return new JsonResponse($data);
		}
		
	}
	/**UPDATE FUNCTION**/
	public function updateCmsManagementAction(Request $request){
		$userID = $request->request->get('hidden_id');
		$page_name = $request->request->get('page_name');
		$page_content = $request->request->get('page_contant');
		
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY ="UPDATE page_content_cms SET `page_name`='$page_name', `page_content`='$page_content' WHERE `id` = '$userID'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$data = array('status'=>'200','message'=>'success');
		return new JsonResponse($data);
	}
	/**DELETE PAGES CMS MANAGEMENT FUNCTION STRAT**/
	public function delete_cmsAction(Request $request){
		$user_id	= $request->request->get('hiddenid');
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "DELETE FROM page_content_cms WHERE id = '$user_id'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$data = array('status'=>'200','message'=>'success');
		return new JsonResponse($data);
	}
	/**DELETE PAGES CMS MANAGEMENT FUNCTION END**/
	
	public function pagesAction(Request $request){
		$page_name = $request->request->get('content');
		$id = $request->request->get('id');
		
		if(!empty($id)){
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY = "SELECT * FROM page_content_cms WHERE id = '$id'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$result = $statement->fetchAll();
			if(!empty($result)){
				
				$data = array('status'=>'200','message'=>'success', 'data'=> $result);
				return new JsonResponse($data);
			}else{
				$data = ['code' => 400, 'message' => 'error'];
				return new JsonResponse($data);			
			}
		}else{
				if(!empty($page_name)){
				$em = $this->getDoctrine()->getManager();
				$RAW_QUERY = "SELECT * FROM page_content_cms WHERE page_name = '$page_name'";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
				$result = $statement->fetchAll();
				if(!empty($result)){
					$res  = $result['0'] ? $result['0'] : $result;
					$page_content = $res['page_content'];
					
					$data = array('status'=>'200','message'=>'success', 'page_content'=> $page_content);
					return new JsonResponse($data);
				}else{
					$data = ['code' => 400, 'message' => 'error'];
					return new JsonResponse($data);			
				}
			}else{
				$em = $this->getDoctrine()->getManager();
				$RAW_QUERY = "SELECT * FROM page_content_cms";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
				$result = $statement->fetchAll();
				$data = array('status'=>'200','message'=>'success', 'data'=> $result);
				return new JsonResponse($data);
			}
		}
	
	}
	public function create_templateAction(Request $request){
		$sign_up = $request->request->get('sign_up');
		$forgot_password = $request->request->get('forgot_password');
		$change_password = $request->request->get('change_password');
		
		$sign_up_content = $request->request->get('sign_up_content');
		$forgot_password_content = $request->request->get('forgot_password_content');
		$change_password_content = $request->request->get('change_password_content');
		
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM email_templates";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		
		if (empty($result)) {
			$em = $this->getDoctrine()->getManager();	
			$RAW_QUERY = "INSERT INTO `email_templates`(`template_title`, `content`) VALUES ('$sign_up','$sign_up_content')";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
		
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY = "INSERT INTO `email_templates`(`template_title`, `content`) VALUES ('$forgot_password','$forgot_password_content')";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY = "INSERT INTO `email_templates`(`template_title`, `content`) VALUES ('$change_password','$change_password_content')";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
		
			$data = ['code' => 200, 'message' => 'success'];
			return new JsonResponse($data);
		}else{
			$em = $this->getDoctrine()->getManager();	
			$RAW_QUERY ="UPDATE email_templates SET content='$sign_up_content' WHERE id='1'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
		
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY ="UPDATE email_templates SET content='$forgot_password_content' WHERE id='2'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY ="UPDATE email_templates SET content='$change_password_content' WHERE id='3'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
		
			$data = ['code' => 200, 'message' => 'update success'];
			return new JsonResponse($data);
		}
	}
	
	public function email_templatesAction(Request $request){
		// print_r($_POST);die;
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM email_templates";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		$data = array('status'=>'200','message'=>'success', 'data'=> $result);
		return new JsonResponse($data);
	}
	
	public function create_menuAction(Request $request){
		$userID = $request->request->get('hidden_id');
		$menu_name	= $request->request->get('menu_name');
		$menu_type	= $request->request->get('menu_type');
		$menu_url	= $request->request->get('menu_url');
		if(!empty($menu_name)){
			$em = $this->getDoctrine()->getManager();	
			$RAW_QUERY = "INSERT INTO menu_managements(menu_name, menu_type, menu_url) VALUES ('$menu_name','$menu_type','$menu_url')";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$data = ['code' => 200, 'message' => 'success'];
			return new JsonResponse($data);
		}else if(!empty($userID)){
			$menuName = $request->request->get('menuName');
			$menuType = $request->request->get('menuType');
			$menuUrl = $request->request->get('menuUrl');
			
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY ="UPDATE menu_managements SET `menu_name`='$menuName', `menu_type`='$menuType', `menu_url`='$menuUrl' WHERE `id` = '$userID'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$data = array('status'=>'200','message'=>'success');
			return new JsonResponse($data);
		}
	}
	public function menusAction(Request $request){
		$id	= $request->request->get('id');
		if(!empty($id)){
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY = "SELECT * FROM menu_managements where id = '$id'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$result = $statement->fetchAll();
			$data = array('status'=>'200','message'=>'success', 'data'=> $result);
			return new JsonResponse($data);
		}else{
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY = "SELECT * FROM menu_managements";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$result = $statement->fetchAll();
			$data = array('status'=>'200','message'=>'success', 'data'=> $result);
			return new JsonResponse($data);
		}
	}
	/**UPDATE MENU**/
	public function update_menusAction(Request $request){
		
		$userID = $request->request->get('hidden_id');
		$menu_name = $request->request->get('menu_name');
		$menu_type = $request->request->get('menu_type');
		$menu_url = $request->request->get('menu_url');
		
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY ="UPDATE email_templates SET `menu_name`='$menu_name', `menu_type`='$menu_type', `menu_url`='$menu_url' WHERE `id` = '$userID'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$data = array('status'=>'200','message'=>'success');
		return new JsonResponse($data);
	}
	
	public function delete_menuAction(Request $request){
		$user_id	= $request->request->get('hiddenid');
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "DELETE FROM menu_managements WHERE id = '$user_id'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$data = array('status'=>'200','message'=>'success');
		return new JsonResponse($data);
	}
}