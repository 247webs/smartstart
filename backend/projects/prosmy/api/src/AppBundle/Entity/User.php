<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="This email address is already in use")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=40)
     */
    protected $username;
	
	
	/**
     * @ORM\Column(type="string", length=50)
     */
	 
	  protected $dateOfBirth;
	
	
	/**
     * @ORM\Column(type="string", length=40)
     */
	protected $phone;
	

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $role;

    /**
     * @Assert\Length(max=4096)
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $password;

	
	/**
	*@ORM\Column(name="gender", type="array")
	*@ORM\Column(type="string", length=50)
	*/
	protected $gender;
	
	/**
	*@ORM\Column(name="country", type="array")
	*@ORM\Column(type="string", length=50)
	*/
	protected $country;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	protected $image;
	/**
     * @ORM\Column(type="string", length=100)
     */
	protected $tockenname;

	
	public function settockenName($tockenname){
        $this->tockenname = $tockenname;
    }
	
	public function gettockenName(){
        return $this->tockenname;
    }
	
	public function setImage($image){
        $this->image = $image;
    }

    public function getImage(){
        return $this->image;
    }
	public function setDateOfBirth($dateOfBirth){
        $this->dateOfBirth = $dateOfBirth;
    }

    public function getDateOfBirth(){
        return $this->dateOfBirth;
    }
	
	
	public function setPhone($phone){
        $this->phone = $phone;
    }
	
	public function getPhone(){
        return $this->phone;
    }
	
    public function eraseCredentials(){
        return null;
    }

    public function getRole(){
        return $this->role;
    }
	
    public function setRole($role = null){
        $this->role = $role;
    }

    public function getRoles(){
        return [$this->getRole()];
    }

    public function getId(){
        return $this->id;
    }

    public function setusername($username){
        $this->username = $username;
    }

    public function getusername() {
        return $this->username;
    }

    public function getUserusername(){
        return $this->email;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function getPlainPassword(){
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword){
        $this->plainPassword = $plainPassword;
    }
	
	public function getGender(){
        return $this->gender;
    }
	
	public function setGender($gender){		
        $this->gender = $gender;	
    }
	
	public function getCountry(){
        return $this->country;
    }
	
	public function setCountry($country){		
        $this->country = $country;	
    }
	
    public function getSalt(){
        return null;
    }
}