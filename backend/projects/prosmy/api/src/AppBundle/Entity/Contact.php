<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="contact")
 * @ORM\Entity
 */
class Contact
{
    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=40)
     */
    protected $name;
	
	/**
     * @ORM\Column(type="string", length=40)
     */
	protected $phone;
	

	/**
     * @ORM\Column(type="string", length=50)
     */
	protected $subject;
	
	/**
     * @ORM\Column(type="string", length=150)
     */
	protected $editor;

	
	public function setPhone($phone){
        $this->phone = $phone;
    }
	
	public function getPhone(){
        return $this->phone;
    }
	
    public function eraseCredentials(){
        return null;
    }

    public function getId(){
        return $this->id;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }


    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

   public function getSubject(){
        return $this->email;
    }

    public function setSubject($subject){
        $this->subject = $subject;
    }

   public function getEditor(){
        return $this->editor;
    }

    public function setEditor($editor){
        $this->editor = $editor;
    }

   
	
    public function getSalt(){
        return null;
    }
}