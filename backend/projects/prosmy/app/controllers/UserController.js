angular.module('app').constant('API_URL', "http://map-art.upworkdevelopers.com/ourproduct/backend/projects/prosmy/api/web/app_dev.php/");
/* REGISTER CONTROLLER */
app.controller('registerCtrl', ['$scope', '$location', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL',  function($scope, $location, $http, $timeout, userGetService, userPostService, API_URL) { 
	$scope.formData = {};
	$scope.passwordError = false;
	$scope.allFieldError = false;
	$scope.errorEmailExist = false;
	$scope.register_button = function(){
		if($scope.registerForm.$valid){
			if($scope.formData.password == $scope.formData.confirm_password){
				userPostService.postData(API_URL+'user/signup', $scope.formData).then(function (responseData) {
					if(responseData.message == 'success'){
						$(location).attr('href', '#/thankYou');
					}else if(responseData.message == 'emailUnic'){
						
						$timeout(function () {
							$scope.errorEmailExist = true;
						}, 0);
						$timeout(function () {
							$scope.errorEmailExist = false;
						}, 5000);
					}
				},function(responseData) {
					console.log('albums retrieval failed.');
				});	
			}else{
				$timeout(function () {
					$scope.passwordError = true;
				}, 0);
				$timeout(function () {
					$scope.passwordError = false;
				}, 4000);
			}
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
}]);

app.controller('thankYouController', ['$scope', '$location', '$http', '$timeout', function($scope, $location, $http, $timeout, API_URL) { 
}]);

/*Login controller*/
app.controller('loginController', ['$scope', '$location', '$http', '$state', '$localStorage', '$window', '$rootScope', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $http, $state, $localStorage, $window, $rootScope, $timeout, userGetService, userPostService, API_URL) {
	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.errorMsg2 = false;
	$scope.passwordError = false;
	$scope.allFieldError = false;
	$scope.adminLoginSuccess = false;
	$scope.emailVerifiedError = false;
	$scope.tokenError = false;
	$scope.formData.token = $location.search().token;
	
	$scope.forgotlink = false;
	
	$scope.login_button = function(){
		if($scope.loginForm.$valid){
			userPostService.postData(API_URL+'user/login', $scope.formData).then(function (responseData) {
				if(responseData.message == 'success'){
					localStorage.setItem("id", responseData.id);
					$scope.adminLoginSuccess = true;
					$timeout(function () {
						$(location).attr('href', '#/dashboard'); 
					}, 4000);
					
				}else if(responseData.message == 'failed'){
					$timeout(function () {
						$scope.errorMsg2 = true;
					}, 0);
					$timeout(function () {
						$scope.errorMsg2 = false;
					}, 4000);
					$(location).attr('href', '#/login');
				}else if(responseData.message ==  'emailNotMatch'){
					$timeout(function () {
						$scope.errorMsg = true;
					}, 0);
					$timeout(function () {
						$scope.errorMsg = false;
					}, 4000);
				}else if(responseData.message ==  'passNotMatch'){
					$timeout(function () {
						$scope.passwordError = true;
					}, 0);
					$timeout(function () {
						$scope.passwordError = false;
					}, 4000);
				}else if(responseData.message ==  'emailVerified'){
					$timeout(function () {
						$scope.emailVerifiedError = true;
					}, 0);
					$timeout(function () {
						$scope.emailVerifiedError = false;
					}, 6000);
				}else if(responseData.message ==  'tokenError'){
					$timeout(function () {
						$scope.tokenError = true;
					}, 0);
					$timeout(function () {
						$scope.tokenError = false;
					}, 6000);
				}
			},function(responseData) {
				console.log('albums retrieval failed.');
			});	
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
}]);

/* forgotPasswordCtrl */
app.controller('forgotPasswordCtrl', ['$scope', '$location', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userGetService, userPostService, API_URL) { 	
	$scope.allFieldError = false;
	$scope.errorMsg2 = false;
	$scope.errorMsg3 = false;
	$scope.emailVerifiedError = false;
	$scope.forgotPassword = function(){
		if($scope.forgotPasswordForm.$valid == true){
			userPostService.postData(API_URL+'user/forgot_password', $scope.formData).then(function (responseData) {
				if(responseData.message == 'success'){
					 $timeout(function () {
						$scope.errorMsg3 = true;
					  }, 0);
					  $timeout(function () {
						$scope.errorMsg3 = false;
					  }, 5000);
				}else if(responseData.message == 'failed'){
					 $timeout(function () {
						$scope.errorMsg2 = true;
					  }, 0);
					$timeout(function () {
						$scope.errorMsg2 = false;
					}, 4000);
				}else if(responseData.message == 'emailVerified'){
					$timeout(function () {
						$scope.emailVerifiedError = true;
					}, 0);
					$timeout(function () {
						$scope.emailVerifiedError = false;
					}, 6000);
				}
			},function(responseData) {
				console.log('albums retrieval failed.');
			});	
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
				  }, 0);
			$timeout(function () {
				$scope.allFieldError = false;
				  }, 6000);
		}	
	}
	
}]);

/*resetPasswordCtrl*/
app.controller('resetPasswordCtrl', ['$scope', '$location', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userGetService, userPostService, API_URL) { 

	$scope.formData = {};
	$scope.allFieldError = false;
	$scope.errorMsg3 = false;
	$scope.errorMsg2 = false;
	$scope.passwordError = false;
	$scope.emailVerifiedError = false;
	$scope.formData.token = $location.search().token;
	$scope.formData.email = $location.search().email;
	$scope.reset_button = function(){
		if($scope.resetForm.$valid == true){
			if($scope.formData.password == $scope.formData.confirm_password){
				userPostService.postData(API_URL+'user/password_reset', $scope.formData).then(function (responseData) {
					if(responseData.message == 'success'){
						$timeout(function () {
							$scope.errorMsg3 = true;
						}, 0);
						$timeout(function () {
							$scope.errorMsg3 = false;
							$(location).attr('href', '#/login');
						}, 6000); 
					}else if(responseData.message == 'failed'){
						$timeout(function () {
							$scope.errorMsg2 = true;
						}, 0);
						$timeout(function () {
							$scope.errorMsg2 = false;
							$(location).attr('href', '#/forgotpassword');
						}, 6000);
					}else if(responseData.message == 'emailVerified'){
						$timeout(function () {
							$scope.emailVerifiedError = true;
						}, 0);
						$timeout(function () {
							$scope.emailVerifiedError = false;
						}, 6000);
					}
				},function(responseData) {
					console.log('albums retrieval failed.');
				});	
			}else{
				$timeout(function () {
				$scope.passwordError = true;
				  }, 0);
				$timeout(function () {
				$scope.passwordError = false;
				  }, 4000);		
			}
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
}]);

/*dAHBOARD CONTROLLER*/

/* CHANGE PASSWORD CONTROLLER */
app.controller('changePasswordCtrl', ['$scope', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $timeout, userGetService, userPostService, API_URL) {
	
	$scope.formData = {};
	$scope.formData.id = localStorage.getItem("id");
	$scope.allFieldError = false;	
	$scope.ErrorMsg2 = false;
	$scope.ErrorMsg3 = false;
	$scope.passwordError = false;
	/*---------status logout---------*/
	$scope.loadData = function(){
		var id = localStorage.getItem("id");
		userPostService.postData(API_URL+'user/getStatus', {'id' : id}).then(function (responseData) {
			console.log(responseData);
			if(responseData.data == '0'){
				localStorage.clear();
				console.log('albums retrieval failed.');
				$timeout(function () {
					$scope.statusMsg = true;
				}, 0);
				$timeout(function () {
				}, 4000);		
			}
		},function(responseData) {
			console.log('albums retrieval failed.');
		});			
	}
	
	$scope.change_password = function(){
		
		if($scope.changPasswordForm.$valid){
			if( $scope.formData.new_password == $scope.formData.confirm_password){
				userPostService.postData(API_URL+'dashboard/change_password', $scope.formData).then(function (responseData) {
					if(responseData.message == 'success'){
						$timeout(function () {
							$scope.ErrorMsg3 = true;
							// $(location).attr('href', '#/changePassword');
						}, 0);
						$timeout(function () {
							$scope.ErrorMsg3 = false;
							$(location).attr('href', '#/dashboard');
						}, 5000);	  
					}else if(responseData.message == 'failed'){
						$timeout(function () {
							$scope.ErrorMsg2 = true;
						}, 0);
						$timeout(function () {
							$scope.ErrorMsg2 = false;
						}, 4000);
					}
				},function(responseData) {
					console.log('albums retrieval failed.');
				});	
			}else{
				$timeout(function () {
					$scope.passwordError = true;
				}, 0);
				$timeout(function () {
					$scope.passwordError = false;
				}, 4000);
			}
			
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}	
	}
}]);
/*VIEW PROFILE CONTROLLER*/
app.controller('profile', ['$scope', '$http', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, userGetService, userPostService, API_URL) {
	$scope.profileData = {};
	var id = localStorage.getItem("id");
	userPostService.postData(API_URL+'dashboard/user_details', {'id':id}).then(function (userData){
		$scope.profileData.name = userData.data[0].name;
		$scope.profileData.email = userData.data[0].email;
		$scope.profileData.phone = userData.data[0].phone;
		$scope.profileData.date_of_birth = userData.data[0].date_of_birth;
		$scope.profileData.gender = userData.data[0].gender;
		$scope.profileData.image = userData.data[0].image;
	},function(userData) {
		console.log('albums retrieval failed.');
	});
	
}]);


/*UPDATE PROFILE CONTROLLER*/
app.controller('updateProfile', ['$scope', '$timeout', '$http', 'userGetService', 'userPostService', 'API_URL', function($scope, $timeout, $http, userGetService, userPostService, API_URL) { 
	$scope.statusMsg = false;
	$scope.allFieldError = false;
	/* Set default value */
	$scope.profileData = {};
	var id = localStorage.getItem("id");
	/* get DB data for edit */

	userPostService.postData(API_URL+'dashboard/user_details', {'id':id}).then(function (response) {
		if(response.message == 'success'){
			$scope.profileData.name = response.data[0].name;
			$scope.profileData.email = response.data[0].email;
			$scope.profileData.phone = response.data[0].phone;
			$scope.profileData.date_of_birth = response.data[0].date_of_birth;
			$scope.profileData.gender = response.data[0].gender;
			$scope.profileData.image = response.data[0].image;
			
		}
	},function(response) {
		console.log('albums retrieval failed.');
	});
	
	/* Update the profile */
	$scope.successUpdateProfile = false;
	$scope.loder = false;
	$scope.updateProfile = function(){
		if($scope.editProfileForm.$valid == true){
			$scope.profileData.id = localStorage.getItem("id");
			userPostService.postData(API_URL+'dashboard/update_user_details', $scope.profileData).then(function (response) {
				if(response.message == 'success'){
					$timeout(function () {
						
						$scope.successUpdateProfile = true;
						$scope.loder = true;
					}, 0);
					$timeout(function () {
						$scope.successUpdateProfile = false;
						$scope.loder = false;
						$(location).attr('href', '#/profile')
					}, 4000);	
				}

			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}		
	}
	

}]);
app.controller('dashboardCtrl', ['$scope', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $timeout, userGetService, userPostService, API_URL) { 
	$scope.statusMsg = false;
	/*---------GET DATA FOR DASHBOARD---------*/
	
	$scope.loadData = function(){
		var id = localStorage.getItem("id");
		userPostService.postData(API_URL+'user/getStatus', {'id' : id}).then(function (responseData) {
			$scope.name = responseData.name; 
		},function(responseData) {
			console.log('albums retrieval failed.');
		});			
	}
}]);

/* Loguout controller*/
app.controller('logout', ['$scope', '$http', '$location', '$timeout', function($scope, $http, $location, $timeout) {
	$scope.logout = function(){
		localStorage.clear();
		$(location).attr('href', '#/home');
	}
	
}]);
/* Layout Controller*/
app.controller('layout', ['$scope', '$http', '$location', '$window', '$timeout', 'userGetService', 'API_URL', function($scope, $http, $location, $timeout, $window, userGetService, API_URL) {

	userGetService.getData(API_URL+'admin/menus').then(function (response) {
		$scope.res  = response.data;
	},function(response) {
		console.log('albums retrieval failed.');
	});
	$scope.reloadRoute = function() {
		location.reload(); 
	}
	$scope.loadData = function(){
		var id = localStorage.getItem("id");
		userPostService.postData(API_URL+'user/getStatus', {'id' : id}).then(function (responseData) {
			if(responseData.data == '0'){
				localStorage.clear();
				console.log('albums retrieval failed.');
				$timeout(function () {
					$scope.statusMsg = true;
				}, 0);
				$timeout(function () {
					$(location).attr('href', '#/login');
				}, 4000);		
			}
		},function(responseData) {
			console.log('albums retrieval failed.');
		});			
	}
}]);

/* Layout Controller*/
app.controller('update_dbCtrl', ['$scope', '$http', '$location', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $location, $timeout, userGetService, userPostService, API_URL) {
	$scope.allFieldError = false;
	$scope.successMsg = false;
	$timeout(function () {
		$scope.bdError = true;
	}, 0);
	$timeout(function () {
		$scope.bdError = false;
	}, 5000);
	$scope.update = function(){
		 if($scope.dbUpdateForm.$valid){
			userPostService.postData(API_URL+'user/update_db', $scope.formData).then(function (responseData) {
			if(responseData.message == 'success'){
				$timeout(function () {
					$scope.successMsg = true;
				}, 0);
				$timeout(function () {
					$scope.successMsg = false;
					$(location).attr('href', '#/login');
				}, 5000);		
			}
		},function(responseData) {
			console.log('albums retrieval failed.');
		});
		}else{
			$timeout(function () {
					$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 5000);	
		}	
	}
	
}]);
