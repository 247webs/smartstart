angular.module('app').constant('API_URL', "http://upworkdevelopers.com/test/prosmy/api/web/");
/* LOGIN CONTROLLER */
app.controller('adminCtrl', ['$scope', '$location', '$http', '$timeout','postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) { 
	$scope.formData = {};
	$scope.formData.email = 'admin@gmail.com';
	$scope.formData.password = 'Any@12345';
	$scope.allFieldError = false;
	$scope.errorMsg = false;
	$scope.flashMsg = false;
	$scope.passwordError = false;
	$scope.adminLoginError = false;
	$scope.adminLoginSuccess = false;
	$scope.login_button = function(){
		if($scope.loginForm.$valid == true){
			postService.postData(API_URL+'admin', $scope.formData).then(function (responseData) {
				if(responseData.message == 'success'){
					$timeout(function () {
						$scope.adminLoginSuccess = true;
					}, 0);
					$timeout(function () {
						$location.url("/admin/dashboard");
					}, 2000);
				}else if(responseData.message =='error'){
					$timeout(function () {
						$scope.errorMsg = true;
					}, 0);
					$timeout(function () {
						$scope.errorMsg = false;
					}, 2000);
				}else if(responseData.message ==  'passNotMatch'){
					$timeout(function () {
						$scope.passwordError = true;
					}, 0);
					$timeout(function () {
						$scope.passwordError = false;
					}, 4000);
				}
				
			},function(responseData) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 2000);
		}
	}
	/*---ADMIN DASHBOARD FUNCTION---*/
	$scope.Status = function(id,status,$index){ 
		$scope.formData.hiddenid = id;	
	}
	$scope.toggleNo = function(){
		if($scope.formData.hiddenStatus == '0'){
			getService.getData(API_URL+'admin/users').then(function (userData) {
				$scope.res  = userData.data;
			},function(responseData) {
				console.log('albums retrieval failed.');
			});
		}else{
			getService.getData(API_URL+'admin/users').then(function (userData) {
				$scope.res  = userData.data;
			},function(responseData) {
				console.log('albums retrieval failed.');
			});
		}
	}
	$scope.changedStatus = function(){
		postService.postData(API_URL+'admin/change_status', $scope.formData).then(function (responseData) {
			if(responseData){
				$timeout(function () {
					$scope.flashMsg = true;
				}, 0);
				$timeout(function () {
				$scope.flashMsg = false;
						  }, 4000);
				$(location).attr('href', '#/admin/dashboard')
			}
		},function(responseData) {
				console.log('albums retrieval failed.');
		});
	}
	
	/*---------GET DATA FOR ADMIN DASHBOARD---------*/
	$scope.loadData = function(){
 		getService.getData(API_URL+'admin/users').then(function (userData) {
			$scope.res  = userData.data;
		},function(responseData) {
			console.log('albums retrieval failed.');
		});
	}
}]);


/*EMAIL TEMPLATE CONTROLLER*/
app.controller('email_templateController', ['$scope', '$location', '$http', '$timeout','postService', 'getService','API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
	$scope.formData = {};
	$scope.formData.sign_up = 'Signup';
	$scope.formData.forgot_password = 'forgot password';
	$scope.formData.change_password = 'change password';
	$scope.SuccessMsg = false;
	$scope.SuccessMsg1 = false;

	$scope.email_template = function(){
		postService.postData(API_URL+'admin/create_template', $scope.formData).then(function (responseData) {
			if(responseData.message==="success"){
				$timeout(function () {
					$scope.SuccessMsg = true;
				}, 0);
				$timeout(function () {
					$scope.SuccessMsg = false;
					$(location).attr('href', '#/admin/dashboard');
				}, 3000);	  
			}else if(responseData.message==="update success"){
				$timeout(function () {
					$scope.SuccessMsg1 = true;
				}, 0);
				$timeout(function () {
					$scope.SuccessMsg1 = false;
					$(location).attr('href', '#/admin/dashboard');
				}, 3000);  	
			}
		},function(responseData) {
			console.log('albums retrieval failed.');
		});
	}

	$scope.loadEmailTemplate  = function(){
		
		getService.getData(API_URL+'admin/email_templates').then(function (userData) {
			if(userData.data.length != 0){	
				$scope.formData.sign_up = userData.data[0].template_title;
				$scope.formData.sign_up_content = userData.data[0].content;
				
				$scope.formData.forgot_password = userData.data[1].template_title;
				$scope.formData.forgot_password_content = userData.data[1].content;
				
				$scope.formData.change_password = userData.data[2].template_title;
				$scope.formData.change_password_content = userData.data[2].content;
			}
		},function(responseData) {
			console.log('albums retrieval failed.');
		});
	}

}]);

/* CMS MANAGEMENT CONTROLLER */
app.controller('cms_managementCtrl', ['$scope', '$location', '$http', '$timeout', 'postService', 'API_URL', function($scope, $location, $http, $timeout, postService, API_URL) { 
	$scope.formData = {};
	$scope.flashMsg = false;
	$scope.uniquePage = false;
	$scope.allFieldError = false;
	$scope.create_page_button = function(){
		if($scope.cms_management.$valid == true){
			postService.postData(API_URL+'admin/cms_management', $scope.formData).then(function (responseData) {
				if(responseData.message == 'success'){
					$timeout(function () {
						$scope.flashMsg = true;
					}, 0);
					$timeout(function () {
						$scope.flashMsg = false;
						$(location).attr('href', '#/admin/pagelist');
					}, 3000);
				}else if(responseData.message == 'unique'){
					$timeout(function () {
						$scope.uniquePage = true;
					}, 0);
					$timeout(function () {
						$scope.uniquePage = false;
						$(location).attr('href', '#/admin/cms_management');
					}, 3000);  
				}
			},function(responseData) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 2000);
		}
	}
	
}]);
/* CMS MANAGEMENT CONTROLLER */
app.controller('pagelistCtrl', ['$scope', '$location', '$http', '$timeout','postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
$scope.loadData = function(){
	getService.getData(API_URL+'admin/pages').then(function (userData) {
		$scope.res  = userData.data;
	},function(responseData) {
		console.log('albums retrieval failed.');
	});
}
}]);
/*HTML DATA BIND*/
angular.module('app')
    .filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);
/* CMS MANAGEMENT CONTROLLER */
app.controller('staticCtrl', ['$scope', '$location', '$http', '$timeout','postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
	
	$scope.formData = {};
	// var content = $location.absUrl().split('?')[1];
	var content = $location.search().page;

	$scope.loadPageData =function(){
		postService.postData(API_URL+'admin/pages', {'content' : content}).then(function (responseData) {
			if(responseData.message == 'success'){
				$timeout(function () {
					
					$scope.myText  = responseData.page_content;
					
				}, 0);
			}
		},function(responseData) {
			console.log('albums retrieval failed.');
		});			
	}
	

}]);


/* MENU MANAGEMENT CONTROLLER */
app.controller('menu_managementCtrl', ['$scope', '$location', '$http', '$timeout', 'getService', 'postService', 'API_URL', function($scope, $location, $http, $timeout, getService, postService,API_URL) {
	$scope.formData = {};
	$scope.successCreate = false;
	$scope.allFieldError = false;
	$scope.deleteMsg = false;
	
	/*MENU DATA**/
	$scope.loadData = function(){
		getService.getData(API_URL+'admin/menus').then(function (userData) {
			$scope.res  = userData.data;
		},function(responseData) {
			console.log('albums retrieval failed.');
		});	
	}
	/*CREATE MENU FUNCTION*/
	$scope.create_menu = function(){
		if($scope.menu_management_form.$valid){
			postService.postData(API_URL+'admin/create_menu', $scope.formData).then(function (responseData) {
				if(responseData.message==="success"){
					$timeout(function () {
						$scope.successCreate = true;
					}, 0);
					$timeout(function () {
						$scope.successCreate = false;
						$(location).attr('href', '#/admin/menu_management');
					}, 3000);	  
				}
			},function(responseData) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 3000);
		}
	}
	/*DELETE FUNCTION*/
	$scope.delete = function(id,$index){
		$scope.formData.hiddenid = id;	
	}
	
	$scope.deleteYes = function(){
		postService.postData(API_URL+'admin/delete_menu', $scope.formData).then(function (responseData) {
			if(responseData.message == 'success'){
				$timeout(function () {
					$scope.deleteMsg = true;
				}, 0);
				$timeout(function () {
					$scope.deleteMsg = true;
					getService.getData(API_URL+'admin/menus').then(function (userData) {
						$scope.res  = userData.data;
					},function(responseData) {
						console.log('albums retrieval failed.');
					});
				}, 3000);
				
			}
		},function(responseData) {
				console.log('albums retrieval failed.');
		});
	}
	
}]);

