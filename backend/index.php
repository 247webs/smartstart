<?php
session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL|E_STRICT);
ini_set('memory_limit', '96M');
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');

$postdata 				= 	file_get_contents("php://input");
$request	 			=	json_decode($postdata);
if(empty($request->selected_framework)){
	$array = array('status' => 'error', 'message' => "All Filed are empty.Pls try argain");
	echo json_encode($array);

}else{
	$selected_framework 	=	addslashes($request->selected_framework);
}

if(empty($request->ftp_host) && empty($request->ftp_port) && empty($request->protocol) && empty($request->ftp_username) && empty($request->ftp_password) && empty($request->db_host) && empty($request->db_port) && empty($request->db_username) && empty($request->db_password) && empty($request->db_name) && empty($request->site_host)){
	
	$array = array('status' => 'error', 'message' => "error : You did not enter a all field.");
	echo json_encode($array);                                                                              
	
} else {
	
	$ftp_server 			=	addslashes($request->ftp_host);
	$ftp_port 				= 	addslashes($request->ftp_port);
	$ftp_username 			= 	addslashes($request->ftp_username);
	$ftp_password 			= 	addslashes($request->ftp_password);
	$protocol 				= 	addslashes($request->protocol);
	$db_host				=	addslashes($request->db_host);
	$db_username			=	addslashes($request->db_username);
	$db_password			=	addslashes($request->db_password);
	$db_name				=	addslashes($request->db_name);	
	$site_host				=	addslashes($request->site_host);	 
}

// if(!isset($_SESSION['fileCounts'])){
	// $_SESSION['fileCounts'] = 0;
// }

function updateCount($count, $frameWork){
	if(!file_exists('projects/'.$frameWork.'FileCount.json')){
		$fp = fopen('projects/'.$frameWork.'FileCount.json', 'w');
		fwrite($fp, json_encode($count));
		fclose($fp);
	}else{
		file_put_contents('projects/'.$frameWork.'FileCount.json', json_encode($count));
	}
}

/*function ftp_putAll($conn_id, $src_dir, $dst_dir) {
	//$conn_id = @ftp_connect($ftp_server);
	$d = @opendir($src_dir);
	static $fileCount = 0;
	while($file = @readdir($d)) { // do this for each file in the directory
        if ($file != "." && $file != "..") { // to prevent an infinite loop
        	++$fileCount;
        	global $selected_framework;
        	updateCount($fileCount, $selected_framework);
            if (is_dir($src_dir."/".$file)) { // do the following if it is a directory
                if (!@ftp_chdir($conn_id, $dst_dir."/".$file)) {
                    @ftp_mkdir($conn_id, $dst_dir."/".$file); // create directories that do not yet exist
                }
				ftp_putAll($conn_id, $src_dir."/".$file, $dst_dir."/".$file); // recursive part
			
            } else {
                @ftp_pasv( $conn_id, true ); 
                $upload = @ftp_put($conn_id, $dst_dir."/".$file, $src_dir."/".$file, FTP_BINARY); // put the files
				
			}
        }
    }
	return true;
}*/ 



function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (filetype($dir."/".$object) == "dir") 
					rrmdir($dir."/".$object); 
				else unlink   ($dir."/".$object);
			}
		}
		reset($objects);
		rmdir($dir);
	}
}


if(!$conn_id = @ftp_connect($ftp_server)){
	
	$array = array('status' => 'error', 'message' => "Failed! Incorrect FTP host.");
	echo json_encode($array);
    
} else if(!ftp_login($conn_id, $ftp_username, $ftp_password)) {
	
	$array = array('status' => 'error', 'message' => "Failed! Incorrect FTP details.");
	echo json_encode($array);
	
} else {

	// $contents = ftp_rawlist($conn_id, '/');
	$contents = @ftp_nlist($conn_id, '.');

	foreach (@$contents as $key => $value) {
		if(in_array('.', $contents) || in_array('..', $contents)){
			unset($contents[$key]);
		}
	}
	if(empty($contents)){ // Root directory should be empty

		$remote_file = 'projects/readme.md'; // we have to put this file/directory on client server

		$handle = 'readme.md'; // place the file/directory here
	
		if(@ftp_put($conn_id, $handle, $remote_file, FTP_BINARY)){

            if($selected_framework == "codeigniter") {
                
                $src_dir 	= 'projects/'.$selected_framework.'.zip'; // we have to put this Zip File 
                $dst_dir 	= $selected_framework.'.zip'; // place the Zip here
                
                require_once $selected_framework . "_configuration.php";
                // Uploading Zip file

                if($uploading = @ftp_put( $conn_id, $dst_dir, $src_dir, FTP_BINARY )){

                    if($uploading){
                        // we have to put this Zip file Lib 
                        $libzipcode = "pclzip.lib.php";
                        $config_ziplib = 'pclzip.lib.php';
                        $unzipLib = @ftp_put($conn_id, $config_ziplib, $libzipcode, FTP_BINARY);
                        // we have to put this CI Unzip file 
                        $constant_unzip = "CI_unzip.php";
                        $config_unzip = 'CI_unzip.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        // we have to put this CI folder file
                        $constant_unzip = "CI_folder_remove.php";
                        $config_unzip = 'CI_folder_remove.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        
                        
                        if($unzipUpload){
                            // Deploy other server Curl call on unzip folder
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."CI_unzip.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }
                            curl_close($ch);
                            
                            // Deploy other server Curl call on folder remove 
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."CI_folder_remove.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }else{
                                
                                 
                                $create_folder = time().'_'.$ftp_server;
                                @mkdir($create_folder, 0777, true);
                        
                                $database_file_path = "database.php"; // $create_folder."/database.php";
                                $config_file_path = "config.php"; // $create_folder."/config.php";
                                $constant_file_path = "constant.js"; // $create_folder."/constant.js";
                                touch($database_file_path);
                                touch($config_file_path);
                                touch($constant_file_path);
                                
                                
                                WriteDatabaseDetails($database_file_path, $db_host, $db_username, $db_password, $db_name);
                                // Putting database file on client server
                                $database_handle = '/api/application/config/database.php';
                                @ftp_put($conn_id, $database_handle, $database_file_path, FTP_BINARY);
                                
                                
                                WriteConfigDetails($config_file_path, $site_host);
                                // Putting config file on client server
                                $config_handle = '/api/application/config/config.php';
                                @ftp_put($conn_id, $config_handle, $config_file_path, FTP_BINARY);

                                WriteAppConstant($constant_file_path, $site_host);
                                // Putting config file on client server
                                $config_handle = '/app/services/constant.js';
                                @ftp_put($conn_id, $config_handle, $constant_file_path, FTP_BINARY);
                                rrmdir($create_folder);
                                
                                // All file Delete for folder for client
                                $ci_zip = $selected_framework.'.zip';
                                $zip_lib = "pclzip.lib.php";
                                $ci_zip_file = "CI_unzip.php";
                                $ci_zip_remove = "CI_folder_remove.php";
                                $ci_folder_remove = "codeigniter";
                                @ftp_delete($conn_id, $ci_zip);
                                @ftp_delete($conn_id, $zip_lib);
                                @ftp_delete($conn_id, $ci_zip_file);
                                @ftp_delete($conn_id, $ci_zip_remove);
                                @ftp_delete($conn_id, $ci_folder_remove);
                            }
                            curl_close($ch);
                            $array = array('status' => 'success', 'message' => "Your! Site deployed successfully");
                            echo json_encode($array);	
                        }
                    }else{
                        $array = array('status' => 'error', 'message' => "Failed! Please resubmit.");
                        echo json_encode($array); 
                    }
                }
                
            }else if($selected_framework == "symfony"){
                
                $src_dir 	= 'projects/'.$selected_framework.'.zip'; // we have to put this Zip File 
                $dst_dir 	= $selected_framework.'.zip'; // place the Zip here
                
                require_once $selected_framework . "_configuration.php";
                // Uploading Zip file
                if($uploading = @ftp_put( $conn_id, $dst_dir, $src_dir, FTP_BINARY )){
                    if($uploading){
                        // we have to put this Zip file Lib 
                        $libzipcode = "pclzip.lib.php";
                        $config_ziplib = 'pclzip.lib.php';
                        $unzipLib = @ftp_put($conn_id, $config_ziplib, $libzipcode, FTP_BINARY);
                        // we have to put this CI Unzip file 
                        $constant_unzip = "symfony_unzip.php";
                        $config_unzip = 'symfony_unzip.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        // we have to put this CI folder file
                        $constant_unzip = "symfony_folder_remove.php";
                        $config_unzip = 'symfony_folder_remove.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        
                        
                        if($unzipUpload){
                            
                            // Deploy other server Curl call on unzip folder
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."symfony_unzip.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }
                            curl_close($ch);
                            
                            // Deploy other server Curl call on folder remove 
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."symfony_folder_remove.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }else{
                                
                                 
                                // Setting database details
                                $create_folder = time().'_'.$ftp_server;
                                @mkdir($create_folder, 0777, true);
                                $database_file_path = "parameters.yml"; // $create_folder."/database.php";
                                
                                $config_file_path = "parameters.yml"; // $create_folder."/config.php";
                                touch($database_file_path);
                                touch($config_file_path);
                                
                                WriteDatabaseDetails($database_file_path, $db_host, $db_username, $db_password, $db_name);
                                // Putting database file on client server
                                $database_handle = '/api/app/config/parameters.yml';
                                //$database_handle = '/parameters.yml';
                                @ftp_put($conn_id, $database_handle, $database_file_path, FTP_BINARY);
                                
                                // All file Delete for folder for client
                                $symfony_zip = $selected_framework.'.zip';
                                $zip_lib = "pclzip.lib.php";
                                $symfony_zip_file = "symfony_unzip.php";
                                $symfony_zip_remove = "symfony_folder_remove.php";
                                $symfony_folder_removes = "symfony";
                                @ftp_delete($conn_id, $symfony_zip);
                                @ftp_delete($conn_id, $zip_lib);
                                @ftp_delete($conn_id, $symfony_zip_file);
                                @ftp_delete($conn_id, $symfony_zip_remove);
                                @ftp_delete($conn_id, $symfony_folder_removes);
                            }
                            curl_close($ch);
                            $array = array('status' => 'success', 'message' => "Your! Site deployed successfully");
                            echo json_encode($array);	
                        }
                    }else{
                        $array = array('status' => 'error', 'message' => "Failed! Please resubmit.");
                        echo json_encode($array); 
                    }
                }
                
            }else if($selected_framework == "laveral"){
                
                $src_dir 	= 'projects/'.$selected_framework.'.zip'; // we have to put this Zip File 
                $dst_dir 	= $selected_framework.'.zip'; // place the Zip here
                
                require_once $selected_framework . "_configuration.php";
                // Uploading Zip file
                if($uploading = @ftp_put( $conn_id, $dst_dir, $src_dir, FTP_BINARY )){
                    if($uploading){
                        // we have to put this Zip file Lib 
                        $libzipcode = "pclzip.lib.php";
                        $config_ziplib = 'pclzip.lib.php';
                        $unzipLib = @ftp_put($conn_id, $config_ziplib, $libzipcode, FTP_BINARY);
                        // we have to put this CI Unzip file 
                        $constant_unzip = "laveral_unzip.php";
                        $config_unzip = 'laveral_unzip.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        // we have to put this CI folder file
                        $constant_unzip = "laveral_folder_remove.php";
                        $config_unzip = 'laveral_folder_remove.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        
                        
                        if($unzipUpload){
                            
                            // Deploy other server Curl call on unzip folder
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."laveral_unzip.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }
                            curl_close($ch);
                            
                            // Deploy other server Curl call on folder remove 
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."laveral_folder_remove.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }else{
                                
                                 
                                // Setting database details
                                $create_folder = time().'_'.$ftp_server;
                                @mkdir($create_folder, 0777, true);
                                $database_file_path = "parameters.yml"; // $create_folder."/database.php";
                                
                                $config_file_path = "parameters.yml"; // $create_folder."/config.php";
                                touch($database_file_path);
                                touch($config_file_path);
                                
                                WriteDatabaseDetails($database_file_path, $db_host, $db_username, $db_password, $db_name);
                                // Putting database file on client server
                                $database_handle = '/parameters.yml';
                                @ftp_put($conn_id, $database_handle, $database_file_path, FTP_BINARY);
                                
                                // All file Delete for folder for client
                                $laveral_zip = $selected_framework.'.zip';
                                $zip_lib = "pclzip.lib.php";
                                $laveral_zip_file = "laveral_unzip.php";
                                $laveral_zip_remove = "laveral_folder_remove.php";
                                $laveral_folder_removes = "laveral";
                                @ftp_delete($conn_id, $laveral_zip);
                                @ftp_delete($conn_id, $zip_lib);
                                @ftp_delete($conn_id, $laveral_zip_file);
                                @ftp_delete($conn_id, $laveral_zip_remove);
                                @ftp_delete($conn_id, $laveral_folder_removes);
                            }
                            curl_close($ch);
                            $array = array('status' => 'success', 'message' => "Your! Site deployed successfully");
                            echo json_encode($array);	
                        }
                    }else{
                        $array = array('status' => 'error', 'message' => "Failed! Please resubmit.");
                        echo json_encode($array); 
                    }
                }
            }else if($selected_framework == "yii"){
                $src_dir 	= 'projects/'.$selected_framework.'.zip'; // we have to put this Zip File 
                $dst_dir 	= $selected_framework.'.zip'; // place the Zip here
                
                require_once $selected_framework . "_configuration.php";
                // Uploading Zip file
                if($uploading = @ftp_put( $conn_id, $dst_dir, $src_dir, FTP_BINARY )){
                    if($uploading){
                        // we have to put this Zip file Lib 
                        $libzipcode = "pclzip.lib.php";
                        $config_ziplib = 'pclzip.lib.php';
                        $unzipLib = @ftp_put($conn_id, $config_ziplib, $libzipcode, FTP_BINARY);
                        // we have to put this CI Unzip file 
                        $constant_unzip = "yii_unzip.php";
                        $config_unzip = 'yii_unzip.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        // we have to put this CI folder file
                        $constant_unzip = "yii_folder_remove.php";
                        $config_unzip = 'yii_folder_remove.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        
                        
                        if($unzipUpload){
                            
                            // Deploy other server Curl call on unzip folder
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."yii_unzip.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }
                            curl_close($ch);
                            
                            // Deploy other server Curl call on folder remove 
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."yii_folder_remove.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }else{
                                
                                 
                                // Setting database details
                                $create_folder = time().'_'.$ftp_server;
                                @mkdir($create_folder, 0777, true);
                                $database_file_path = "parameters.yml"; // $create_folder."/database.php";
                                
                                $config_file_path = "parameters.yml"; // $create_folder."/config.php";
                                touch($database_file_path);
                                touch($config_file_path);
                                
                                WriteDatabaseDetails($database_file_path, $db_host, $db_username, $db_password, $db_name);
                                // Putting database file on client server
                                $database_handle = '/parameters.yml';
                                @ftp_put($conn_id, $database_handle, $database_file_path, FTP_BINARY);
                                
                                // All file Delete for folder for client
                                $yii_zip = $selected_framework.'.zip';
                                $zip_lib = "pclzip.lib.php";
                                $yii_zip_file = "yii_unzip.php";
                                $yii_zip_remove = "yii_folder_remove.php";
                                $yii_folder_removes = "yii";
                                @ftp_delete($conn_id, $yii_zip);
                                @ftp_delete($conn_id, $zip_lib);
                                @ftp_delete($conn_id, $yii_zip_file);
                                @ftp_delete($conn_id, $yii_zip_remove);
                                @ftp_delete($conn_id, $yii_folder_removes);
                            }
                            curl_close($ch);
                            $array = array('status' => 'success', 'message' => "Your! Site deployed successfully");
                            echo json_encode($array);	
                        }
                    }else{
                        $array = array('status' => 'error', 'message' => "Failed! Please resubmit.");
                        echo json_encode($array); 
                    }
                }
            }else if($selected_framework == "cakephp"){
                $src_dir 	= 'projects/'.$selected_framework.'.zip'; // we have to put this Zip File 
                $dst_dir 	= $selected_framework.'.zip'; // place the Zip here
                
                require_once $selected_framework . "_configuration.php";
                // Uploading Zip file
                if($uploading = @ftp_put( $conn_id, $dst_dir, $src_dir, FTP_BINARY )){
                    if($uploading){
                        // we have to put this Zip file Lib 
                        $libzipcode = "pclzip.lib.php";
                        $config_ziplib = 'pclzip.lib.php';
                        $unzipLib = @ftp_put($conn_id, $config_ziplib, $libzipcode, FTP_BINARY);
                        // we have to put this CI Unzip file 
                        $constant_unzip = "cakephp_unzip.php";
                        $config_unzip = 'cakephp_unzip.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        // we have to put this CI folder file
                        $constant_unzip = "cakephp_folder_remove.php";
                        $config_unzip = 'cakephp_folder_remove.php';
                        $unzipUpload = @ftp_put($conn_id, $config_unzip, $constant_unzip, FTP_BINARY);
                        
                        
                        if($unzipUpload){
                            
                            // Deploy other server Curl call on unzip folder
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."cakephp_unzip.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }
                            curl_close($ch);
                            
                            // Deploy other server Curl call on folder remove 
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $site_host."cakephp_folder_remove.php");
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            if(curl_exec($ch) === FALSE) {
                                return false;
                            }else{
                                
                                 
                                // Setting database details
                                $create_folder = time().'_'.$ftp_server;
                                @mkdir($create_folder, 0777, true);
                                $database_file_path = "parameters.yml"; // $create_folder."/database.php";
                                
                                $config_file_path = "parameters.yml"; // $create_folder."/config.php";
                                touch($database_file_path);
                                touch($config_file_path);
                                
                                WriteDatabaseDetails($database_file_path, $db_host, $db_username, $db_password, $db_name);
                                // Putting database file on client server
                                $database_handle = '/parameters.yml';
                                @ftp_put($conn_id, $database_handle, $database_file_path, FTP_BINARY);
                                
                                // All file Delete for folder for client
                                $cakephp_zip = $selected_framework.'.zip';
                                $zip_lib = "pclzip.lib.php";
                                $cakephp_zip_file = "cakephp_unzip.php";
                                $cakephp_zip_remove = "cakephp_folder_remove.php";
                                $cakephp_folder_removes = "cakephp";
                                @ftp_delete($conn_id, $cakephp_zip);
                                @ftp_delete($conn_id, $zip_lib);
                                @ftp_delete($conn_id, $cakephp_zip_file);
                                @ftp_delete($conn_id, $cakephp_zip_remove);
                                @ftp_delete($conn_id, $cakephp_folder_removes);
                            }
                            curl_close($ch);
                            $array = array('status' => 'success', 'message' => "Your! Site deployed successfully");
                            echo json_encode($array);	
                        }
                    }else{
                        $array = array('status' => 'error', 'message' => "Failed! Please resubmit.");
                        echo json_encode($array); 
                    }
                }
            }else if($selected_framework == "drupal"){
            
            }else if($selected_framework == "kohana"){
                
            }
			
		} else {
            
			$array = array('status' => 'error', 'message' => "Failed! Incorrect Diectory path.");
			echo json_encode($array);
		
		}	
	}else{
		$array = array('status' => 'error', 'message' => "Failed! FTP Root Directory Should be empty.");
		echo json_encode($array);
	}
} 
