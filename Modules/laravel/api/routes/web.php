<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Frontend
|--------------------------------------------------------------------------|
*/

// Home
Route::name('home')->get('/', 'Front\PostController@index');

// Contact
Route::resource('contacts', 'Front\ContactController', ['only' => ['create', 'store']]);
Route::group([
        'prefix' => '{something}',
        'middleware' => ['web', 'auth']
    ], function() {
});
// Posts and comments
Route::prefix('posts')->namespace('Front')->group(function () { 
   
});

/*------------------------------------------------*/
Route::get('addPayment','PaymentController@addPayment')->name('addPayment');


Route::post('paypal', 'PaymentController@postPaymentWithpaypal')->name('paypal');


Route::get('paypal','PaymentController@getPaymentStatus')->name('status');
/*------------------------------------------------*/

Route::post('/user/login', 'UserController@login');
Route::post('/user/signup', 'UserController@signup');

Route::post('user/forgot_password', 'UserController@forgot_password');
Route::post('user/password_reset', 'UserController@password_reset');
Route::post('user/update_db', 'UserController@update_db');
Route::post('/user/emailVerified', 'UserController@emailVerified');


Route::post('dashboard/change_password', 'DashboardController@change_password');
Route::post('dashboard/user_details', 'DashboardController@user_details');
Route::post('dashboard/update_user_details', 'DashboardController@update_user_details');

Route::get('/user/getStatus', 'UserController@getStatus');
Route::post('/user/getStatus', 'UserController@getStatus');


Route::post('/admin', 'AdminController@admin');
Route::get('/admin/users', 'AdminController@usersDetails');
Route::post('/admin/change_status', 'AdminController@change_status');
Route::post('/admin/pages', 'AdminController@pages');
Route::get('/admin/pages', 'AdminController@pages');


Route::get('/admin/cms_management', 'AdminController@cms_management');
Route::post('/admin/cms_management', 'AdminController@cms_management');
Route::post('/admin/update_cms_management', 'AdminController@update_cms_management');
Route::post('/admin/delete_template', 'AdminController@delete_template');


Route::get('/admin/menus', 'AdminController@menus');
Route::post('/admin/menus', 'AdminController@menus');

Route::post('/admin/create_menu', 'AdminController@create_menu');
Route::post('/admin/update_menu', 'AdminController@update_menu');
Route::post('/admin/delete_menu', 'AdminController@delete_menu');


Route::get('/admin/email_templates', 'AdminController@email_templates');
Route::post('/admin/email_templates', 'AdminController@email_templates');

Route::get('/admin/create_template', 'AdminController@create_template');
Route::post('/admin/create_template', 'AdminController@create_template');



// Authentification
Auth::routes();

