<?php
namespace App;
use Hash;
use Auth;
use DB;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'page_content_cms';

    protected $fillable = [
        'page_name',
		'page_content',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public static function page_content_cms(array $page_content_cmsData){
		$model = new static($page_content_cmsData);
		$model->save();
		return $model;
    }	
}
