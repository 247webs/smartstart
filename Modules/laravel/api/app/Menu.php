<?php
namespace App;
use Hash;
use Auth;
use DB;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Menu extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'menu_managements';
    protected $fillable = [
		'menu_name',
		'menu_type',
		'menu_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public static function create_menu(array $create_menu_insertData){
		$model = new static($create_menu_insertData);
		$model->save();
		return $model;
    }	
	
		
	
}
