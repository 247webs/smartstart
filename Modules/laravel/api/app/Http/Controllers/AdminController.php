<?php
namespace App\Http\Controllers;
use Mail;
use App\User;
use App\Admin;
use App\Menu;
use App\Email;
use Hash;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller {
	/**--ADMIN-LOGIN FUNCTION--**/
	public function admin(Request $request){
		$email = $request->get('email');
		$password = $request->get('password');	
		$whereData = array('email' => $email, 'password'=>$password);
		$userDetails = DB::table('users')->where($whereData)->first();	
		if(!empty($userDetails)){
			$email = $userDetails->email;
			$role = $userDetails->role;
			$id = $userDetails->id;
			$db_password = base64_decode($userDetails->password);
			if($role == 2){
				$data = array('code' => '200', 'id' => $id, 'message' => 'success', 'success_message' => 'Login has been successfull.');
				return response($data);
			}else{
				$data = array('Code' => '400', 'message' => 'error', 'error_message' => '');
				return response($data);	
			}
		}else{
			$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'Email or password is incorrect.');
			return response($data);			
		}
	}
	/**--END ADMIN-LOGIN FUNCTION--**/
	
	/**--USER LIST FUNCTION--**/
	public function usersDetails(Request $request){
		$userDetails = DB::table('users')->where('role', '=', 1)->get();
		
		$data = array('code' => '200','message' => 'success', 'data' => $userDetails, 'success_message' => 'User list');
		return response($data);
	}
	public function change_status(Request $request){
		$user_id = $request->get('hiddenid');
		$hiddenStatus = $request->get('hiddenStatus');
		
		$userDetails = DB::table('users')->where('id', '=', $user_id)->first();
		if(!empty($userDetails)){
		
			$status = $userDetails->status;
			if($status == '1'){
				$update = DB::table('users')->where('id', '=', $user_id)->update([
					'status'	=> '0',
				]);
				$data = array('code' => '200','message' => 'success', 'success_message' => 'This user status has been changed successfully.');
				return response($data);
			}else if($status == '0'){
				$update = DB::table('users')->where('id', '=', $user_id)->update([
					'status'	=> '1',
				]);
				$data = array('code' => '200', 'message' =>'success', 'success_message' => 'This user status has been changed successfully.');
				return response($data);
			}
		}else{
			$data = array('code' => '400', 'message' => 'error', 'error_message' => '');
			return response($data);
		}
	}
	/**--END CHANGE STATUSs FUNCTION--**/
	
	/**--PAGE LIST DETAILS FUNCTION--**/
	public function pages(Request $request){
		$id = $request->get('id');
		$page_name = $request->request->get('content');
		if(!empty($id)){
			$result = DB::table('page_content_cms')->where('id', '=', $id)->first();
			if(!empty($result)){
				$data = array('code' => '200', 'message' => 'success', 'data' => $result, 'success_message' => '');
				return response($data);
			}else{
				$data = array('Code' => '400', 'message' => 'error', 'error_message' => '');
				return response($data);			
			}
		}else{
			if(!empty($page_name)){
				$userDetails = DB::table('page_content_cms')->where('page_name', '=', $page_name)->first();
				if(!empty($userDetails)){
					$page_content = $userDetails->page_content;
					$data = array('code' => '200','message' => 'success', 'page_content' => $page_content, 'success_message' => '');
					return response($data);
				}else{
					$data = array('Code' => '400', 'message' => 'error', 'error_message' => '');
					return response($data);			
				}
			}else{
				$page_content_cms = DB::table('page_content_cms')->get();
				$data = array('code' => '200', 'message' => 'success', 'data' => $page_content_cms, 'success_message' => '');
				return response($data);
			}
		}
	}
	/**EMAIL TEMPLATE GET DATA FUNCTION--**/
	public function email_templates(Request $request){
		$result = DB::table('email_templates')->get();
		$data = array('code' => '200', 'message' => 'success', 'data' => $result, 'success_message' => '');
		return response($data);
	}
	/**--END TEMPLATE GET DATA FUNCTION--**/
		
	/**--EMAIL-TEMPLATE FUNCTION--**/
	public function create_template(Request $request){
		$sign_up = $request->get('sign_up');
		$forgot_password = $request->get('forgot_password');
		$change_password = $request->get('change_password');
		$sign_up_content = $request->get('sign_up_content');
		$forgot_password_content = $request->get('forgot_password_content');
		$change_password_content = $request->get('change_password_content');
		
		$emailTemplatesData = DB::table('email_templates')->get();
		
		if($emailTemplatesData->isEmpty()){
			$create_templateInsertData = [
				['template_title' => $sign_up, 'content' => $sign_up_content],
				['template_title' => $forgot_password,'content' => $forgot_password_content],
				['template_title' => $change_password,'content' => $change_password_content]
			];
			foreach ($create_templateInsertData as $value) {
				$result = DB::table('email_templates')->insert($value);	
			}
			$data = array('code' => '200', 'message' => 'success', 'success_message' => 'Tamplate has been created successfully.');
			return response($data);
		}else{
			$update = DB::table('email_templates')->where('id', '=', 1)->update([
				'content'	=> $sign_up_content
			]);
			
			$update = DB::table('email_templates')->where('id', '=', 2)->update([
				'content'	=> $forgot_password_content
			]);
			
			$update = DB::table('email_templates')->where('id', '=', 3)->update([
				'content'	=> $change_password_content
			]);
			
			$data = ['code' => '200', 'message' => 'success', 'success_message' => 'Tamplate has been Update successfully.'];
			return response($data);
		}
	}
	/**--END EMAIL-TEMPLATE FUNCTION--**/
	
	/**--CREATE CMS PAGE FUNCTION--**/
	public function cms_management(Request $request){
		$page_name = $request->get('page_name');
		$page_contant = $request->get('page_contant');
		
		$page_content_cmsData = array(
			'page_name' 	=> $page_name,
			'page_content'	=> $page_contant,
		);
		$userDetails = DB::table('page_content_cms')->where('page_name', '=', $page_name)->first();
		if(empty($userDetails)){
			$result = Admin::page_content_cms($page_content_cmsData);
			$data = ['code' => '200', 'message' => 'success', 'success_message' => 'Template has been created successfully.'];
			return response($data);
		}
		else{
			$data = array('code' => '400', 'message' => 'error', 'error_message' => 'This page name is already exist.');
			return response($data);			
		}
	}
	/**--END CREATE CMS FUNCTION--**/
	
	/**--UPDATE CMS PAGE FUNCTION--**/
	public function update_cms_management(Request $request){
		$user_id = $request->get('hidden_id');
		$page_name = $request->get('pageName');
		$page_content = $request->get('pageContent');
		
		$userDetails = DB::table('page_content_cms')->where('page_name', '=', $page_name)->first();
		
			$update = DB::table('page_content_cms')->where('id', '=', $user_id)->update([
				'page_name'	=> $page_name,
				'page_content'	=> $page_content
			]);
			$data = array('code' => '200', 'message' => 'success', 'success_message' => 'Template has been updated successfully.');
			return response($data);
		
	}
	/**--END UPDATE CMS FUNCTION--**/
	
	/**--DELETE TEMPLATE FUNCTION--**/
	public function delete_template(Request $request){
		$user_id = $request->get('hiddenid');
		$value=DB::table('page_content_cms')->where('id', '=', $user_id)->delete();
		$data = array('code' => '200', 'message' => 'success', 'success_message' => 'Template has been deleted successfully.');
		return response($data);
	}
	/**--END DELETE CMS FUNCTION--**/
	
	/**MENU-LIST FUNCTION--**/
	public function menus(Request $request){
		$id	= $request->get('id');
		if(!empty($id)){
			$menuManagementsDetails = DB::table('menu_managements')->where('id', '=', $id)->first();
			$data = array('status' => '200','message' => 'success', 'data' => $menuManagementsDetails);
			return response($data);
		}else{
			$result = DB::table('menu_managements')->get();
			$data = array('status' => '200','message' => 'success', 'data' => $result);
			return response($data);
		}
	}
	/**--END MENU-LIST FUNCTION--**/
	
	/**--CREATE MENU FUNCTION--**/
	public function create_menu(Request $request){
		$create_menu_insertData = array(
			'menu_name' 	=> $request->get('menu_name'),
			'menu_type'	=> $request->get('menu_type'),
			'menu_url'	=> $request->get('menu_url')
		);
		$result = Menu::create_menu($create_menu_insertData);
		$data = array('code' => '200', 'message' => 'success', 'success_message' => 'Menu has been created successfully.');
		return response($data);
		
	}
	/**--END CREATE MENU FUNCTION--**/
	
	/**UPDATE MENU FUNCTION--**/
	public function update_menu(Request $request){
		$user_id = $request->get('hidden_id');
		$update = DB::table('menu_managements')->where('id', '=', $user_id)->update([
			'menu_name'	=> $request->get('menuName'),
			'menu_type'	=> $request->get('menuType'),
			'menu_url'	=> $request->get('menuUrl')
		]);
		$data = array('code' => '200', 'message' => 'success', 'success_message' => 'Menu has been updated successfully.');
		return response($data);
	}
	/**--DELETE MENU FUNCTION--**/
	public function delete_menu(Request $request){
		$user_id	= $request->get('hiddenid');
		$value=DB::table('menu_managements')->where('id', '=', $user_id)->delete();
		$data = array('code' => '200', 'message' => 'success', 'success_message' => 'Menu has been deleted successfully.');
		return response($data);
	}
	/**--END DELETE MENU FUNCTION--**/	
}
