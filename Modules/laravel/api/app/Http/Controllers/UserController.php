<?php
namespace App\Http\Controllers;
use Mail;
use App\User;
use App\Email;
use App\Admin;
use App\Menu;
use Hash;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class UserController extends Controller {
	/**--SIGN-UP FUNCTION--**/
    public function signup(Request $request){
		$token = substr(md5(uniqid(rand(1,6))), 0, 25);
		$validator = Validator::make($request->all(),[
			'name' => 'required',
			'email'=>'required|email|unique:users,email',
			'password' => 'required',
			'confirm_password' => 'required'
		   ]);
		if($validator->fails()) { 
			$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'This email already exist.');
			return response($data);
		}else{
			$insertData = array(
				'email' 			=> 	trim($request->get('email')),
				'password' 			=> 	md5($request->get('password')),
				'name' 				=> 	$request->get('name'),
				'token'				=>	$token,
				'image'				=>	'upload/user_logo.png'
			);
			$result = User::createRegistration($insertData);			
			if($result){
				$base_url  = $_SERVER['HTTP_REFERER'];
				$link = "<a href='".$base_url."#/login?email=".$request->get('email').'&token='.$token."'>Click here for login</a>";
				$emailTempDetails = DB::table('email_templates')->where('template_title', '=','Signup')->first();
				$templateRes = sprintf($emailTempDetails->content,$result->name);
				$to = $result->email;
				$subject = "Confirm Your Registration";
				$txt = $templateRes.'<br>'.$link;
				$headers = "From: rdwivedi183@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";
				mail($to,$subject,$txt,$headers);
				$data = array('Code' => '200', 'message' => 'success', 'success_message' => 'User register successfull.');
				return response($data);	
			}
		}	
    }
	
	/**--EMAIL-VERIFIED FUNCTION--**/
	public function emailVerified(Request $request){
		$email = $request->get('email');
		$token = $request->get('token');
		$update = DB::table('users')->where('email', '=', $email)->update([
			'token'				=> '',
			'is_email_verified'	=> '1'
		]);
	}
	
	/**--LOGIN FUNCTION--**/
	public function login(Request $request){
		$email = $request->get('email');
		$password = md5($request->get('password'));
		$token = $request->get('token');
		$whereData = array('email'=> $email , 'password'=> $password);  
		$userDetails = DB::table('users')->where($whereData)->first(); 
		

		if(!empty($userDetails)){	
			$db_email 			= $userDetails->email;
			$name 			= $userDetails->name;
			$user_id			= $userDetails->id;
			$status				= $userDetails->status;
			$role				= $userDetails->role;
			$db_token			= $userDetails->token;
			$is_email_verified	= $userDetails->is_email_verified;
			if($token){
				if(!$db_token){
					$update = DB::table('users')->where('email', '=', $email)->update([
						'token'				=> '',
						'is_email_verified'	=> '1'
					]);
					$data = array('Code' => '200', 'id' => $user_id, 'name' =>$name, 'message' => 'success', 'success_message' => 'Login has been successfull.');
					return response($data);
				}else{
					$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'Token is expaire.');
					return response($data);	
				}
			}
			else if($is_email_verified == 0){
				$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'Your email address is not yet verified, please verify your email first.');
				return response($data);
			}else if($status == 0){
				$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'You are disabled by admin please contact administrator.');
				return response($data);
			}else{
				$data = array('Code' => '200', 'id' => $user_id, 'name' => $name, 'message' => 'success', 'success_message' => 'Login has been successfull.');
				return response($data);
			}
		}else{
			$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'Email or password is incorrect.');
			return response($data);	
		}	
	}		
	
	/**--FORGOT PASSWORD--**/
	public function forgot_password(Request $request){
		$email = $request->get('email');
		$userDetails = DB::table('users')->where('email', '=', $email)->first();
		if(!empty($userDetails)){
			if($userDetails->is_email_verified == 1){
				$token = substr(md5(uniqid(rand(1,6))), 0, 25);	
				$update = DB::table('users')->where('email', '=', $email)->update([
					'token'	=> $token,
				]);
				$enc_email = base64_encode($email);
				$base_url  = $_SERVER['HTTP_REFERER'];
				$link = "<a href='".$base_url."
				#/resetpassword?email=" .$enc_email .'&token='.$token."' target='_blank'>Click here for reset password</a>";				
				$emailTempDetails = DB::table('email_templates')->where('template_title', '=','Forgot Password')->first();
				$templateRes = sprintf($emailTempDetails->content, $userDetails->name);
				$to = $email;
				$subject = "Forgot Password";
				$txt = $templateRes.'<br>'.$link;
				$headers = "From: rdwivedi183@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";
				mail($to,$subject,$txt,$headers);
				$data = array('Code' => '200', 'message' => 'success', 'success_message' => 'Reset Password link is sent to your email.');
				return response($data);
			}else{
				$data = array('Code' => '200', 'message' => 'success', 'success_message' => 'Your email address is not yet verified, please verify your email first and reset the password.');
				return response($data);
			}
		}else{
			$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'Email is incorrect.');
			return response($data);
		}
	}
	/**--PASSWORD RESET FUNCTION--**/
	public function password_reset(Request $request){
		$email =  base64_decode($request->get('email'));
		$token = $request->get('token');
		$password =  md5($request->get('password'));
		$userDetails = DB::table('users')->where('email', '=', $email)->first();
		if(!empty($userDetails)){
			$db_token = $userDetails->token;		
			if($token == $db_token){
				$update = DB::table('users')->where('email', '=', $email)->update([
					'password'	=> $password,
					'token'	=> '',
				]);	
				$data = array('Code' => '200', 'message' => 'success', 'success_message' => 'Password has been reset successfully.');
				return response($data);
			}else{
				$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'Token is expaire.');
				return response($data);
			}
		}else{
			$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'failed');
			return response($data);
		}
	}
	/**--UPDATE-DB-CONTROLLER--**/
	 public function update_db(Request $request){

		$path =  base_path().'/.env';
		$host_name = $request->get('host_name');
		$data_base_name = $request->get('data_base_name');
		$user_name = $request->get('user_name');
		$password = $request->get('password');
		
		$connection = mysqli_connect($host_name, $user_name, $password, $data_base_name);
		if($connection){
			touch($path);
			$file = fopen($path,"w");	
$database_details = "
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:QkJCLjEU5ZjxwvEGy2jM3vDtfcDl/Ml1TZgYtaIRhS8=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=".$host_name."
DB_PORT=3306
DB_DATABASE=".$data_base_name."
DB_USERNAME=".$user_name."
DB_PASSWORD=".$password."

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=
MAIL_PORT=465
MAIL_USERNAME=rdwivedi183@gmail.com
MAIL_PASSWORD=9450000285
MAIL_ENCRYPTION=ssl
MAIL_FROM_ADDRESS=admin@site.com
MAIL_FROM_NAME=administrator

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
";

			fwrite($file,$database_details);
			fclose($file);
			$data = array('Code' => '200', 'message' => 'success', 'success_message' => 'Database credentials has been update successfully.');
			return response($data);
		}
		mysqli_close($connection);
	}
	
	/**--GET STATUS FUNCTION--**/
	public function getStatus(Request $request){	
		$user_id = $request->get('id');
		$userDetails = DB::table('users')->where('id', '=', $user_id)->first();
		if(!empty($userDetails)){
			$status = $userDetails->status;
			if($status == 1){
				$data = array('code' => 200, 'message' => 'active');
				return response($data);
			}else{
				$data = array('code' => 400, 'message' => 'inactive');
				return response($data);
			}
		}else{
			$data = array('status' => '400', 'message' => 'faild', 'error_message' => '');
			return response($data);
		}
	}
}

