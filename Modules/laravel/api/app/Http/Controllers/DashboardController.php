<?php
namespace App\Http\Controllers;
use Mail;
use App\User;use App\Email;
use Hash;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class DashboardController extends Controller {
	
	/**--CHANGE PASSWORD FUNCTION--**/
	public function change_password(Request $request){
		$user_id = $request->get('id');
		$old_password = md5($request->get('old_password'));
		$new_password = md5($request->get('new_password'));
		$userDetails = DB::table('users')->where('id', '=', $user_id)->where('password', '=', $old_password)->first();
		if(!empty($userDetails)){
			$update = DB::table('users')->where('id', '=', $user_id)->update([
				'password'	=> $new_password,
			]);			$emailTempDetails = DB::table('email_templates')->where('template_title', '=','Change Password')->first();			$templateRes = sprintf($emailTempDetails->content,$userDetails->name);			
			$to = $userDetails->email;
			$subject = "Change Password";
			$txt = $templateRes;
			$headers = "From: rdwivedi183@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";
			mail($to,$subject,$txt,$headers);
			$data = array('Code' => '200', 'message' => 'success', 'success_message' => 'Your password has been changed successfully.');
			return response($data);
		}else{
			$data = array('Code' => '400', 'message' => 'error', 'error_message' => 'Old password does not match.');
			return response($data);
		}	
	}
	
	/**--USER DETAILS FUNCTION--**/
	public function user_details(Request $request){
		$user_id = $request->get('id');
		$userDetails = DB::table('users')->where('id', '=', $user_id)->first();
		$data = array('status' => '200', 'message' => 'success', 'data' => $userDetails, 'success_message' => '' );
		return response($data);
    }

	/**--UPDATE USER DETAILS FUNCTION--**/
	public function update_user_details(Request $request){
		$id = $request->get('uid');
		$user_id = $request->get('id');
		if(!empty($user_id)){
			$name = $request->get('name');
			$date_of_birth = $request->get('date_of_birth');
			$newDate = date("Y-m-d", strtotime($date_of_birth));
			$phone = $request->get('phone');
			$email = $request->get('email');
			$gender = $request->get('gender');
			$update = DB::table('users')->where('id', '=', $user_id)->update([
				'name'		=> $name,
				'phone'		=> $phone,
				'gender'	=> $gender,
				'date_of_birth'	=> $newDate,
			]);			
			$userData = array('status' => '200','message' => 'success', 'success_message' => 'Profile has been updated successfully.');
			return response($userData);
		}else if(!empty($id)){			
		$type = $_FILES['file']['type'];			
		$imageType = explode("/",$type)[0];
			if($imageType == 'image'){
				$file = $request->files->get('file');
				$new = time();
				$original_name = $new.$file->getClientOriginalName();
				$path = 'public/upload';
				$file->move($path,$original_name);
				$dbPath = $path.'/'.$original_name;
				$update = DB::table('users')->where('id', '=', $id)->update([
					'image'		=> $dbPath
				]);
				$userData = array('status' => '200','message' => 'success', 'success_message' => 'Image has been change.');
				return response($userData);
			}else{
				$userData = array('status' => '400', 'message' => 'file type', 'error_message' => 'Image Type');
				return response($userData);
			}	
		}		
	}
}

