<?php
namespace App;
use Hash;
use Auth;
use DB;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'users';
    protected $fillable = [
        'name',
		'email',
		'password',
		'token', 
		'image'
		
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public static function createRegistration(array $insertData){
		$model = new static($insertData);
		$model->save();
		return $model;
    }		
	public static function getuserDetails($request)
	{
		// print_r($request->all());die;
		$email =$request->get('email');
		$password =$request->get('password');
		
		$userDetails = DB::table('users')->where('email', '=', $email)->first();
		$db_email = $userDetails->email;
		
		$db_password = base64_decode($userDetails->password);
		if($email == $db_email &&  $db_password == $password){
			$data = array('status'=>'200','message'=>'success');
			return response($data);
		}else{
			
		}
		die;
	}
	
}
