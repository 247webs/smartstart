<?php
namespace App;
use Hash;
use Auth;
use DB;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Email extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'email_templates';
    protected $fillable = [
        'template_title',
		'content'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public static function create_template(array $create_templateInsertData){
		// print_r($create_templateInsertData);die;
		$model = new static($create_templateInsertData);
		$model->save();

		return $model;
    }		
	
}
