/* LOGIN CONTROLLER */
app.controller('adminCtrl', ['$scope', '$location', '$http', '$timeout','postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) { 
	$scope.formData = {};
	$scope.formData.email = 'admin@gmail.com';
	$scope.formData.password = 'Any@12345';

	$scope.errorMsg = false;
	$scope.successMsg = false;

	$scope.login_button = function(){
		if($scope.loginForm.$valid == true){
			postService.postData(API_URL+'admin', $scope.formData).then(function (response) {
				$scope.successMsg = '';
				$scope.errorMsg = '';
				if(response.message == 'success'){
					$timeout(function () {
						$scope.successMsg = response.success_message;
					}, 0);
					$timeout(function () {
						$location.url("/admin/dashboard");
					}, 2000);
				}else if(response.message == 'error'){
					$timeout(function () {
						$scope.errorMsg = response.error_message;
					}, 0);
					$timeout(function () {
						$scope.errorMsg = false;
					}, 2000);
				}
				
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 2000);
		}
	}
	/*---ADMIN DASHBOARD FUNCTION---*/
	$scope.Status = function(id,status,$index){ 
		$scope.formData.hiddenid = id;	
	}
	$scope.toggleNo = function(){
		if($scope.formData.hiddenStatus == '0'){
			getService.getData(API_URL+'admin/users').then(function (userData) {
				$scope.res  = userData.data;
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			getService.getData(API_URL+'admin/users').then(function (userData) {
				$scope.res  = userData.data;
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}
	}
	$scope.changedStatus = function(){
		postService.postData(API_URL+'admin/change_status', $scope.formData).then(function (response) {
			$scope.successMsg = '';
				$scope.errorMsg = '';
			if(response){
				$timeout(function () {
					$scope.successMsg = response.success_message;
				}, 0);
				$timeout(function () {
					$scope.successMsg = false;
				}, 4000);
				$(location).attr('href', '#/admin/dashboard')
			}
		},function(response) {
				console.log('albums retrieval failed.');
		});
	}
	
	/*---------GET DATA FOR ADMIN DASHBOARD---------*/
	$scope.loadData = function(){
 		getService.getData(API_URL+'admin/users').then(function (userData) {
			$timeout(function () {
				$scope.res  = userData.data;
			}, 0);
		},function(response) {
			console.log('albums retrieval failed.');
		});
	}
}]);
/*------------------END ADMIN-LOGIN CONTROLLER--------------------------*/

/*EMAIL TEMPLATE CONTROLLER*/
app.controller('email_templateController', ['$scope', '$location', '$http', '$timeout','postService', 'getService','API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
	$scope.formData = {};
	$scope.formData.sign_up = 'Signup';
	$scope.formData.forgot_password = 'Forgot Password';
	$scope.formData.change_password = 'Change Password';
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.email_template = function(){
		if($scope.email_template_form.$valid){
			postService.postData(API_URL+'admin/create_template', $scope.formData).then(function (response) {
				$scope.successMsg = '';
				$scope.errorMsg = '';
				if(response.message === "success"){
					$timeout(function () {
						$scope.successMsg = response.success_message;
					}, 0);
					$timeout(function () {
						$scope.SuccessMsg = false;
						$(location).attr('href', '#/admin/dashboard');
					}, 4000);	  
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}	
	}

	$scope.loadEmailTemplate  = function(){
		
		getService.getData(API_URL+'admin/email_templates').then(function (userData) {
			if(userData.data.length != 0){	
				$scope.formData.sign_up = userData.data[0].template_title;
				$scope.formData.sign_up_content = userData.data[0].content;
				
				$scope.formData.forgot_password = userData.data[1].template_title;
				$scope.formData.forgot_password_content = userData.data[1].content;
				
				$scope.formData.change_password = userData.data[2].template_title;
				$scope.formData.change_password_content = userData.data[2].content;
			}
		},function(response) {
			console.log('albums retrieval failed.');
		});
	}

}]);
/*------------------END EMAIL TEMPLATE CONTROLLER--------------------------*/

/* CMS MANAGEMENT CONTROLLER */
app.controller('cms_managementCtrl', ['$scope', '$location', '$http', '$timeout', 'postService', 'API_URL', function($scope, $location, $http, $timeout, postService, API_URL) { 
	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.create_page_button = function(){
		if($scope.cms_management.$valid == true){
			postService.postData(API_URL+'admin/cms_management', $scope.formData).then(function (response) {
				$scope.successMsg = '';
				$scope.errorMsg = '';
				if(response.message === 'success'){
					$timeout(function () {
						$scope.successMsg = response.success_message;
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;
						$(location).attr('href', '#/admin/dashboard');
					}, 4000);
				}else if(response.message === 'error'){
					$timeout(function () {
						$scope.errorMsg = response.error_message;
					}, 0);
					$timeout(function () {
						$scope.errorMsg = false;
					}, 4000);  
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 2000);
		}
	}	
}]);
/* CMS MANAGEMENT CONTROLLER */
app.controller('pagelistCtrl', ['$scope', '$location', '$http', '$timeout','postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.baseUrl = $location.host();
	$scope.formData = {};
	$scope.loadData = function(){
		getService.getData(API_URL+'admin/pages').then(function (userData) {
			var url = $location.absUrl().split('#')[0];
			$timeout(function () {
				$scope.res  = userData.data;
			}, 0);
		},function(response) {
			console.log('albums retrieval failed.');
		});
	}
	
	/**-----------GET VALUE-FOR CMS-FUNCTION-START----------------------**/
	$scope.edit = function(id){
		postService.postData(API_URL+'admin/pages', {'id':id}).then(function (userData){
			$scope.formData.pageName = userData.data.page_name;
			$scope.formData.pageContent = userData.data.page_content;
			$scope.formData.hidden_id = id;
		},function(userData) {
			console.log('albums retrieval failed.');
		});
	}
	
	/**-----------UPDATE-FOR CMS-FUNCTION-START----------------------**/
	$scope.update = function(){
		if($scope.cms_managementUpdate.$valid == true){
			postService.postData(API_URL+'admin/update_cms_management', $scope.formData).then(function (response) {
				if(response.message == 'success'){
					$timeout(function () {
						$scope.successMsg = response.success_message;
						
						getService.getData(API_URL+'admin/pages').then(function (userData) {
						$scope.res  = userData.data;
						},function(response) {
							console.log('albums retrieval failed.');
						});
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;
					}, 5000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 2000);
		}
	}	
	
	/**-----------DELETE FUNCTION-----------**/
	$scope.delete = function(id,$index){
		$scope.formData.hiddenid = id;
	}
	
	$scope.deleteYes = function(){
		postService.postData(API_URL+'admin/delete_template', $scope.formData).then(function (response) {
			$scope.successMsg = '';
			$scope.errorMsg = '';
			if(response.message == 'success'){
				$timeout(function () {
					$scope.successMsg = response.success_message;
					getService.getData(API_URL+'admin/pages').then(function (userData) {
						$scope.res  = userData.data;
					},function(response) {
						console.log('albums retrieval failed.');
					});
				}, 0);
				$timeout(function () {
					$scope.successMsg = false;
				}, 5000);
				
			}
		},function(response) {
				console.log('albums retrieval failed.');
		});
	}
}]);
/*HTML DATA BIND*/
angular.module('app')
    .filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);
/* CMS MANAGEMENT CONTROLLER */
app.controller('staticCtrl', ['$scope', '$location', '$http', '$timeout','postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
	
	$scope.formData = {};
	var content = $location.search().page;
	$scope.loadPageData =function(){
		postService.postData(API_URL+'admin/pages', {'content' : content}).then(function (response) {
			$scope.successMsg = '';
			$scope.errorMsg = '';
			if(response.message == 'success'){
				$timeout(function () {
					$scope.myText  = response.page_content;
					console.log($scope.myText);
				}, 0);
			}
		},function(response) {
			console.log('albums retrieval failed.');
		});			
	}
	

}]);
/*------------------END CMS MANAGEMENT CONTROLLER--------------------------*/

/* MENU MANAGEMENT CONTROLLER */
app.controller('menu_managementCtrl', ['$scope', '$location', '$http', '$timeout', 'getService', 'postService', 'API_URL', function($scope, $location, $http, $timeout, getService, postService,API_URL) {
	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.successMsg = false;
	
	/*MENU DATA**/
	$scope.loadData = function(){
		getService.getData(API_URL+'admin/menus').then(function (userData) {
			$scope.res  = userData.data;
		},function(response) {
			console.log('albums retrieval failed.');
		});	
	}
	/*CREATE MENU FUNCTION*/
	$scope.create_menu = function(){
		if($scope.menu_management_form.$valid){
			postService.postData(API_URL+'admin/create_menu', $scope.formData).then(function (response) {
				$scope.successMsg = '';
				$scope.errorMsg = '';
				if(response.message === "success"){
					$timeout(function () {
						$scope.successMsg = response.success_message;
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;
						$(location).attr('href', '#/admin/menu_management');
					}, 3000);	  
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 3000);
		}
	}
	
	/**-----------GET VALUE-FOR CMS-FUNCTION-START----------------------**/
	$scope.edit = function(id){
		postService.postData(API_URL+'admin/menus', {'id':id}).then(function (userData){
			$scope.formData.menuName = userData.data.menu_name;
			$scope.formData.menuType = userData.data.menu_type;
			$scope.formData.menuUrl = userData.data.menu_url;
			$scope.formData.hidden_id = id;
		},function(userData) {
			console.log('albums retrieval failed.');
		});
	}
	
	/**-----------UPDATE-FOR-CMS-FUNCTION-START----------------------**/
	$scope.updateSuccess = false;
	$scope.update = function(){
		if($scope.menu_management_edit_form.$valid == true){
			postService.postData(API_URL+'admin/update_menu', $scope.formData).then(function (response) {
				$scope.successMsg = '';
				$scope.errorMsg = '';
				if(response.message == 'success'){
					$timeout(function () {
						$scope.successMsg = response.success_message;
						getService.getData(API_URL+'admin/menus').then(function (userData) {
						$scope.res  = userData.data;
						},function(response) {
							console.log('albums retrieval failed.');
						});
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;	
					}, 6000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 2000);
		}
	}
	
	/**-----------DELETE FUNCTION-------------------------**/
	$scope.delete = function(id,$index){
		$scope.formData.hiddenid = id;	
	}
	
	$scope.deleteYes = function(){
		postService.postData(API_URL+'admin/delete_menu', $scope.formData).then(function (response) {
			$scope.successMsg = '';
			$scope.errorMsg = '';
			if(response.message == 'success'){
				$timeout(function () {
					$scope.successMsg = response.success_message;
						getService.getData(API_URL+'admin/menus').then(function (userData) {
						$scope.res  = userData.data;
					},function(response) {
						console.log('albums retrieval failed.');
					});
				}, 0);
				$timeout(function () {
					$scope.successMsg = false;
				}, 3000);
				
			}
		},function(response) {
				console.log('albums retrieval failed.');
		});
	}
}]);
/*------------------END MENU-MANAGEMENT CONTROLLER--------------------------*/
/* cardDetails Controller*/

app.controller('cardDetailsCtrl', ['$scope', '$location', '$http', '$timeout','postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
	
	$scope.formData = {};
	$scope.sss = function(){
		
		postService.postData(API_URL+'paypal', $scope.formData).then(function (responseData) {
			if(response){
				$timeout(function () {
					alert();
				}, 0);
				$timeout(function () {
					$scope.successMsg = false;
				}, 3000);
				
			}
		},function(responseData) {
				console.log('albums retrieval failed.');
		});
	}
	

}]);