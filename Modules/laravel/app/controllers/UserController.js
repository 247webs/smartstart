/* REGISTER CONTROLLER */
app.controller('registerCtrl', ['$scope', '$location', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL',  function($scope, $location, $http, $timeout, userGetService, userPostService, API_URL) { 
	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.register_button = function(){
		if($scope.registerForm.$valid){
			if($scope.formData.password == $scope.formData.confirm_password){
				userPostService.postData(API_URL+'user/signup', $scope.formData).then(function (response) {
					$scope.successMsg = '';
					$scope.errorMsg = '';
					if(response.message === 'success'){
						$(location).attr('href', '#/thankYou');
					}else if(response.message === 'error'){
					
						$timeout(function () {
							$scope.errorMsg = response.error_message;
						}, 0);
						$timeout(function () {
							$scope.errorMsg = false;
						}, 5000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});	
			}else{
				$timeout(function () {
					$scope.errorMsg = 'Confirm password does not match.';
				}, 0);
				$timeout(function () {
					$scope.errorMsg = false;
				}, 4000);
			}
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}
	}
}]);
/*------------------END REGISTER FUNCTION--------------------------*/
/*Login controller*/
app.controller('loginController', ['$scope', '$location', '$http', '$state', '$localStorage', '$window', '$rootScope', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $http, $state, $localStorage, $window, $rootScope, $timeout, userGetService, userPostService, API_URL) {
	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.forgotlink = false;
	$scope.formData.email = "rdwivedi183@mailinator.com";
	$scope.formData.password = "123456";
	/* Function For IS EMAIL VERIFIED */
	
		$scope.formData.email = $location.search().email;
		$scope.formData.token = $location.search().token;
		
		if($scope.formData.email != undefined && $scope.formData.token != undefined){
			userPostService.postData(API_URL+'user/emailVerified', {'email': $scope.formData.email, 'token': $scope.formData.token}).then(function (response){

			},function(response) {
				console.log('albums retrieval failed.');
			});
		}
	
	$scope.login_button = function(){
		if($scope.loginForm.$valid){
			userPostService.postData(API_URL+'user/login', $scope.formData).then(function (response) {
				$scope.successMsg = '';
				$scope.errorMsg = '';
				if(response.message === 'success'){
					localStorage.setItem("id", response.id);
					localStorage.setItem("name", response.name);
					$scope.successMsg = response.success_message;
					$timeout(function () {
						$(location).attr('href', '#/dashboard'); 
					}, 4000);
				}else if(response.message === 'error'){
					$timeout(function () {
						$scope.errorMsg = response.error_message;
					}, 0);
					$timeout(function () {
						$scope.errorMsg = false;
					}, 4000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});	
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}
	}
}]);
/*------------------END LOGIN FUNCTION--------------------------*/

/* forgotPasswordCtrl */
app.controller('forgotPasswordCtrl', ['$scope', '$location', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userGetService, userPostService, API_URL) { 	
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.forgotPassword = function(){
		if($scope.forgotPasswordForm.$valid == true){
			userPostService.postData(API_URL+'user/forgot_password', $scope.formData).then(function (response) {
				$scope.successMsg = '';
				$scope.errorMsg = '';
				if(response.message === 'success'){
					 $timeout(function () {
						$scope.successMsg = response.success_message;
					  }, 0);
					  $timeout(function () {
						$scope.successMsg = false;
					  }, 5000);
				}else if(response.message == 'error'){
					 $timeout(function () {
						$scope.errorMsg = response.error_message;
					  }, 0);
					$timeout(function () {
						$scope.errorMsg = false;
					}, 5000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});	
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
				  }, 0);
			$timeout(function () {
				$scope.errorMsg = false;
				  }, 6000);
		}	
	}
	
}]);
/*------------------END FORGOT FUNCTION--------------------------*/
/*resetPasswordCtrl*/
app.controller('resetPasswordCtrl', ['$scope', '$location', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userGetService, userPostService, API_URL) { 

	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.formData.token = $location.search().token;
	$scope.formData.email = $location.search().email;
	
	$scope.reset_button = function(){
		if($scope.resetForm.$valid == true){
			if($scope.formData.password == $scope.formData.confirm_password){
				userPostService.postData(API_URL+'user/password_reset', $scope.formData).then(function (response) {
					$scope.successMsg = '';
					$scope.errorMsg = '';
					if(response.message == 'success'){
						$timeout(function () {
							$scope.successMsg = response.success_message;
						}, 0);
						$timeout(function () {
							$scope.successMsg = false;
							$(location).attr('href', '#/login');
						}, 6000); 
					}else if(response.message == 'error'){
						$timeout(function () {
							$scope.errorMsg = response.error_message;
						}, 0);
						$timeout(function () {
							$scope.errorMsg = false;
							$(location).attr('href', '#/forgotpassword');
						}, 6000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});	
			}else{
				$timeout(function () {
				$scope.errorMsg = 'Confirm password does not match.';
				  }, 0);
				$timeout(function () {
				$scope.errorMsg = false;
				  }, 5000);		
			}
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 5000);
		}
	}
}]);
/*------------------END RESET FUNCTION--------------------------*/

/* CHANGE PASSWORD CONTROLLER */
app.controller('changePasswordCtrl', ['$scope', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $timeout, userGetService, userPostService, API_URL) {
	
	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.formData.id = localStorage.getItem("id");
	
	$scope.change_password = function(){
		if($scope.changPasswordForm.$valid){
			if( $scope.formData.new_password == $scope.formData.confirm_password){
				userPostService.postData(API_URL+'dashboard/change_password', $scope.formData).then(function (response) {
					$scope.successMsg = '';
					$scope.errorMsg = '';
					if(response.message == 'success'){
						$timeout(function () {
							$scope.successMsg = response.success_message;
						}, 0);
						$timeout(function () {
							$scope.successMsg = false;
							$(location).attr('href', '#/dashboard');
						}, 5000);	  
					}else if(response.message == 'error'){
						$timeout(function () {
							$scope.errorMsg = response.error_message;
						}, 0);
						$timeout(function () {
							$scope.errorMsg = false;
						}, 4000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});	
			}else{
				$timeout(function () {
					$scope.errorMsg = 'Confirm password does not match.';
				}, 0);
				$timeout(function () {
					$scope.errorMsg = false;
				}, 4000);
			}
			
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}	
	}
}]);
/*------------------ CHANGE PASSWORD CONTROLLER--------------------------*/

/*VIEW PROFILE CONTROLLER*/
app.controller('profile', ['$scope', '$http', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, userGetService, userPostService, API_URL) {
	$scope.profileData = {};
	var id = localStorage.getItem("id");
	userPostService.postData(API_URL+'dashboard/user_details', {'id':id}).then(function (userData){
		// alert(userData.flage);
		$scope.profileData = userData.data;
	},function(userData) {
		console.log('albums retrieval failed.');
	});
	
}]);


/*UPDATE PROFILE CONTROLLER*/
app.controller('updateProfile', ['$scope', '$location', '$timeout', '$http', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $timeout, $http, userGetService, userPostService, API_URL) { 
	$scope.statusMsg = false;
	$scope.errorMsg = false;
	$scope.successMsg = false;
	/* Set default value */
	$scope.profileData = {};
	var id = localStorage.getItem("id");
	/* get DB data for edit */

	userPostService.postData(API_URL+'dashboard/user_details', {'id':id}).then(function (response) {
		if(response.message == 'success'){
			$scope.profileData = response.data;
		}
	},function(response) {
		console.log('albums retrieval failed.');
	});
	
	/* Update the profile */
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.updateProfile = function(profileData){
		
		if($scope.editprofile.$valid){
			$scope.profileData.id = localStorage.getItem("id");
			userPostService.postData(API_URL+'dashboard/update_user_details', $scope.profileData).then(function (response) {
				if(response.message == 'success'){
					$timeout(function () {
						$scope.successMsg = response.success_message;
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;
						$scope.loder = false;
						$(location).attr('href', '#/profile')
					}, 4000);	
				}

			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}		
	}
	

}]);
/*------------------END PROFILE FUNCTION--------------------------*/

/*dASHBOARD CONTROLLER*/
app.controller('dashboardCtrl', ['$scope', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $timeout, userGetService, userPostService, API_URL) { 
	/*---------GET DATA FOR DASHBOARD---------*/
		$scope.name = localStorage.getItem("name");
		
}]);
/*------------------END DASHBOARD FUNCTION--------------------------*/

/* Loguout controller*/
app.controller('logout', ['$scope', '$http', '$location', '$timeout', function($scope, $http, $location, $timeout) {
	$scope.logout = function(){
		localStorage.clear();
		$(location).attr('href', '#/home');
	}
	
}]);
/*------------------END LOGOUT FUNCTION--------------------------*/

/* Layout Controller*/
app.controller('layout', ['$scope', '$http', '$location', '$timeout', '$window',  'userGetService', 'userPostService', 'API_URL', function($scope, $http, $location, $timeout, $window, userGetService, userPostService, API_URL) {


	userGetService.getData(API_URL+'admin/menus').then(function (response) {
		$scope.res  = response.data;
	},function(response) {
		console.log('albums retrieval failed.');
	});
	$scope.reloadRoute = function() {
		location.reload(); 
	}
	
	/*---------Uer-status-Logout--------*/
	var id = localStorage.getItem("id");
	userPostService.postData(API_URL+'user/getStatus', {'id' : id}).then(function (response) {
		if(response.message == 'inactive'){
			$timeout(function () {
				$scope.inactive = 'You are disabled by admin please contact administrator.';
			}, 0);
			$timeout(function () {
				$(location).attr('href', '#/login');
				localStorage.clear();
				$scope.inactive = false;
			}, 6000);		
		}
	},function(response) {
		console.log('albums retrieval failed.');
	});			
	
}]);
/*------------------END LAYOUT FUNCTION--------------------------*/
/* update_dbCtrl Controller*/
app.controller('update_dbCtrl', ['$scope', '$http', '$location', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $location, $timeout, userGetService, userPostService, API_URL) {
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$timeout(function () {
		$scope.errorMsg = 'Your database credentials are wrong, please enter correct credentials.';
	}, 0);
	$timeout(function () {
		$scope.bdError = false;
	}, 5000);
	$scope.update = function(){
		if($scope.dbUpdateForm.$valid){
			userPostService.postData(API_URL+'user/update_db', $scope.formData).then(function (response) {
			if(response.message == 'success'){
				$timeout(function () {
					$scope.successMsg = response.success_message;
				}, 0);
				$timeout(function () {
					$scope.successMsg = false;
					$(location).attr('href', '#/login');
				}, 5000);		
			}
		},function(response) {
			console.log('albums retrieval failed.');
		});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please fill out all mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 5000);	
		}	
	}
	
}]);
/*------------------END UPDATE-DB FUNCTION--------------------------*/