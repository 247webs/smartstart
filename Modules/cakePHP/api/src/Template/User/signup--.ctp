<html>
	<head>
		<title>Cake Php</title>
	</head>
	<body>
		<?php echo $this->Form->create('user', array('url' => array('controller' => 'user', 'action' => 'signup'))); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<p><h3><b>Register an account</b></h3></p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="text" name="name" id="name" placeholder="Name" class="form-control">
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="text" name="email" id="email" placeholder="Email" class="form-control">
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-3">
						<input type="password" name="password" id="password" placeholder="Password" class="form-control">
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-3">
						<input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" class="form-control">
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="submit" name="button" value=" Submit " class="form-control btn btn-primary" id="submit">
						<a href="/user/forgetPassword">Forget Password</a>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</body>
</html>

