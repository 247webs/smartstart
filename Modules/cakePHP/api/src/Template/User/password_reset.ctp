<html>
	<head>
		<title>Cake Php</title>
	</head>
	<body>
		<?php
			$getUrl = $_SERVER[ 'REQUEST_URI' ] ;
			$url = explode("/",$getUrl);
			$email = $url[3];
			$randomNum = $url[4];
		?>
		<?php echo $this->Form->create('user', array('url' => array('controller' => 'user', 'action' => 'passwordReset'))); ?>
			<div class="row">
				<input type="hidden" name="hiddenEmail" value="<?php echo $email?>">
				<input type="hidden" name="hiddenRandomNum" value="<?php echo $randomNum?>">
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<p><h3><b>Create your password</b></h3></p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-3">
						<input type="password" name="password" id="password" placeholder="Password" class="form-control" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-3">
						<input type="password" name="confirm_password" id="cpassword" placeholder="Confirm Password" class="form-control">
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="submit" name="button" value=" Submit " class="form-control btn btn-primary" id="submit">
						<a href="/user/signup">Register</a>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</body>
</html>

