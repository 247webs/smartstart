<html>
	<head>
		<title>Cake Php</title>
	</head>
	<body>
		<?php echo $this->Form->create('usert', array('url' => array('controller' => 'user', 'action' => 'forgetPassword'))); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<p><h3><b>Forget Password</b></h3></p>
					</div>
				</div>
				<input type="hidden" name="hidden" value="email">
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="email" name="email" id="email" placeholder="Email" class="form-control" required>
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="submit" name="button" value=" Submit " class="btn btn-primary" id="submit">
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</body>
</html>

