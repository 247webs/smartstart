<html>
	<head>
		<title>Cake Php</title>
	</head>
	<body>
		<?php echo $this->Form->create('user', array('url' => array('controller' => 'user', 'action' => 'login'))); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<p><h3><b>Login Here</b></h3></p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="email" name="email" id="email" placeholder="Email" class="form-control" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-3">
						<input type="password" name="password" id="password" placeholder="Password" class="form-control">
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="submit" name="button" value=" Submit " class="btn btn-primary" id="submit">
						<a href="/user/signup">Register</a>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</body>
</html>

