<html>
	<head>
		<title>Cake Php</title>
	</head>
	<body>
		<?php echo $this->Form->create('dashboard', array('url' => array('controller' => 'dashboard', 'action' => 'changePassword'))); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<p><h3><b>Change Password</b></h3></p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-3">
						<input type="password" name="old_password" id="old_password" placeholder="Old Password" class="form-control" minlength="8" required>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-3">
						<input type="password" name="new_password" id="new_password" placeholder="New Password" class="form-control" minlength="8">
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3 col-md-offset-3">
						<input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm New Password" class="form-control" minlength="8">
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6 col-md-offset-3">
						<input type="submit" name="button" value=" Reset " class="form-control btn btn-primary" id="submit">
						<a href="/dashboard/home">Dashboard</a>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
	</body>
</html>

