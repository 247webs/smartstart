<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use DateTime;
use Cake\View\View;
use Cake\Routing\Router;
use Cake\Mailer\Email;
use Cake\Utility\Text;
use App\Controller\Session;

use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use Cake\Controller\Component\RequestHandlerComponent;
use Cake\Http\ServerRequest;


class DashboardController extends AppController {
	
	public function initialize(){
        parent::initialize();
		// $this->autoRender = false;
		$this->loadModel('Users');
		$this->loadModel('Email_templates');
    }
	
	public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->allow(['userDetails', 'editDetails', 'updateDetails', 'updateImage', 'changePassword', 'getStatus']);
    }
	
	/* View login user details */
	public function userDetails(){
		if($this->request->is('post')) {
			$users =$this->Users->find()->where(['id'=>$this->request->getData('id')])->toArray();
			$testdata = ['statuscode' => 200, 'message' => 'success', "data"=>$users];
			$this->set($testdata);
		}
	}
	
	/* Edit the login user details */
	public function editDetails(){
		if($this->request->is('post')) {
			$users = $this->Users->find()->where(['id' => $this->request->getData('id')])->toArray();
			$testdata = ['statuscode' => 200, 'message' => 'success', "data"=>$users];
			$this->set($testdata);
		}
	}
	
	/* Update login user details */
	public function updateDetails(){
		if($this->request->is('post')){
			$data = $this->request->getData();
			$date_of_birth = date("Y-m-d",strtotime($this->request->getData('date_of_birth')));
			$data['date_of_birth']  = $date_of_birth;
			
			$usersTable = TableRegistry::get('users');
			$userData = $usersTable->get($this->request->getData('id'));
			$usersTable->patchEntity($userData,$data);
			if($usersTable->save($userData)){
				$testdata = ['statuscode' => 200, 'message' => 'success', 'data' => $userData];
			}else{
				$testdata = ['statuscode' => 200, 'message' => 'error'];
			}
			$this->set($testdata);
		}
	}
	
	/* Image Upload */
	public function updateImage(){
		$this->autoRender = false;
		if($this->request->is('post')) {
			$data = $this->request->getData();
			$uid = $data['uid'];
			$name = $data['file']['name'];
			$imgName = time().$name;
			$tmp_name = $data['file']['tmp_name'];
			$type = $data['file']['type'];
			$fileType = explode('/', $type);
			$imgType = $fileType[0];
			if($imgType == 'image'){
				move_uploaded_file($tmp_name, '../webroot/uploads/' . $imgName);
				$usersTable = TableRegistry::get('Users');
				$user = $usersTable->get($uid);
				$user->image = $imgName;
				if($usersTable->save($user)){
					$testdata = ['statuscode' => 200, 'message' => 'success', 'name' => $imgName];
					echo json_encode($testdata);
				}
			}else{
				$testdata = ['statuscode' => 400, 'message' => 'error image type'];
				echo json_encode($testdata);
			}
		}
	}
	
	/* Change password */
	public function changePassword(){
		if($this->request->is('post')) {
			$data = $this->request->getData();

			$old_password = $data['oldPassword'];
			$new_password = $data['newPassword'];
			$id = $data['id'];
			
			$data = $this->Users->find()->where(['id' => $id])->toArray();
			$dbPassword = $data[0]['password'];
			$name = $data[0]['name'];
			$email = $data[0]['email'];

			if($old_password == $dbPassword){
				$usersTable = TableRegistry::get('users');
				$user = $usersTable->get($id);
				$user->password = $new_password;
				$usersTable->save($user);
				
				/* Get data for change password email-template */
				$this->loadModel('Email_templates');
				$emailChangePass = $this->Email_templates->find()->where(['template_title' => 'Change Password'])->toArray();
				$emailChangePassTemp = $emailChangePass[0]['content'];
				$templateRes = sprintf($emailChangePassTemp,$name);
				
				/* Email Function */
				$to = $email;
				$subject = "Change Password";
				$txt = $templateRes;
				$headers = "From: testshivam000@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";
				mail($to,$subject,$txt,$headers);
				
				$testdata = ['code' => 200, 'message' => 'success'];
				$this->set($testdata);
			}else{
				$testdata = ['statuscode' => 400, 'message' => 'error'];
				$this->set($testdata);
			}
		}
	}
	
	/* Check status of login user */
	public function getStatus(){
		if($this->request->is('post')) {
			$id = $this->request->getData('id');
			$users = $this->Users->find()->where(['id' => $id])->toArray();
			$status = $users[0]['status'];
			if($status == 1){
				$testdata = ['statuscode' => 200, 'message' => 'active'];
				$this->set($testdata);
			}else{
				$testdata = ['statuscode' => 400, 'message' => 'inactive'];
				$this->set($testdata);
			}
		}
	}
}

