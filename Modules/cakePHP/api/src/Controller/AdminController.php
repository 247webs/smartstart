<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\UnauthorizedException;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use DateTime;
use Cake\View\View;
use Cake\Routing\Router;
use Cake\Mailer\Email;
use Cake\Utility\Text;
use Cake\Utility\Security;
use App\Controller\Session;
use Cake\Validation\Validator;
use Cake\Controller\Component\RequestHandlerComponent;
use Cake\Http\ServerRequest;

class AdminController extends AppController {
	
	public function initialize(){
        parent::initialize();
		$this->loadModel('Users');
		$this->loadModel('Menu_managements');
		$this->loadModel('Page_content_cms');
		$this->loadModel('Email_templates');
    }
	
	public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->allow(['login', 'users', 'pages', 'cmsManagement', 'editCmsManagement', 'updateCmsManagement', 'deleteCms', 'contentData', 'createTemplate', 'emailTemplates', 'menus', 'createMenu', 'deleteMenu', 'editMenus', 'updateMenus', 'changeStatus']);
    }

	/* LOGIN FUNCTION FOR ADMIN */
	public function login() {
		if($this->request->is('post')){
			$where = [];
			$where['email'] 	= $this->request->getData('email');
			$where['role'] 		= $this->request->getData('role');
			$users = $this->Users->find('all', array('fields' => array('id', 'email', 'role')))->where($where)->toArray();
			
			if(!empty($users)){
				$admin = $this->Users->find()->where(['email' => $this->request->getData('email')])->toArray();

				$dbPassword = $admin[0]['password'];
				$id = $admin[0]['id'];
				if($dbPassword == $this->request->getData('password')){
					$testdata = ['code' => 200, 'message' => 'success', 'id' => $id];
					$this->set($testdata);
				}else{
					$testdata = ['code' => 400, 'message' => 'email password error'];
					$this->set($testdata);
				}
			}else{
				$testdata = ['code' => 400, 'message' => 'email password error'];
				$this->set($testdata);
			}
        }
    }

	/* GET ALL USERS FOR ADMIN DASHBOARD */
	public function users(){
		$this->loadModel('Users');
		$where['role'] 	= 1;
		$users = $this->Users->find()->where($where)->toArray();
		$testdata = ['statuscode' => 200, 'message' => 'success', "data"=>$users];
		$this->set($testdata);
	}
	
/* CMS MANAGEMENT */
	/* Page List */
	public function pages(){
		$pageLists = $this->Page_content_cms->find()->toArray();
		$testdata = ['statuscode' => 200, 'message' => 'success', "data"=>$pageLists];
		$this->set($testdata);
	}

	/* Insert cms data */
	public function cmsManagement(){
		if($this->request->is('post')) {
			$data = $this->request->getData();
			$where = [];
			$where['page_name'] = $this->request->getData('page_name');
			$exist = $this->Page_content_cms->find('all', array('fields' => array('page_name')))->where($where)->toArray();
			if(empty($exist)){
				$cmsTable = TableRegistry::get('page_content_cms');
				$user = $cmsTable->newEntity();
				$user->page_name = $data['page_name'];
				$user->page_content = $data['page_content'];
				if($cmsTable->save($user)) {
					$id = $user->id;
					$testdata = ['statuscode' => 200, 'message' => 'success', 'id' => $id];
					$this->set($testdata);
				}
			}else{
				$testdata = ['statuscode' => 400, 'message' => 'unique pageName'];
				$this->set($testdata);
			}
		}
	}
	
	/* Get content */
	public function contentData(){
		if($this->request->is('post')) {
			$page_name = $this->request->getData('page_name');
			$content = $this->Page_content_cms->find()->where(['page_name' => $page_name])->toArray();
			$testdata = ['statuscode' => 200, 'message' => 'success', "data"=>$content];
			$this->set($testdata);
		}
	}
	
	/* Edit CMS/Page */
	public function editCmsManagement(){
		if($this->request->is('post')) {
			$id = $this->request->getData('id');
			$data = $this->Page_content_cms->find()->where(['id' => $id])->toArray();
			$testdata = ['statuscode' => 200, 'message' => 'success', "data"=>$data];
			$this->set($testdata);
		}
	}
	
	/* Update CMS/Page */
	public function updateCmsManagement(){
		if($this->request->is('post')) {
			$cmsTable = TableRegistry::get('Page_content_cms');
			$userData = $cmsTable->get($this->request->getData('hidden_id'));
			$cmsTable->patchEntity($userData,$this->request->getData());
			if($cmsTable->save($userData)){
				$testdata = ['statuscode' => 200, 'message' => 'success'];
			}else{
				$testdata = ['statuscode' => 200, 'message' => 'error'];
			}
			$this->set($testdata);
		}
	}
	
	/* Delete CMS/Page */
	public function deleteCms(){
		if($this->request->is('post')) {
			$hiddenid = $this->request->getData('hiddenid');
			$entity = $this->Page_content_cms->get($hiddenid);
			$result = $this->Page_content_cms->delete($entity);
			$testdata = ['statuscode' => 200, 'message' => 'success'];
			$this->set($testdata);
		}
	}
	
/* EMAIL TEMPLATE */
	/* Create Email Template */
	public function createTemplate(){
		if($this->request->is('post')) {
			$data = $this->request->getData();
			
			$sign_up = $data['sign_up'];
			$forgot_password = $data['forgot_password'];
			$change_password = $data['change_password'];
			
			$sign_up_content = $data['sign_up_content'];
			$forgot_password_content = $data['forgot_password_content'];
			$change_password_content = $data['change_password_content'];
			
			$createTemplate = [
				['template_title' => $sign_up, 'content' => $sign_up_content],
				['template_title' => $forgot_password, 'content' => $forgot_password_content],
				['template_title' => $change_password, 'content' => $change_password_content]
			];
			
			$connection = ConnectionManager::get('default');
			$results = $connection->execute('SELECT * FROM email_templates')->fetchAll('assoc');
			
			if(empty($results)){
				foreach($createTemplate as $value){
					$email_template = TableRegistry::get('Email_templates');
					$user = $email_template->newEntity();
					$user->template_title = $value['template_title'];
					$user->content = $value['content'];
					if ($email_template->save($user)) {
					}
					$testdata = ['statuscode' => 200, 'message' => 'success'];
					$this->set($testdata);
				}
			}else{
				$connection = ConnectionManager::get('default');
				$connection->update('email_templates', ['content' => $sign_up_content], ['id' => 1]);
				$connection->update('email_templates', ['content' => $forgot_password_content], ['id' => 2]);
				$connection->update('email_templates', ['content' => $change_password_content], ['id' => 3]);
				$testdata = ['statuscode' => 200, 'message' => 'update success'];
				$this->set($testdata);
			}
		}
	}
	
	/* Get Email Template Data */
	public function emailTemplates(){
		$connection = ConnectionManager::get('default');
		$results = $connection->execute('SELECT * FROM email_templates')->fetchAll('assoc');
		$testdata = ['statuscode' => 200, 'message' => 'success', "data" => $results];
		$this->set($testdata);
	}
	
/* --- MENU MANAGEMENT --- */
	/* Menu List */
	public function menus(){
		$menuLists = $this->Menu_managements->find()->toArray();
		$testdata = ['statuscode' => 200, 'message' => 'success', "data"=>$menuLists];
		$this->set($testdata);
	}
	
	/* Create Menu*/
	public function createMenu(){
		if($this->request->is('post')) {
			$data = $this->request->getData();
			$menu_managementTable = TableRegistry::get('Menu_managements');
			$menu_management = $menu_managementTable->newEntity();
			$menu_management->menu_name = $data['menu_name'];
			$menu_management->menu_type = $data['menu_type'];
			$menu_management->menu_url = $data['menu_url'];
			if ($menu_managementTable->save($menu_management)) {
				$id = $menu_management->id;
				$testdata = ['code' => 200, 'message' => 'success', 'id' => $id];
				$this->set($testdata);
			}
		}
	}
	
	/* Edit Menu*/
	public function editMenus(){
		if($this->request->is('post')) {
			$id = $this->request->getData('id');
			$data = $this->Menu_managements->find()->where(['id' => $id])->toArray();
			$testdata = ['statuscode' => 200, 'message' => 'success', "data"=>$data];
			$this->set($testdata);
		}
	}
	
	/* UPdate Menu */
	public function updateMenus(){
		if($this->request->is('post')) {
			$cmsTable = TableRegistry::get('Menu_managements');
			$userData = $cmsTable->get($this->request->getData('hidden_id'));
			$cmsTable->patchEntity($userData,$this->request->getData());
			if($cmsTable->save($userData)){
				$testdata = ['statuscode' => 200, 'message' => 'success'];
			}else{
				$testdata = ['statuscode' => 200, 'message' => 'error'];
			}
			$this->set($testdata);
		}
	}
	
	/* Delete Menu*/
	public function deleteMenu(){
		if($this->request->is('post')) {
			$id = $this->request->getData('hiddenid');
			$entity = $this->Menu_managements->get($id);
			$result = $this->Menu_managements->delete($entity);
			$testdata = ['statuscode' => 200, 'message' => 'success'];
			$this->set($testdata);
		}
	}
	
/* USER STATUS Active/Inactive */
	public function changeStatus(){
		if($this->request->is('post')) {
			$hiddenid = $this->request->getData('hiddenid');
			
			$user = $this->Users->find()->where(['id' => $hiddenid])->toArray();
			$status = $user[0]['status'];
			if($status == 1){
				$usersTable = TableRegistry::get('users');
				$user = $usersTable->get($hiddenid);
				$user->status = 0;
				if($usersTable->save($user)){
					$testdata = ['statuscode' => 200, 'message' => 'success'];
					$this->set($testdata);
				}
			}elseif($status == 0){
				$usersTable = TableRegistry::get('users');
				$user = $usersTable->get($hiddenid);
				$user->status = 1;
				if($usersTable->save($user)){
					$testdata = ['statuscode' => 200, 'message' => 'success'];
					$this->set($testdata);
				}
			}
		}
	}
}

