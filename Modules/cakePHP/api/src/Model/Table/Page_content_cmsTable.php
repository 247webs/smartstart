<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;

class Page_content_cms extends Table {
	public function initialize(array $config){
		$this->addBehavior("Timestamp");
		$this->setTable('page_content_cms');
	}
}