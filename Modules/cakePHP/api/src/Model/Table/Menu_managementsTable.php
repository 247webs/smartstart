<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;

class Menu_managements extends Table {
	public function initialize(array $config){
		$this->addBehavior("Timestamp");
		$this->setTable('menu_managements');
	}
}