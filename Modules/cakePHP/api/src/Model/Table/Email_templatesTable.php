<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;

class Email_templates extends Table {
	public function initialize(array $config){
		$this->addBehavior("Timestamp");
		$this->setTable('email_templates');
	}
}