app.factory('postService', function($http, $q) {
	return {
		postData: function(API_URL, formData) {
			var deferred = $q.defer();
			$.ajax({
				type: 'POST',
				url: API_URL,
				dataType: "json",	
				data: formData,
				success: function(res) {deferred.resolve(res);}, error : function(msg, code) {deferred.reject(msg);}
			});
			return deferred.promise;
		}
	};
});

app.factory('getService', function($http, $q) {
	return {
		getData: function(API_URL) {
			var deferred = $q.defer();
			$.ajax({
				type: 'GET',
				url: API_URL,
				dataType: "json",	
				success: function(res) {deferred.resolve(res);}, error : function(msg, code) {deferred.reject(msg);}
			});
			return deferred.promise;
		}
	};
});
