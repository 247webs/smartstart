angular.module('app').constant('API_URL', "http://upworkdevelopers.com/test/prockp/api/");

/* Register Controller */
app.controller('registerCtrl', ['$scope', '$location', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userGetService, userPostService, API_URL) { 
	$scope.formData = {};
	$scope.passwordError = false;
	$scope.allFieldError = false;
	$scope.uniqueEmailError = false;
	$scope.register_button = function(){
		if($scope.registerForm.$valid){
			if($scope.formData.password == $scope.formData.confirm_password){
				userPostService.postData(API_URL+'user/signup', $scope.formData).then(function (response){
					if(response.message === "success"){
						$(location).attr('href', '#/thankYou');
					}else if(response.message === "unique"){
						$timeout(function () {
							$scope.uniqueEmailError = true;
						}, 0);
						$timeout(function () {
							$scope.uniqueEmailError = false;
						}, 4000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});
			}else{
				$timeout(function () {
					$scope.passwordError = true;
				}, 0);
				$timeout(function () {
					$scope.passwordError = false;
				}, 4000);
			}
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
}]);

/* Thank You Controller */
app.controller('thankYouController', ['$scope', '$location', '$http', '$timeout', function($scope, $location, $http, $timeout) { 
	
}]);

/* Login Controller */
app.controller('loginController', ['$scope', '$location', '$http', '$state', '$localStorage', '$window', '$rootScope', '$timeout', 'userPostService', 'API_URL', function($scope, $location, $http, $state, $localStorage, $window, $rootScope, $timeout,userPostService, API_URL) {
	/* Set Default values */
	$scope.formData = {};
	$scope.formData.email = 'test@gmail.com';
	$scope.formData.password = 'test@123';
	$scope.emailPasswordError = false;
	$scope.loginSuccess = false;
	$scope.allFieldError = false;
	$scope.activeInactive = false;
	$scope.emailVerifiedError = false;

	
	/* Function For IS EMAIL VERIFIED */
	$scope.is_email_verified = function(){
		$scope.formData.email = $location.search().email;
		$scope.formData.token = $location.search().token;

		if($scope.formData.email != undefined && $scope.formData.token != undefined){
			userPostService.postData(API_URL+'user/emailVerified', {'email': $scope.formData.email, 'token': $scope.formData.token}).then(function (response){

			},function(response) {
				console.log('albums retrieval failed.');
			});
		}
	}
	
	/* Function For LOGIN */
	$scope.login_button = function(){
		if($scope.loginForm.$valid){
			userPostService.postData(API_URL+'user/login', $scope.formData).then(function (response){
				if(response.message === "success"){
					$timeout(function () {
						$scope.loginSuccess = true;
					}, 0);
					$timeout(function () {
						$scope.loginSuccess = false;
						localStorage.setItem("id", response.id); /* set id in localStorage*/
						localStorage.setItem("name", response.name); /* set name in localStorage */
						$(location).attr('href', '#/dashboard');
					}, 4000);
				}else if(response.message === "email password error"){
					$timeout(function () {
						$scope.emailPasswordError = true;
					}, 0);
					$timeout(function () {
						$scope.emailPasswordError = false;
					}, 5000);
				}else if(response.message === "active inactive"){
					$timeout(function () {
						$scope.activeInactive = true;
					}, 0);
					$timeout(function () {
						$scope.activeInactive = false;
					}, 5000);
				}else if(response.message === "email verified error"){
					$timeout(function () {
						$scope.emailVerifiedError = true;
					}, 0);
					$timeout(function () {
						$scope.emailVerifiedError = false;
					}, 5000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
}]);

/* USER DASHBOARD */
app.controller('dashboardCtrl', ['$scope', '$http', '$timeout', 'getService', function($scope, $http, $timeout, getService) {
	var name = localStorage.getItem("name");
	$scope.name = name;
}]);

/* Forget Password Controller */
app.controller('forgotPasswordCtrl', ['$scope', '$location', '$http', '$timeout', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userPostService, API_URL) { 	
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.emailNotMatch = false;
	$scope.emailVerifiedError = false;

	$scope.forgotPassword = function(){
		if($scope.forgotPasswordForm.$valid == true){
			userPostService.postData(API_URL+'user/forgetPassword', $scope.formData).then(function (response) {
				if(response.message === 'success'){
					$timeout(function () {
						$scope.successMsg = true;
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;
					}, 8000);
				}else if(response.message === 'error'){
					$timeout(function () {
						$scope.emailNotMatch = true;
					}, 0);
					$timeout(function () {
						$scope.emailNotMatch = false;
					}, 5000);
				}else if(response.message === 'email verified error'){
					$timeout(function () {
						$scope.emailVerifiedError = true;
					}, 0);
					$timeout(function () {
						$scope.emailVerifiedError = false;
					}, 5000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = true;
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}	
	}
}]);

/* Reset Password Controller */
app.controller('resetPasswordCtrl', ['$scope', '$location', '$http', '$timeout', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userPostService, API_URL) { 
	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.passErrorMsg = false;
	$scope.tokenExpire = false;
	$scope.resetPassSucc = false;
	$scope.formData.email = $location.search().email;
	$scope.formData.token = $location.search().token;
	$scope.reset_button = function(){
		if($scope.resetForm.$valid == true){
			if($scope.formData.cNewPassword == $scope.formData.password){
				userPostService.postData(API_URL+'user/passwordReset', $scope.formData).then(function (response) {
					if(response.message === 'success'){
						$timeout(function () {
							$scope.resetPassSucc = true;
						}, 0);
						$timeout(function () {
							$scope.resetPassSucc = false;
							$(location).attr('href', '#/login');
						}, 4000);
					}else if(response.message === 'error'){
						$timeout(function () {
							$scope.tokenExpire = true;
						}, 0);
						$timeout(function () {
							$scope.tokenExpire = false;
							$(location).attr('href', '#/forgotpassword');
						}, 4000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});
			}else{
				$timeout(function () {
					$scope.passErrorMsg = true;
				}, 0);
				$timeout(function () {
					$scope.passErrorMsg = false;
				}, 4000);
			}
		}else{
			$timeout(function () {
				$scope.errorMsg = true;
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}
	}
}]);

/* Change Password Controller */
app.controller('changePasswordCtrl', ['$scope', '$http', '$location', '$timeout', 'userPostService', 'API_URL', function($scope, $http, $location, $timeout, userPostService, API_URL) {
	$scope.allFieldError = false;	
	$scope.passNotMatch = false;
	$scope.oldPassError = false;
	$scope.changePassSucc = false;
	$scope.formData = {};
	$scope.formData.id = localStorage.getItem("id");
	$scope.change_password_button = function(){
		if($scope.changPasswordForm.$valid == true){
			if($scope.formData.cNewPassword == $scope.formData.newPassword){
				userPostService.postData(API_URL+'dashboard/changePassword', $scope.formData).then(function (response) {
					if(response.message === 'success'){
						$timeout(function () {
							$scope.changePassSucc = true;
						}, 0);
						$timeout(function () {
							$scope.changePassSucc = false;
							$(location).attr('href', '#/dashboard');
						}, 4000);
					}else if(response.message === 'error'){
						$timeout(function () {
							$scope.oldPassError = true;
						}, 0);
						$timeout(function () {
							$scope.oldPassError = false;
						}, 4000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});
			}else{
				$timeout(function () {
					$scope.passNotMatch = true;
				}, 0);
				$timeout(function () {
					$scope.passNotMatch = false;
				}, 4000);
			}
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}	
	}
}]);

/* Update Profile Controller */
app.controller('updateProfile', ['$scope', '$location', '$http', '$timeout', 'postService', 'API_URL', function($scope, $location, $http, $timeout, postService, API_URL) { 
	$scope.files = [];
	/* Set default value */
	$scope.profileData = {};
	$scope.profileDataImg = {};
	var id = localStorage.getItem("id");
	
	/* get DB data for edit */
	postService.postData(API_URL+'dashboard/editDetails', {'id':id}).then(function (response) {
		if(response.message === 'success'){
			$scope.profileData.name = response.data[0].name;
			$scope.profileData.email = response.data[0].email;
			$scope.profileData.phone = response.data[0].phone;

			$scope.profileData.date_of_birth = (new Date(response.data[0].date_of_birth),'yyyy-MM-dd');
			$scope.profileData.gender = response.data[0].gender;
			$scope.profileDataImg.image = response.data[0].image;
			
		}
	},function(response) {
		console.log('albums retrieval failed.');
	});
	
	/* Update the profile */
	$scope.allFieldError = false;
	$scope.successUpdateProfile = false;
	
	$scope.updateProfile = function(){
		if($scope.editProfileForm.$valid == true){
			$scope.profileData.id = localStorage.getItem("id");
			postService.postData(API_URL+'dashboard/updateDetails', $scope.profileData).then(function (response) {
				if(response.message === 'success'){
					$timeout(function () {
						$scope.successUpdateProfile = true;
					}, 0);
					$timeout(function () {
						$(location).attr('href', '#/profile');
						$scope.successUpdateProfile = false;
					}, 4000);
				}

			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}	
	}
}]);

/* My Account -> Profile Controller -> View Profile*/
app.controller('profile', ['$scope', '$http', 'userPostService', 'API_URL', function($scope, $http, userPostService, API_URL) {
	$scope.profileData = {};
	$scope.profileDataImg = {};
	var id = localStorage.getItem("id");
	userPostService.postData(API_URL+'dashboard/userDetails', {'id':id}).then(function (response){
		$scope.profileData.name = response.data[0].name;
		$scope.profileData.email = response.data[0].email;
		$scope.profileData.phone = response.data[0].phone;
		$scope.profileData.date_of_birth = response.data[0].date_of_birth;
		$scope.profileData.gender = response.data[0].gender;
		$scope.profileDataImg.image = response.data[0].image;
	},function(response) {
		console.log('albums retrieval failed.');
	});
}]);

/* Loguout Controller */
app.controller('logout', ['$scope', '$http', '$location', '$timeout', function($scope, $http, $location, $timeout) {
	$scope.logout = function(){
		localStorage.clear();
		$(location).attr('href', '#/login');
	}
}]);

/* Layout Controller & Home Page */
app.controller('layout', ['$scope', '$http', '$location', '$timeout', 'userGetService', 'userPostService', 'API_URL', '$rootScope', function($scope, $http, $location, $timeout, userGetService, userPostService, API_URL, $rootScope) {

	/* Service use for menu management and show data in side header */
	userGetService.getData(API_URL+'admin/menus').then(function (response) {
		$scope.res  = response.data;
	},function(response) {
		console.log('albums retrieval failed.');
	});
	
	/* Service use for checking active & inactive status of login user on state change */
	var id = localStorage.getItem("id");
	$scope.inactive  = $rootScope.inactive;
	$scope.inactive = false;
	userPostService.postData(API_URL+'dashboard/getStatus', {'id':id}).then(function (response){
		if(response.message === 'inactive'){
			localStorage.clear();
			$timeout(function () {
				$scope.inactive = true;
				$rootScope.inactive = true;
			}, 0);
			$timeout(function () {
				$scope.inactive = false;
				$rootScope.inactive = false;
				$(location).attr('href', '#/login');
			}, 5000);
		}
	},function(response) {
		console.log('albums retrieval failed.');
	});
}]);

/* Data Base Error Controller */
app.controller('update_dbCtrl', ['$scope', '$http', '$location', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $location, $timeout, userGetService, userPostService, API_URL) {

	$scope.allFieldError = false;
	$scope.successMsg = false;
	$timeout(function () {
		$scope.bdError = true;
	}, 0);
	$timeout(function () {
		$scope.bdError = false;
	}, 6000);
	$scope.update = function(){
		if($scope.dbUpdateForm.$valid){
			userPostService.postData(API_URL+'user/updateDb', $scope.formData).then(function (response) {
				if(response.message === 'success'){
					$timeout(function () {
						$scope.successMsg = true;
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;
						$(location).attr('href', '#/login');
					}, 4000);		
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);	
		}	
	}
}]);
