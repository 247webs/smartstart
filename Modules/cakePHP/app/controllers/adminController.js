/* ----------- Admin Login Controller ----------- */
app.controller('adminCtrl', ['$scope', '$location', '$http', '$timeout', 'getService', 'postService', 'API_URL', function($scope, $location, $http, $timeout, getService, postService, API_URL) { 

	/* Set Default fields values */
	$scope.formData = {};
	
	$scope.loginSuccess = false;
	$scope.emailPasswordError = false;
	$scope.allFieldError = false;
	
	$scope.forgotPasswordHide = false;
	
	$scope.formData.email = 'admin@gmail.com';
	$scope.formData.password = 'admin@123';
	$scope.formData.role = 2;

	/* Login function for admin */
	$scope.login_button = function(){
		if($scope.loginForm.$valid == true){
			postService.postData(API_URL+'admin/login', $scope.formData).then(function (response) {
				if(response.message === "success"){
					$timeout(function () {
						$scope.loginSuccess = true;
					}, 0);
					$timeout(function () {
						$scope.loginSuccess = false;
						$location.url("/admin/dashboard");
					}, 4000);
				}else if(response.message === "email password error"){
					$timeout(function () {
						$scope.emailPasswordError = true;
					}, 0);
					$timeout(function () {
						$scope.emailPasswordError = false;
					}, 4000);
				}else if(response.message === "email password error"){
					$timeout(function () {
						$scope.emailPasswordError = true;
					}, 0);
					$timeout(function () {
						$scope.emailPasswordError = false;
					}, 4000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}

	/*---------GET DATA FOR ADMIN DASHBOARD---------*/
	$scope.loadData = function(){
 		getService.getData(API_URL+'admin/users').then(function (response) {
			$scope.res = response.data;
		},function(responseData) {
			console.log('albums retrieval failed.');
		});
	}
	
	/* ---------- User ACTIVE/INACTIVE by Admin ----------- */
	/* Click on chekbox */
	$scope.Status = function(id, $index){
		$scope.formData.hiddenid = id;
	}
	
	/* Click on YES button in checkbox */
	$scope.changedStatus = function(){
		postService.postData(API_URL+'admin/changeStatus', $scope.formData).then(function (response) {
			if(response.message === 'success'){
				$timeout(function () {
					$scope.userActiveInactive = true;
				}, 0);
				$timeout(function () {
					$scope.userActiveInactive = false;
					$(location).attr('href', '#/admin/dashboard')
				}, 4000);
			}
		},function(response) {
				console.log('albums retrieval failed.');
		});
	}
	
	/* Click on NO button in checkbox */
	$scope.toggleNo = function(){
		if($scope.formData.hiddenStatus == '0'){
			getService.getData(API_URL+'admin/users').then(function (response) {
				$scope.res  = response.data;
			},function(responseData) {
				console.log('albums retrieval failed.');
			});
		}else{
			getService.getData(API_URL+'admin/users').then(function (response) {
				$scope.res  = response.data;
			},function(responseData) {
				console.log('albums retrieval failed.');
			});
		}
	}
}]);

/* ----------- EMAIL TEMPLATE CONTROLLER ----------- */
app.controller('email_templateController', ['$scope', '$location', '$http', '$timeout','postService', 'getService','API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
	/* Set Default Value */
	$scope.formData = {};
	$scope.formData.sign_up = 'Signup';
	$scope.formData.forgot_password = 'Forgot Password';
	$scope.formData.change_password = 'Change Password';
	$scope.successCreate = false;
	$scope.successUpdate = false;
	$scope.allFieldError = false;
	
	$scope.email_template = function(){
		if($scope.email_template_form.$valid == true){
			postService.postData(API_URL+'admin/createTemplate', $scope.formData).then(function (responseData) {
				if(responseData.message === "success"){
					$timeout(function () {
						$scope.successCreate = true;
					}, 0);
					$timeout(function () {
						$scope.successCreate = false;
					}, 4000);	  
				}
				else if(responseData.message==="update success"){
					$timeout(function () {
						$scope.successUpdate = true;
					}, 0);
					$timeout(function () {
						$scope.successUpdate = false;
					}, 4000);
				}
			},function(responseData) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
	
	/*Show email template data*/
	$scope.loadEmailTemplate  = function(){
		getService.getData(API_URL+'admin/emailTemplates').then(function (response) {
			if(response.message === 'success' && response.data.length != 0){
				$scope.formData.sign_up = response.data[0].template_title;
				$scope.formData.sign_up_content = response.data[0].content;
				
				$scope.formData.forgot_password = response.data[1].template_title;
				$scope.formData.forgot_password_content = response.data[1].content;
				
				$scope.formData.change_password = response.data[2].template_title;
				$scope.formData.change_password_content = response.data[2].content;
			}
		},function(responseData) {
			console.log('albums retrieval failed.');
		});
	}
}]);

/* ----------- MENU MANAGEMENT CONTROLLER ----------- */
app.controller('menu_managementCtrl', ['$scope', '$location', '$http', '$timeout', 'getService', 'postService', 'API_URL', function($scope, $location, $http, $timeout, getService, postService,API_URL) {
	/* Set Default Value */
	$scope.formData = {};
	$scope.successCreate = false;
	$scope.successDelete = false;
	$scope.allFieldError = false;
	
	/* Get Data For Menu List */
	$scope.menuLoadData = function(){
		getService.getData(API_URL+'admin/menus').then(function (response) {
			$scope.res  = response.data;
		},function(response) {
			console.log('albums retrieval failed.');
		});	
	}
	
	/* Create The Menu(insert) */
	$scope.create_menu = function(){
		if($scope.menu_management_form.$valid){
			postService.postData(API_URL+'admin/createMenu', $scope.formData).then(function (response) {
				if(response.message === "success"){
					$timeout(function () {
						$scope.successCreate = true;
					}, 0);
					$timeout(function () {
						$scope.successCreate = false;
						$(location).attr('href', '#/admin/menu_list');
					}, 4000);	  
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
	
	/* Edit The Menu */
	$scope.edit = function(id){
		postService.postData(API_URL+'admin/editMenus', {'id':id}).then(function (response){
			$scope.formData.menu_name = response.data[0].menu_name;
			$scope.formData.menu_type = response.data[0].menu_type;
			$scope.formData.menu_url = response.data[0].menu_url;
			$scope.formData.hidden_id = id;
		},function(response) {
			console.log('albums retrieval failed.');
		});
	}
	
	/* Update The Menu */
	$scope.updateSuccess = false;
	$scope.update = function(){
		if($scope.menu_management_edit_form.$valid == true){
			postService.postData(API_URL+'admin/updateMenus', $scope.formData).then(function (response) {
				if(response.message == 'success'){
					$timeout(function () {
						$scope.updateSuccess = true;
					}, 0);
					$timeout(function () {
						$scope.updateSuccess = false;
						getService.getData(API_URL+'admin/menus').then(function (response) {
						$scope.res  = response.data;
						},function(response) {
							console.log('albums retrieval failed.');
						});
						$('#myEditModal').modal('toggle');
					}, 4000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
	
	/* Delete The Menu */
	$scope.delete = function(id,$index){
		$scope.formData.hiddenid = id;	
	}
	
	$scope.deleteYes = function(){
		postService.postData(API_URL+'admin/deleteMenu', $scope.formData).then(function (response) {
			if(response.message === 'success'){
				$timeout(function () {
					$scope.successDelete = true;
				}, 0);
				$timeout(function () {
					$scope.successDelete = false;
					getService.getData(API_URL+'admin/menus').then(function (response) {
						$scope.res  = response.data;
					},function(response) {
						console.log('albums retrieval failed.');
					});	
				}, 6000);
			}
		},function(response) {
			console.log('albums retrieval failed.');
		});
	}
	
}]);

/* ---------- CMS MANAGEMENT CONTROLLER ---------- */
app.controller('cms_managementCtrl', ['$scope', '$location', '$http', '$timeout', 'postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) { 
	/* Set default fields */
	$scope.formData = {};
	$scope.pageSuccess = false;
	$scope.pageUnique = false;
	$scope.allFieldError = false;
	
	/* Get Data For Page/CMS List */
	$scope.cmsLoadData = function(){
		getService.getData(API_URL+'admin/pages').then(function (response) {
			$scope.res  = response.data;
		},function(response) {
			console.log('albums retrieval failed.');
		});
	}
	
	/* Create Page (Insert) */
	$scope.create_page_button = function(){
		if($scope.cms_management.$valid == true){
			postService.postData(API_URL+'admin/cmsManagement', $scope.formData).then(function (response) {
				if(response.message === 'success'){
					$timeout(function () {
						$scope.pageSuccess = true;
					}, 0);
					$timeout(function () {
						$scope.pageSuccess = false;
						$(location).attr('href', '#/admin/pagelist');
					}, 4000);
				}else if(response.message === 'unique pageName'){
					$timeout(function () {
						$scope.pageUnique = true;
					}, 0);
					$timeout(function () {
						$scope.pageUnique = false;
					}, 4000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
	
	/* Edit CMS/Page */
	$scope.edit = function(id){
		postService.postData(API_URL+'admin/editCmsManagement', {'id':id}).then(function (response){
			$scope.formData.page_name = response.data[0].page_name;
			$scope.formData.page_content = response.data[0].page_content;
			$scope.formData.hidden_id = id;
		},function(response) {
			console.log('albums retrieval failed.');
		});
	}
	
	/* Update CMS/Page */
	$scope.updateSuccess = false;
	
	$scope.update = function(){
		if($scope.cms_managementUpdate.$valid == true){
			postService.postData(API_URL+'admin/updateCmsManagement', $scope.formData).then(function (response) {
				if(response.message == 'success'){
					$timeout(function () {
						$scope.updateSuccess = true;
					}, 0);
					$timeout(function () {
						$scope.updateSuccess = false;
						getService.getData(API_URL+'admin/pages').then(function (userData) {
						$scope.res  = userData.data;
						},function(response) {
							console.log('albums retrieval failed.');
						});
						$('#myEditModal').modal('toggle');
					}, 4000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);
		}
	}
	
	/* Delete CMS/Page */
	$scope.delete = function(id,$index){
		$scope.formData.hiddenid = id;
	}
	
	$scope.deleteMsg = false;
	
	$scope.deleteYes = function(){
		postService.postData(API_URL+'admin/deleteCms', $scope.formData).then(function (response) {
			if(response.message === 'success'){
				$timeout(function () {
					$scope.deleteMsg = true;
				}, 0);
				$timeout(function () {
					$scope.deleteMsg = false;
					getService.getData(API_URL+'admin/pages').then(function (response) {
						$scope.res  = response.data;
					},function(response) {
						console.log('albums retrieval failed.');
					});
				}, 4000);
			}
		},function(response) {
			console.log('albums retrieval failed.');
		});
	}
	
}]);

/* CMS MANAGEMENT CONTROLLER FOR PAGE VIEW */
app.controller('staticCtrl', ['$scope', '$location', '$http', '$timeout','postService', 'getService', 'API_URL', function($scope, $location, $http, $timeout, postService, getService, API_URL) {
	$scope.formData = {};
	var page_name = $location.absUrl().split('?')[1];

	postService.postData(API_URL+'admin/contentData', {'page_name' : page_name}).then(function (response) {
		if(response.message === 'success'){
			$scope.myText  = response.data[0].page_content;
		}
	},function(response) {
		console.log('albums retrieval failed.');
	});		
}]);

/* HTML DATA BIND */
angular.module('app').filter('to_trusted', ['$sce', function($sce){
	return function(text) {
		return $sce.trustAsHtml(text);
	};
}]);
