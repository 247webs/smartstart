<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_insert_user_table extends CI_Migration {

	public function up()
	{
		$fields = array(
			'`id` int(11) NOT NULL',
			'`name` varchar(100) NOT NULL',
			'`role` int(11) DEFAULT \'1\'',
			'`status` int(11) NOT NULL DEFAULT \'1\'',
			'`is_email_verified` int(11) NOT NULL DEFAULT \'0\'',
			'`email` varchar(300) NOT NULL',
			'`password` varchar(300) NOT NULL',
			'`token` varchar(300) DEFAULT NULL',
			'`phone` int(10) DEFAULT NULL',
			'`date_of_birth` date DEFAULT NULL',
			'`gender` varchar(7) DEFAULT NULL',
			'`image` text DEFAULT NULL',
			'`created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'`modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP'
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users');

		$data = array(
			array(
				'id' => "1",
				'name' => "admin@gmail.com",
				'role' => "2",
				'status' => "1",
				'is_email_verified' => "1",
				'email' => "admin@gmail.com",
				'password' => "Any@12345",
			),
		);

		$this->db->insert_batch('users', $data);
		$this->db->query("ALTER TABLE `users` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;");
	}

	public function down()
	{
		$this->dbforge->drop_table('users');
	}

}