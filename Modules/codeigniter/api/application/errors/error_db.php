<!-- 
<?php
	// if(strpos($message, 'Unable to connect to your database server using the provided settings.')){
		?>
		<!DOCTYPE html>
		<html>
		<head>
			<title>DB Connection Error</title>

			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

			<style type="text/css">
				/*.formContainer{
					padding: 0px 15px 14px;
				}*/

				.rowAlert{
					height: 70px;
				}

				.alert{
					display: none;
					width: 95%;
				}
				.formContainer{
					background-color: #fff;
				    margin: 10px 0px;
				    min-height: 500px;
				    box-shadow: 0px 1px 5px 0px;
				    width: 80%;
				    padding: 1px;
				}

				.logoClass{
					text-align: center;
					margin-top: 15px;
				}
				body{
					background-color: #f3f3f3;
				}
				.labels{
					color: #716a6a;
				    padding-left: 20px;
				    vertical-align: -webkit-baseline-middle;
				}
				.info{
					color: #929292;
					text-align: left;
				}
				.formRow{
					margin-top: 10px;
				}
			</style>
		</head>
		<body>
			<div class="container" align="center">
				<div class="row logoClass">
					<img src="<?php // echo base_url(); ?>assets/images/logo.png" width="80px;"><!-  Image Logo->
				</div>
				<div class="formContainer">
					<div class="row">
						<h4 style="text-align: center; margin-top: 20px;">We are unable to connect to your database server using provided settings.</h4>
					</div>
					<div class="row rowAlert">
						<div class="alert alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong> Indicates a successful or positive action.
						</div>
					</div>
					<form action="" method="POST" id="saveDatabaseSettings">
						<div class="row formRow">
							<div class="col-sm-2" align="left">
								<label class="labels">Database Name</label>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<input type="text" name="DatabaseName" id="DatabaseName" class="form-control" placeholder="Database Name">
								</div>
							</div>
							<div class="col-sm-6">
								<h5 class="info">The name of the database you want to run your application in.</h5>
							</div>
						</div>
						<div class="row formRow">
							<div class="col-sm-2" align="left">
								<label class="labels">User Name</label>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<input type="text" name="UserName" id="UserName" class="form-control" placeholder="MySQL/MySQLi User Name">
								</div>
							</div>
							<div class="col-sm-6">
								<h5 class="info">Your MySQL/MySQLi User Name.</h5>
							</div>
						</div>
						<div class="row formRow">
							<div class="col-sm-2" align="left">
								<label class="labels">Password</label>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<input type="password" name="Password" id="Password" class="form-control" placeholder="Password">
								</div>
							</div>
							<div class="col-sm-6">
								<h5 class="info">Your MySQL/MySQLi Password.</h5>
							</div>
						</div>
						<div class="row formRow">
							<div class="col-sm-2" align="left">
								<label class="labels">Database Host</label>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<input type="text" name="HostName" id="HostName" class="form-control" placeholder="Database Host">
								</div>
							</div>
							<div class="col-sm-6">
								<h5 class="info">You should be able to get this info from your web host.</h5>
							</div>
						</div>					
						<div class="row formRow">
							<div class="col-sm-12">
								<button class="btn" id="SaveDetails">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div> -->

			<!-- <div class="formContainer">
				<div class="alert alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> Indicates a successful or positive action.
				</div>
				<form action="" method="POST" id="saveDatabaseSettings">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Host Name</label>
								<input type="text" name="HostName" id="HostName" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Database Name</label>
								<input type="text" name="DatabaseName" id="DatabaseName" class="form-control">					
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>User Name</label>
								<input type="text" name="UserName" id="UserName" class="form-control">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="Password" id="Password" class="form-control">					
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-primary" id="SaveDetails">Save</button>
						</div>
					</div>
				</form>
			</div> ->
			<script type="text/javascript">
			$(document).ready(function(){
				var base_url = '<?php // echo base_url(); ?>';
				$("#SaveDetails").click(function(e){
					e.preventDefault();
					var Valid = true;

					if($("#HostName").val() == '' || $("#HostName").val() == undefined){
						Valid = false;
						$("#HostName").css('border-color', '#c10202');
					}else{
						$("#HostName").css('border-color', '#ccc');
					}

					if($("#DatabaseName").val() == '' || $("#DatabaseName").val() == undefined){
						Valid = false;
						$("#DatabaseName").css('border-color', '#c10202');
					}else{
						$("#DatabaseName").css('border-color', '#ccc');
					}

					if($("#UserName").val() == '' || $("#UserName").val() == undefined){
						Valid = false;
						$("#UserName").css('border-color', '#c10202');
					}else{
						$("#UserName").css('border-color', '#ccc');
					}

					if($("#Password").val() == '' || $("#Password").val() == undefined){
						Valid = false;
						$("#Password").css('border-color', '#c10202');
					}else{
						$("#Password").css('border-color', '#ccc');
					}

					if(Valid){
						// alert('valid');
						$(".alert").removeClass('alert-success');
						$(".alert").removeClass('alert-danger');
						$(".alert").html('');
						$.ajax({
							method: 'POST',
							url: base_url+'checkAndSaveDBDetails.php',
							data: $("#saveDatabaseSettings").serialize(),
							success: function(data){
								// console.log('change');
								if(data == 'Success'){
									// do if success
									$(".alert").addClass('alert-success');
									$(".alert").show();
									$(".alert").html('Database details has been updated successfully. <br> <b>Note: Rename the file name from <i>checkAndSaveDBDetails</i> to whatever you want, found at root directory.</b>');
									setTimeout(function(){ location.reload(); }, 5000);
								}else{
									$(".alert").addClass('alert-danger');
									$(".alert").show();
									$(".alert").html('Some of your given details is incorrect, Please check and try again.');
									// do this if not correct
								}
							}
						})
					}
				})
			})

		</script>
		</body>
		</html>
		
		

		<?
	// }else{ 
		?>
	-->
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<title>Database Error</title>


		<style type="text/css">

		::selection{ background-color: #E13300; color: white; }
		::moz-selection{ background-color: #E13300; color: white; }
		::webkit-selection{ background-color: #E13300; color: white; }

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			-webkit-box-shadow: 0 0 8px #D0D0D0;
		}

		p {
			margin: 12px 15px 12px 15px;
		}

		</style>
		</head>
		<body>
			<div id="container">
				<h1><?php echo $heading; ?></h1>
				<?php echo $message; ?>
			</div>
		</body>
		</html>
<?	// }	?>