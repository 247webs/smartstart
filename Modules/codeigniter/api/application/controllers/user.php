<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('model');
		$this->load->helper(array('url','form','html','text','string','file'));
	}
/* Signup Function */
	public function signup(){
		$where = ['email' => $this->input->post('email')];
		$emailExist = $this->model->GetData('default', 'users', $where, 'num');
		if(empty($emailExist)){			
			/* Generate a random number */			
			$token = rand();
			$insert = [
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
				'image' => 'defa.jpg',				
				'token' => $token
			];
			$user_id = $this->model->insertData('default', 'users', $insert, TRUE);				/* Get data for signup email template */			
			$where = ['template_title' => 'Signup'];			
			$emailSignup = $this->model->GetData('default', 'email_templates', $where, 'row');	$emailSignupTemp = $emailSignup->content;			
			$templateRes = sprintf($emailSignupTemp,$this->input->post('name'));			$email = $this->input->post('email');
			
			$base_url = $_SERVER['HTTP_REFERER'];
			
			$url = "<a href='".$base_url."#/login?email=".$email.'&token='.$token."'>Click here for login</a>";			
			/* Email Function */			
			$to = $email;			
			$subject = "Confirm Your Registration";			
			$txt = $templateRes.'<br/>'.$url;			
			$headers = "From: testshivam000@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";			
			mail($to,$subject,$txt,$headers);
			
			$returnArray = ['code'=>200, 'message'=>'success'];
		}else{
			$returnArray = ['code'=>400, 'message'=>'error', 'error_message'=>'This email is alerady exist.'];
		}
		echo json_encode($returnArray);
	}		
/* IS EMAIL VERIFIED FUNCTION */	
	public function emailVerified(){
		$data = $this->input->post();		
		$email = $data['email'];		
		$token = $data['token'];		
		$where = ['email' => $email];		
		$users = $this->model->GetData('default', 'users', $where, 'row');		
		if(!empty($users)){			
			$where = ['id' => $users->id];			
			$updateData = [				
				'token' => '',				
				'is_email_verified' => 1			
			];			
			$this->model->updateData('default', 'users', $updateData, $where);		
		}		
	}
/* Login Function */
	public function login(){
		$where = [
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password')
		];		
		$authUser = $this->model->GetData('default', 'users', $where, 'row');
		if(empty($authUser)){
			/* Wrong password or email error */
			$returnArray = ['code'=>400, 'message'=>'error', 'error_message'=>'Email or Password does not match.'];
		}else{			
			if($authUser->is_email_verified == 1 & $authUser->token == ''){
				if($authUser->status == 1){	
					$returnArray = ['code'=>200, 'message'=>'success', 'id'=>$authUser->id, 'name'=>$authUser->name, 'success_message'=>'Login has been successfull.'];
				}else{
					$returnArray = ['code'=>400, 'message'=>'error', 'error_message'=>'You are disabled by admin please contact administrator.'];
				}			
			}else{				
				$returnArray = ['code'=>400, 'message'=>'error', 'error_message'=>'Your email address is not verified yet, Firstly verified your email address.'];			
			}
		}
		echo json_encode($returnArray);
	}
/* Forgot Password Function */
	public function forgetPassword(){
		$whereEmail = ['email' => $this->input->post('email')];
		$userExist = $this->model->GetData('default', 'users', $whereEmail, 'row');
		if(!empty($userExist)){
			if($userExist->is_email_verified == 1){
				$token = rand();
				$email = $this->input->post('email');
				/* Insert token by update */
				$updateData = ['token' => $token];
				$this->model->updateData('default', 'users', $updateData, $whereEmail);
				
				/* Get data for signup email template */			
				$where = ['template_title' => 'Forgot Password'];			
				$emailForgotPass = $this->model->GetData('default', 'email_templates', $where, 'row');	
				$emailForgotPassTemp = $emailForgotPass->content;
				$templateRes = sprintf($emailForgotPassTemp, $userExist->name);
				
				$base_url = $_SERVER['HTTP_REFERER'];
				
				$url = "<a href='".$base_url."#/resetpassword?email=".$email.'&token='.$token."'>Click here for Reset Password</a>";

				/* Email Function */			
				$to = $email;			
				$subject = "Reset Password";			
				$txt = $templateRes.'<br/>'.$url;			
				$headers = "From: testshivam000@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";			
				mail($to,$subject,$txt,$headers);
				
				$returnArray = ['code'=>200, 'message'=>'success', 'success_message'=>'Reset Password link is send to your email address.'];
			}else{
				$returnArray = ['code'=>400, 'message'=>'error', ''=>'Your email address is not verified yet , Firstly verified your email address.'];
			}
		}else{
			$returnArray = ['code'=>400, 'message'=>'error', 'error_message'=>'Email does not match.'];
		}
		echo json_encode($returnArray);
	}
/* Reset Password Function */
	public function passwordReset(){
		$whereEmail = ['email' => $this->input->post('email')];
		$checkToken = $this->model->GetData('default', 'users', $whereEmail, 'row');
		if($checkToken->token == $this->input->post('token')){
			$updateData = [
				'password' => $this->input->post('password'),
				'token' => ''
			];
			$this->model->updateData('default', 'users', $updateData, $whereEmail);
			$returnArray = ['code'=>200, 'message'=>'success', 'success_message'=>'Password has been reset successfully.'];
		}else{
			$returnArray = ['code'=>400, 'message'=>'error', 'error_message'=>'Your token is expaire.'];
		}
		echo json_encode($returnArray);
	}
	
/* Get login user details for view */
	public function userDetails(){
		$where = ['id' => $this->input->post('id')];
		$getUser = $this->model->GetData('default', 'users', $where, 'row');
		$getUser->flag = "codeigniter";
		if(!empty($getUser)){
			echo json_encode($getUser);
		}
	}
/* Get login user details for edit */
	public function editDetails(){
		$where = ['id' => $this->input->post('id')];
		$getUser = $this->model->GetData('default', 'users', $where, 'row');
		$getUser->message = "success";
		$getUser->code = 200;
		if(!empty($getUser)){
			echo json_encode($getUser);
		}
	}
	
/* Update login user details */
	public function updateDetails(){
		if(isset($_POST) && !empty($_POST)){
			$updateData = [
				'name' => $this->input->post('name'),
				'phone' => $this->input->post('phone'),
				'date_of_birth' => date('Y-m-d', strtotime($this->input->post('date_of_birth'))),
				'gender' => $this->input->post('gender'),
			];
			$where = [
				'id' => $this->input->post('id')
			];
			$this->model->updateData('default', 'users', $updateData, $where);
			$returnArray = ['code'=>200, 'message'=>'success', 'success_message'=>'Profile has been updated successfully.'];
			echo  json_encode($returnArray);
		}
	}

/* Upload image function */
	public function updateImage(){
		$where = ['id' => $this->input->post('uid')];
		$config['upload_path'] = "./assets/uploads/";
		$config['file_name'] = time().'-'.$_FILES['file']['name'];
		$config['allowed_types'] = 'gif|jpg|png';

		$this->load->library('upload', $config);
		if($this->upload->do_upload('file')){
			$updateData = ['image' => $config['file_name']];
			$this->model->updateData('default', 'users', $updateData, $where);
			$returnArray = ['code' => 200, 'message' => 'success'];
			echo json_encode($returnArray);
		}
	}
	
/* Change Password Function */
	public function changePassword(){
		$changePasswordData = [
			'id' => $this->input->post('id'),
			'oldPassword' => $this->input->post('oldPassword'),
			'newPassword' => $this->input->post('newPassword'),
		];
		$data = $this->model->ChangePassword($changePasswordData);
		if($data[0]->password == $this->input->post('oldPassword')){
			$getdata = $this->model->updateChangePassword($changePasswordData);
			if($getdata == 1){
				/* Get data for signup email template */			
				$where = ['template_title' => 'Change Password'];			
				$emailChangePass = $this->model->GetData('default', 'email_templates', $where, 'row');	
				$emailChangePassTemp = $emailChangePass->content;			
				$templateRes = sprintf($emailChangePassTemp, $data[0]->name);		
				$email = $data[0]->email;
					
				/* Email Function */			
				$to = $email;			
				$subject = "Change Password";			
				$txt = $templateRes;			
				$headers = "From: testshivam000@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";			
				mail($to,$subject,$txt,$headers);

				$returnArray = ['code'=>200, 'message'=>'success', 'success_message'=>'Password has been change successfully.', 'data'=>''];
			}
		}else{
			$returnArray = ['code'=>400, 'message'=>'error', 'error_message'=>'Old password does not match.'];
		}
		echo json_encode($returnArray);
	}
	
	/* Database error */
	public function updateDb(){
		die('updateDb error, but not here');
	}
}

