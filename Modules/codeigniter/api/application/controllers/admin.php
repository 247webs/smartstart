<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('model');
	}
/* Admin Login Function */
	public function login(){
		$where = [
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'role' => 2
		];		
		$authUser = $this->model->GetData('default', 'users', $where, 'row');
		if(empty($authUser)){
			/* Wrong password or email error */
			$returnArray = ['code' => 400, 'message' => 'error', 'error_message'=>'Email or Password does not match.'];
		}else{
			$returnArray = ['code' => 200, 'message' => 'success', 'id' => $authUser->id, 'name' => $authUser->name, 'success_message'=>'Login has been successfull.'];
		}
		echo json_encode($returnArray);
	}
/* Get all users for admin dashboard */
	public function users(){
		$where = ['role' => 1];
		$allUsersData = $this->model->GetData('default', 'users', $where, 'all');
		if(!empty($allUsersData)){
			$returnArray = ['code' => 200, 'data' => $allUsersData];
		}else{
			$returnArray = ['code' => 400, 'message' => 'No Data Found'];
		}
		echo json_encode($returnArray);
	}
/* USER STATUS Active/Inactive */
	public function changeStatus(){
		$whereId = ['id' => $this->input->post('hiddenid')];
		$statusData = $this->model->GetData('default', 'users', $whereId, 'row');
		if($statusData->status == 1){
			$updateStatus = ['status' => 0];
			$this->model->updateData('default', 'users', $updateStatus, $whereId);
			$returnArray = ['code' => 200, 'message' => 'success'];
		}else if($statusData->status == 0){
			$updateStatus = ['status' => 1];
			$this->model->updateData('default', 'users', $updateStatus, $whereId);
			$returnArray = ['code' => 200, 'message' => 'success'];
		}
		echo json_encode($returnArray);
	}
/* ----- EMAIL TEMPLATE ----- */
	/* Get Email Template Data */
	public function emailTemplates(){
		$result = $this->model->GetData('default', 'email_templates', $where=array(), 'all');
		$returnArray = ['code' => 200, 'message' => 'success', 'data' => $result];
		echo json_encode($returnArray);
	}
	/* Create & Update Email Template */
	public function createTemplate(){
		$data = $this->input->post();
		$sign_up = $data['sign_up'];
		$forgot_password = $data['forgot_password'];
		$change_password = $data['change_password'];

		$sign_up_content = $data['sign_up_content'];
		$forgot_password_content = $data['forgot_password_content'];
		$change_password_content = $data['change_password_content'];
		
		$createTemplate = [
			['template_title' => $sign_up, 'content' => $sign_up_content],
			['template_title' => $forgot_password, 'content' => $forgot_password_content],
			['template_title' => $change_password, 'content' => $change_password_content]
		];
		$result = $this->model->GetData('default', 'email_templates', $where=array(), 'all');
		if(empty($result)){
			foreach($createTemplate as $value){
				$user_id = $this->model->insertData('default', 'email_templates', $value, TRUE);
			}
			$returnArray = ['code' => 200, 'message' => 'success', 'success_message'=>'Template has been created successfully.'];
		}else{
			$this->model->updateData('default', 'email_templates', ['content' => $sign_up_content], $where=['id' => 1]);
			
			$this->model->updateData('default', 'email_templates', ['content' => $forgot_password_content], $where=['id' => 2]);
			
			$this->model->updateData('default', 'email_templates', ['content' => $change_password_content], $where=['id' => 3]);
			$returnArray = ['code' => 200, 'message' => 'success', 'success_message'=>'Template has been updated successfully.'];
		}
		echo json_encode($returnArray);
	}	

/* ----- CMS/PAGE MANAGEMENT ----- */
	/* Insert cms data */
	public function cmsManagement(){
		$where = ['page_name' => $this->input->post('page_name')];
		$result = $this->model->GetData('default', 'page_content_cms', $where, 'row');

		if(empty($result)){
			$insert = [
				'page_name' => $this->input->post('page_name'),
				'page_content' => $this->input->post('page_content')
			];
			$user_id = $this->model->insertData('default', 'page_content_cms', $insert, TRUE);
			$returnArray = ['code' => 200, 'message' => 'success', 'success_message'=>'Page has been created successfully.'];
		}else{
			$returnArray = ['statuscode' => 400, 'message' => 'error', 'error_message'=>'This page name is already exist.'];
		}
		echo json_encode($returnArray);
	}
	
	/* CMS/Page List */
	public function pages(){
		$result = $this->model->GetData('default', 'page_content_cms', $where=array(), 'all');
		$returnArray = ['code' => 200, 'message' => 'success', 'data' => $result, 'flag' => 'codeigniter'];
		echo json_encode($returnArray);
	}
	
	/* Edit CMS/Page */
	public function editCmsManagement(){
		$where = ['id' => $this->input->post('id')];
		$result = $this->model->GetData('default', 'page_content_cms', $where, 'all');
		$returnArray = ['code' => 200, 'message' => 'success', 'data' => $result, 'flag' => 'codeigniter'];
		echo json_encode($returnArray);
	}
	
	/* Update CMS/Page */
	public function updateCmsManagement(){
		$where = ['page_name' => $this->input->post('page_name')];
		$result = $this->model->GetData('default', 'page_content_cms', $where, 'row');
		if(empty($result)){
			$whereId = ['id' => $this->input->post('hidden_id')];
			$updateCms = [
				'page_name' => $this->input->post('page_name'),
				'page_content' => $this->input->post('page_content')
			];
			$this->model->updateData('default', 'page_content_cms', $updateCms, $whereId);
			$returnArray = ['code' => 200, 'message' => 'success', 'success_message'=>'Page has been updated successfully.'];
		}else{
			$returnArray = ['statuscode' => 400, 'message' => 'error', 'error_message'=>'This page name is already exist.'];
		}
		echo json_encode($returnArray);
	}
	
	/* Get content for page view */
	public function contentData(){
		$where = ['page_name' => $this->input->post('page_name')];
		$result = $this->model->GetData('default', 'page_content_cms', $where, 'all');
		$returnArray = ['code' => 200, 'message' => 'success', 'data' => $result, 'flag' => 'codeigniter'];
		echo json_encode($returnArray);
	}
	
	/* Delete CMS/Page */
	public function deleteCms(){
		$whereId = ['id' => $this->input->post('hiddenid')];
		$this->model->DeleteData('default', 'page_content_cms', $whereId, $delete=array());
		$returnArray = ['code' => 200, 'message' => 'success', 'success_message'=>'Page has been deleted successfully.'];
		echo json_encode($returnArray);
	}
	
/* ----- MENU MANAGEMENT ----- */
	/* Create Menu*/
	public function createMenu(){
		$insert = [
			'menu_name' => $this->input->post('menu_name'),
			'menu_type' => $this->input->post('menu_type'),
			'menu_url' => $this->input->post('menu_url')
		];
		$user_id = $this->model->insertData('default', 'menu_managements', $insert, TRUE);
		$returnArray = ['code' => 200, 'message' => 'success', 'success_message'=>'Menu has been created successfully.'];
		echo json_encode($returnArray);
	}
	
	/* Menu List */
	public function menus(){
		$result = $this->model->GetData('default', 'menu_managements', $where=array(), 'all');
		$returnArray = ['code' => 200, 'message' => 'success', 'data' => $result, 'flag' => 'codeigniter'];
		echo json_encode($returnArray);
	}
	
	/* Edit Menu*/
	public function editMenus(){
		$where = ['id' => $this->input->post('id')];
		$result = $this->model->GetData('default', 'menu_managements', $where, 'all');
		$returnArray = ['code' => 200, 'message' => 'success', 'data' => $result, 'flag' => 'codeigniter'];
		echo json_encode($returnArray);
	}
	
	/* UPdate Menu */
	public function updateMenus(){
		$whereId = ['id' => $this->input->post('hidden_id')];
		$updateMenu = [
			'menu_name' => $this->input->post('menu_name'),
			'menu_type' => $this->input->post('menu_type'),
			'menu_url' => $this->input->post('menu_url')
		];
		$this->model->updateData('default', 'menu_managements', $updateMenu, $whereId);
		$returnArray = ['code' => 200, 'message' => 'success', 'success_message'=>'Menu has been updated successfully.'];
		echo json_encode($returnArray);
	}
	
	/* Delete Menu*/
	public function deleteMenu(){
		$whereId = ['id' => $this->input->post('hiddenid')];
		$this->model->DeleteData('default', 'menu_managements', $whereId, $delete=array());
		$returnArray = ['code' => 200, 'message' => 'success', 'success_message'=>'Menu has been deleted successfully.'];
		echo json_encode($returnArray);
	}
	
	/* Check status of login user */
	public function getStatus(){
		$where = ['id' => $this->input->post('id')];
		$users = $this->model->GetData('default', 'users', $where, 'row');
		$status = $users->status;
		if($status == 1){
			$returnArray = ['statuscode' => 200, 'message' => 'active'];
		}else{
			$returnArray = ['statuscode' => 400, 'message' => 'inactive'];
		}
		echo json_encode($returnArray);
	}
}