<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$db = $this->load->database('default', true);

		if(!$this->db->table_exists('users')){
			$db = $this->load->database('default', true);
			$db->truncate('migrations'); 
			$this->load->library('migration');
			if(!$this->migration->current()){
				show_error($this->migration->error_string());
				die('migration fails');
			}
			$returnArray = array(
				'code' => 200
			);
		}else{
			$returnArray = array(
				'code' => 400
			);
		}

		echo json_encode($returnArray);
		// $data['users'] = $db->select('*')->from('users')->get()->result_array();

		// $this->load->view('welcome_message', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */