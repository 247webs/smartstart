<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}

/* Check/GetData => Email-already-exist, Forgot-password-email, Reset-password-email, admin-dashboard, active-inactive,  */
	public function GetData($dbName, $tableName, $where = array(), $return = "all", $select = '*', 
	$whereIN = array()){
		$db = $this->load->database($dbName, true);
		$db->select($select);
		if(!empty($whereIN)){
			$db->where_in($where, $whereIN);
		}elseif(!empty($where)){
			$db->where($where);
		}
		$qry = $db->get($tableName);
		$returnData;
		if($return == 'all'){
			$returnData = $qry->result_array();
		}elseif($return == 'num'){
			$returnData = $qry->num_rows();
		}elseif($return == 'row'){
			$returnData = $qry->row();
		}
		return $returnData;
	}

/* Insert => Signup-Data, Email-template-data,  */
	public function insertData($dbName, $tableName, $insert, $lastID = false){
		$db = $this->load->database($dbName, true);
		$db->insert($tableName, $insert);
		if($lastID){
			return $db->insert_id();
		}
	}
	
/* Update => Forgot-password-token, Forgot-password-password,  */
	public function updateData($dbName, $tableName, $update, $where = array()){
		$db = $this->load->database($dbName, true);
		$db->where($where);
		$db->update($tableName, $update);
	}

/* Delete Function */
	public function DeleteData($dbName, $tableName, $where, $delete=array()){
		$db = $this->load->database($dbName, true);
		if(is_array($delete) && !empty($delete)){
			$db->where_in($where, $delete);
		}else{
			$db->where($where);
		}
		$db->delete($tableName);
	}

/* --- Change Password Model Functions --- */	
	public function ChangePassword($changePasswordData){
		$this->db->where('id', $changePasswordData['id']);
		$query = $this->db->get('users');
		$result = $query->result();
		return $result;
	}
	
	public function updateChangePassword($changePasswordData){
		$this->db->set('password', $changePasswordData['newPassword']);
		$this->db->where('id', $changePasswordData['id']);
		$this->db->update('users');
		return true;
	}
}

