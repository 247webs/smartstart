'use strict';
var app = angular.module('app', ['ui.router', 'ngStorage', 'number']);
angular.module('app').constant('API_URL', "http://localhost/Projects Backup/codeigniter/api/");
app.config(function($stateProvider, $urlRouterProvider){
    $stateProvider
		.state('/', {
            url: '/',
			controller: 'layout',
            templateUrl: 'view/user/home.html',
        })
		.state('register', {
            url: '/register',
			controller: 'registerCtrl',
            templateUrl: 'view/user/register.html',
        })
		.state('thankYou', {
            url: '/thankYou',
			controller: 'loginController',
            templateUrl: 'view/user/thankyouView.html',
        })
		.state('login', {
            url: '/login',
			controller: 'loginController',
            templateUrl: 'view/user/login.html',
        })
		.state('admin', {
            url: '/admin',
			controller: 'adminCtrl',
            templateUrl: 'view/user/login.html',
        })
		.state('dashboard', {
            url: '/admin/dashboard',
			controller: 'adminCtrl',
            templateUrl: 'view/admin/adminDashboard.html',
        })
		.state('cms_management', {
            url: '/admin/cms_management',
			controller: 'cms_managementCtrl',
            templateUrl: 'view/admin/cms_management.html',
        })
		.state('pagelist', {
            url: '/admin/pagelist',
			controller : 'cms_managementCtrl',	
            templateUrl: 'view/admin/pagelist.html',
        })
		.state('static', {
            url: '/static',
			controller : 'staticCtrl',	
            templateUrl: 'view/admin/static.html',
        })
		.state('menu_list', {
            url: '/admin/menu_list',
			controller: 'menu_managementCtrl',
            templateUrl: 'view/admin/menu_list.html',
        })
		
		.state('create_menu', {
            url: '/admin/create_menu',
			controller: 'menu_managementCtrl',
            templateUrl: 'view/admin/create_menu.html',
        })
		.state('email_template_management', {
            url: '/admin/email_template_management',
			controller: 'email_templateController',
            templateUrl: 'view/admin/email_template.html',
        })
		.state('login/dashboard', {
            url: '/dashboard',
			controller : 'dashboardCtrl',
            templateUrl: 'view/dashboard/dashboard.html',
        })
		
		.state('login/profile', {
            url: '/profile',
			controller: 'profile',
            templateUrl: 'view/dashboard/myaccount.html',
        })
		
		.state('login/editprofile', {
            url: '/editprofile',
			controller : 'updateProfile',
            templateUrl: 'view/dashboard/editprofile.html',
        })
		.state('login/changePassword', {
            url: '/changePassword',
			controller : 'changePasswordCtrl',	
            templateUrl: 'view/dashboard/changePassword.html',
        })
		.state('login/forgotpassword', {
            url: '/forgotpassword',
			controller : 'forgotPasswordCtrl',	
            templateUrl: 'view/user/forgotPassword.html',
        })
		.state('login/resetpassword', {
            url: '/resetpassword',
			controller : 'resetPasswordCtrl',	
            templateUrl: 'view/user/resetpassword.html',
        })
		.state('update_db', {
            url: '/update_db',
			controller : 'update_dbCtrl',	
            templateUrl: 'view/user/update_db.html',
        })
		$urlRouterProvider.otherwise('/');
	});

/* USER POST SERVICE */
app.factory('userPostService', function($http, $q) {
	return {
		postData: function(API_URL, formData) {
			var deferred = $q.defer();
			$.ajax({
				type: 'POST',
				url: API_URL,
				dataType: "json",	
				data: formData,
				success: function(res) {
					deferred.resolve(res);
				}, 
				error : function(msg, code) {
					/* For database credentials error */
					if(msg.status == 500 && msg.responseText.search('Unable to connect to your database server using the provided settings.') != -1){
						$(location).attr('href', '#/update_db');
					}
					deferred.reject(msg);
				}
			});
			return deferred.promise;
		}
	};
});

/* USER GET SERVICE */
app.factory('userGetService', function($http, $q) {
	return {
		getData: function(API_URL) {
			var deferred = $q.defer();
			$.ajax({
				type: 'GET',
				url: API_URL,
				dataType: "json",	
				success: function(res) {
					deferred.resolve(res);
				}, 
				error : function(msg, code) {
					deferred.reject(msg);
				}
			});
			return deferred.promise;
		}
	};
});

/* ADMIN POST SERVICE */
app.factory('postService', function($http, $q) {
	return {
		postData: function(API_URL, formData) {
			var deferred = $q.defer();
			$.ajax({
				type: 'POST',
				url: API_URL,
				dataType: "json",	
				data: formData,
				success: function(res) {
					deferred.resolve(res);
				}, error : function(msg, code) {
					deferred.reject(msg);
				}
			});
			return deferred.promise;
		}
	};
});

/* ADMIN GET SERVICE */
app.factory('getService', function($http, $q) {
	return {
		getData: function(API_URL) {
			var deferred = $q.defer();
			$.ajax({
				type: 'GET',
				url: API_URL,
				dataType: "json",	
				success: function(res) {
					deferred.resolve(res);
				}, error : function(msg, code) {
					deferred.reject(msg);
				}
			});
			return deferred.promise;
		}
	};
});

/* UPLOAD IMAGE DIRECTIVE & FACTORY */
app.directive('myDirective', function (httpPostFactory, API_URL) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {
            element.bind('change', function () {
                var formData = new FormData();	
                formData.append('file', element[0].files[0]);
                formData.append('uid', localStorage.getItem("id"));
                httpPostFactory(API_URL+'user/updateImage', formData, function (callback) {
				});
            });
        }
    };
});
app.factory('httpPostFactory', function ($http) {
    return function (file, data, callback) {
        $http({
            url: file,
            method: "POST",
            data: data,
            headers: {'Content-Type': undefined}
        }).success(function (response) {
            callback(response);
        });
    };
});

/* DIRECTIVE FOR ONLY NUMBER */
angular.module('number', []).directive('number', function () {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function (scope, element, attrs, ctrl) {
			ctrl.$parsers.push(function (input) {
				if (input == undefined) return ''
				var inputNumber = input.toString().replace(/[^0-9]/g, '');
				if (inputNumber != input) {
					ctrl.$setViewValue(inputNumber);
					ctrl.$render();
				}
				return inputNumber;
			});
		}
	};
});

/* DIRECTIVE FOR DATE-PICKER */
app.directive('datepicker', function() {
	return {
		require: 'ngModel',
		link: function(scope, el, attr, ngModel) {
			$(el).datepicker({
				onSelect: function(dateText) {
					scope.$apply(function() {
						ngModel.$setViewValue(dateText);
					});
				}
			});
		}
	};
});