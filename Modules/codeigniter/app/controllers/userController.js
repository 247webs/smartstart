/* Register Controller */
app.controller('registerCtrl', ['$scope', '$location', '$http', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userGetService, userPostService, API_URL) {
	/* Set Default values */
	$scope.formData = {};
	$scope.errorMsg = false;
	$scope.successMsg = false;
	$scope.register_button = function(){
		if($scope.registerForm.$valid){
			if($scope.formData.password == $scope.formData.confirm_password){
				userPostService.postData(API_URL+'user/signup', $scope.formData).then(function (response){
					$scope.errorMsg = '';
					$scope.successMsg = '';
					if(response.message === "success"){
						$(location).attr('href', '#/thankYou');
					}else if(response.message === "error"){
						$timeout(function () {
							$scope.errorMsg = response.error_message;
						}, 0);
						$timeout(function () {
							$scope.errorMsg = false;
						}, 7000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});
			}else{
				$timeout(function () {
					$scope.errorMsg = "Password does not match.";
				}, 0);
				$timeout(function () {
					$scope.errorMsg = false;
				}, 5000);
			}
		}else{
			$timeout(function () {
				$scope.errorMsg = "Please filled mandatory fields.";
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 5000);
		}
	}
}]);

/* Thank You View & Login Controller */
app.controller('loginController', ['$scope', '$location', '$http', '$timeout', function($scope, $location, $http, $timeout) { 
	
}]);

/* Login Controller */
app.controller('loginController', ['$scope', '$location', '$http', '$state', '$localStorage', '$window', '$rootScope', '$timeout', 'userPostService', 'API_URL', function($scope, $location, $http, $state, $localStorage, $window, $rootScope, $timeout,userPostService, API_URL) {
	/* Set Default values */
	$scope.formData = {};
	$scope.formData.email = 'test@gmail.com';
	$scope.formData.password = 'test@123';

	$scope.errorMsg = false;
	$scope.successMsg = false;

	/* Function For IS EMAIL VERIFIED */
	$scope.is_email_verified = function(){
		$scope.formData.email = $location.search().email;
		$scope.formData.token = $location.search().token;

		if($scope.formData.email != undefined && $scope.formData.token != undefined){
			userPostService.postData(API_URL+'user/emailVerified', {'email': $scope.formData.email, 'token': $scope.formData.token}).then(function (response){

			},function(response) {
				console.log('albums retrieval failed.');
			});
		}
	}
	
	/* Function For LOGIN */
	$scope.login_button = function(){
		if($scope.loginForm.$valid){
			userPostService.postData(API_URL+'user/login', $scope.formData).then(function (response){
				$scope.errorMsg = '';
				$scope.successMsg = '';
				if(response.message === "success"){
					$timeout(function () {
						$scope.successMsg = response.success_message;
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;
						/* set id in localStorage */
						localStorage.setItem("id", response.id);
						/* set name in localStorage */
						localStorage.setItem("name", response.name);
						$(location).attr('href', '#/dashboard');
					}, 5000);
				}else if(response.message === "error"){
					$timeout(function () {
						$scope.errorMsg = response.error_message;
					}, 0);
					$timeout(function () {
						$scope.errorMsg = false;
					}, 5000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = "Please filled mandatory fields.";
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 5000);
		}
	}
}]);

/* User Dashboard Controller */
app.controller('dashboardCtrl', ['$scope', '$http', '$timeout', 'getService', function($scope, $http, $timeout, getService) {
	var name = localStorage.getItem("name");
	$scope.name = name;
}]);

/* Forgot Password Controller */
app.controller('forgotPasswordCtrl', ['$scope', '$location', '$http', '$timeout', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userPostService, API_URL) {
	/* Set Default values */
	$scope.errorMsg = false;
	$scope.successMsg = false;
	
	$scope.forgotPassword = function(){
		if($scope.forgotPasswordForm.$valid == true){
			userPostService.postData(API_URL+'user/forgetPassword', $scope.formData).then(function (response) {
				$scope.errorMsg = '';
				$scope.successMsg = '';
				if(response.message === 'success'){
					$timeout(function () {
						$scope.successMsg = response.success_message;
					}, 0);
				}else if(response.message === 'error'){
					$timeout(function () {
						$scope.errorMsg = response.error_message;
					}, 0);
					$timeout(function () {
						$scope.errorMsg = false;
					}, 4000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please enter an email address.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}	
	}
}]);

/* Reset Password Controller */
app.controller('resetPasswordCtrl', ['$scope', '$location', '$http', '$timeout', 'userPostService', 'API_URL', function($scope, $location, $http, $timeout, userPostService, API_URL) {
	/* Set Default values */
	$scope.formData = {};
	$scope.formData.email = $location.search().email;
	$scope.formData.token = $location.search().token;

	$scope.errorMsg = false;
	$scope.successMsg = false;
	
	$scope.reset_button = function(){
		if($scope.resetForm.$valid == true){
			if($scope.formData.cNewPassword == $scope.formData.password){
				userPostService.postData(API_URL+'user/passwordReset', $scope.formData).then(function (response) {
					$scope.errorMsg = '';
					$scope.successMsg = '';
					if(response.message === 'success'){
						$timeout(function () {
							$scope.successMsg = response.success_message;
						}, 0);
						$timeout(function () {
							$scope.successMsg = false;
							$(location).attr('href', '#/login');
						}, 5000);
					}else if(response.message === 'error'){
						$timeout(function () {
							$scope.errorMsg = response.error_message;
						}, 0);
						$timeout(function () {
							$scope.errorMsg = false;
							$(location).attr('href', '#/forgotpassword');
						}, 5000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});
			}else{
				$timeout(function () {
					$scope.errorMsg = 'Confirm password does not match.';
				}, 0);
				$timeout(function () {
					$scope.errorMsg = false;
				}, 4000);
			}
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please filled mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 4000);
		}
	}
}]);

/* Change Password Controller */
app.controller('changePasswordCtrl', ['$scope', '$http', '$location', '$timeout', 'userPostService', 'API_URL', function($scope, $http, $location, $timeout, userPostService, API_URL) {
	/* Set Default values */
	$scope.formData = {};
	$scope.formData.id = localStorage.getItem("id");
	
	$scope.passNotMatch = false;
	$scope.errorMsg = false;
	$scope.successMsg = false;
	
	$scope.change_password_button = function(){
		if($scope.changPasswordForm.$valid == true){
			if($scope.formData.cNewPassword == $scope.formData.newPassword){
				userPostService.postData(API_URL+'user/changePassword', $scope.formData).then(function (response) {
					$scope.errorMsg = '';
					$scope.successMsg = '';
					if(response.message === 'success'){
						$timeout(function () {
							$scope.successMsg = response.success_message;
						}, 0);
						$timeout(function () {
							$scope.successMsg = false;
							$(location).attr('href', '#/dashboard');
						}, 4000);
					}else if(response.message === 'error'){
						$timeout(function () {
							$scope.errorMsg = response.error_message;
						}, 0);
						$timeout(function () {
							$scope.errorMsg = false;
						}, 4000);
					}
				},function(response) {
					console.log('albums retrieval failed.');
				});
			}else{
				$timeout(function () {
					$scope.errorMsg = 'Confirn password does not match.';
				}, 0);
				$timeout(function () {
					$scope.errorMsg = false;
				}, 5000);
			}
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please filled mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 5000);
		}	
	}
}]);

/* My Account -> Profile Controller -> View Profile */
app.controller('profile', ['$scope', '$http', 'userPostService', 'API_URL', function($scope, $http, userPostService, API_URL) {
	$scope.profileData = {};
	$scope.profileDataImg = {};
	var id = localStorage.getItem("id");
	userPostService.postData(API_URL+'user/userDetails', {'id':id}).then(function (response){
		$scope.profileData = response;
		$scope.profileDataImg.image = response.image;
	},function(response) {
		console.log('albums retrieval failed.');
	});
}]);

/* Update Profile Controller */
app.controller('updateProfile', ['$scope', '$location', '$http', '$timeout', 'postService', 'API_URL', function($scope, $location, $http, $timeout, postService, API_URL) { 
	/* Set default value */
	$scope.profileData = {};
	$scope.profileDataImg = {};
	var id = localStorage.getItem("id");
	
	/* get DB data for edit */
	postService.postData(API_URL+'user/editDetails', {'id':id}).then(function (response) {
		if(response.message === 'success'){
			$scope.profileData = response;
			$scope.profileDataImg.image = response.image;
		}
	},function(response) {
		console.log('albums retrieval failed.');
	});
	
	/* Update the login user profile */
	$scope.errorMsg = false;
	$scope.successMsg = false;
	
	$scope.updateProfile = function(){
		if($scope.editProfileForm.$valid == true){
			$scope.profileData.id = localStorage.getItem("id");
			postService.postData(API_URL+'user/updateDetails', $scope.profileData).then(function (response) {
				if(response.message === 'success'){
					$timeout(function () {
						$scope.successMsg = response.success_message;
					}, 0);
					$timeout(function () {
						$(location).attr('href', '#/profile');
						$scope.successMsg = false;
					}, 4000);
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.errorMsg = 'Please filled mandatory fields.';
			}, 0);
			$timeout(function () {
				$scope.errorMsg = false;
			}, 5000);
		}
	}
}]);

/* Loguout Controller*/
app.controller('logout', ['$scope', '$http', '$location', '$timeout', function($scope, $http, $location, $timeout) {
	$scope.logout = function(){
		localStorage.clear();
		$(location).attr('href', '#/login');
	}
}]);

/* Layout Controller & Home Page */
app.controller('layout', ['$scope', '$http', '$location', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $location, $timeout, userGetService, userPostService, API_URL) {
	
	/* Service use for menu management and show data in header */
	userGetService.getData(API_URL+'admin/menus').then(function (response) {
		$scope.res  = response.data;
	},function(response) {
		console.log('albums retrieval failed.');
	});
	
	/* Service use for checking active & inactive status of login user on every state change */
	var id = localStorage.getItem("id");
	$scope.inactive = false;
	userPostService.postData(API_URL+'admin/getStatus', {'id':id}).then(function (response){
		if(response.message === 'inactive'){
			$timeout(function () {
				$scope.inactive = 'You are disabled by admin please contact administrator.';
			}, 0);
			$timeout(function () {
				$scope.inactive = false;
				localStorage.clear();
				$(location).attr('href', '#/login');
			}, 5000);
		}
	},function(response) {
		console.log('albums retrieval failed.');
	});
}]);

/* Data Base credentials Error Controller */
app.controller('update_dbCtrl', ['$scope', '$http', '$location', '$timeout', 'userGetService', 'userPostService', 'API_URL', function($scope, $http, $location, $timeout, userGetService, userPostService, API_URL) {

	$scope.allFieldError = false;
	$scope.successMsg = false;
	$timeout(function () {
		$scope.bdError = true;
	}, 0);
	$timeout(function () {
		$scope.bdError = false;
	}, 6000);
	$scope.update = function(){
		if($scope.dbUpdateForm.$valid){
			userPostService.postData(API_URL+'checkAndSaveDBDetails.php', $scope.formData).then(function (response) {
				if(response.message === 'success'){
					$timeout(function () {
						$scope.successMsg = true;
					}, 0);
					$timeout(function () {
						$scope.successMsg = false;
						$(location).attr('href', '#/login');
					}, 4000);		
				}else if(response.message === 'error'){
					$timeout(function () {
						$scope.bdError = true;
					}, 0);
					$timeout(function () {
						$scope.bdError = false;
					}, 4000);		
				}
			},function(response) {
				console.log('albums retrieval failed.');
			});
		}else{
			$timeout(function () {
				$scope.allFieldError = true;
			}, 0);
			$timeout(function () {
				$scope.allFieldError = false;
			}, 4000);	
		}	
	}
}]);