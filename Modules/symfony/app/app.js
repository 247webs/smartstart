'use strict';
var app = angular.module('app', ['ui.router', 'ngStorage','number']);
angular.module('app').constant('API_URL', "http://localhost/symfony/api/web/");
app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider.state('/', {
            url: '/',
			controller: 'layout',
            templateUrl: 'view/user/home.html',
        })

		.state('login', {
            url: '/login',
			controller: 'loginController',
            templateUrl: 'view/user/login.html',
        })
		.state('admin', {
            url: '/admin',
			controller: 'adminCtrl',
            templateUrl: 'view/user/login.html',
        })
		.state('admin_dashboard', {
            url: '/admin/dashboard',
			controller: 'adminCtrl',
            templateUrl: 'view/admin/adminDashboard.html',
        })
		.state('cms_management', {
            url: '/admin/cms_management',
			controller: 'cms_managementCtrl',
            templateUrl: 'view/admin/cms_management.html',
        })
		.state('pagelist', {
            url: '/admin/pagelist',
			controller : 'pagelistCtrl',	
            templateUrl: 'view/admin/pagelist.html',
        })
		.state('create_menu', {
            url: '/admin/create_menu',
			controller: 'menu_managementCtrl',
            templateUrl: 'view/admin/create_menu.html',
        })

		.state('menu_list', {
            url: '/admin/menu_management',
			controller: 'menu_managementCtrl',
            templateUrl: 'view/admin/menu_list.html',
        })
		
		.state('email_template_managmenet', {
            url: '/admin/email_template_management',
			controller: 'email_templateController',
            templateUrl: 'view/admin/email_template.html',
        })
		
		  .state('register', {
            url: '/register',
			controller: 'registerCtrl',
            templateUrl: 'view/user/register.html',
        })
		
		.state('thankYou', {
            url: '/thankYou',
			controller: 'thankYouController',
            templateUrl: 'view/user/thankyouView.html',
        })

		 .state('dashboard', {
            url: '/dashboard',
			controller : 'dashboardCtrl',
            templateUrl: 'view/dashboard/dashboard.html',
        })
		
		.state('profile', {
            url: '/profile',
			controller: 'profile',
            templateUrl: 'view/dashboard/myaccount.html',
        })
		
		.state('editprofile', {
            url: '/editprofile',
			controller : 'updateProfile',
            templateUrl: 'view/dashboard/editprofile.html',
        })
		
		.state('changePassword', {
            url: '/changePassword',
			controller : 'changePasswordCtrl',	
            templateUrl: 'view/dashboard/changePassword.html',
        })
		
		.state('forgotpassword', {
            url: '/forgotpassword',
			controller : 'forgotPasswordCtrl',	
            templateUrl: 'view/user/forgotPassword.html',
        })
		
		.state('resetpassword', {
            url: '/resetpassword',
			controller : 'resetPasswordCtrl',	
            templateUrl: 'view/user/resetpassword.html',
        })
		
		.state('static', {
            url: '/static',
			controller : 'staticCtrl',	
            templateUrl: 'view/admin/static.html',
			parms:{
				page:''
			}
        })
		.state('update_db', {
            url: '/update_db',
			controller : 'update_dbCtrl',	
            templateUrl: 'view/user/update_db.html',
        })
		
    $urlRouterProvider.otherwise('/');
});

/* --------------------------------------------- */
app.factory('userPostService', function($http, $q) {
	return {
		postData: function(API_URL, formData) { 
			var deferred = $q.defer();
				$.ajax({
					type: 'POST',
					url: API_URL,
					dataType: "json",	
					data: formData,
					success: function(res) {
						deferred.resolve(res);
					}, 
					error : function(err) {
						 
						if(err.responseText==="db_error"){
							$(location).attr('href', '#/update_db');
						}
					}
				});
			return deferred.promise;
		}
	};
});

app.factory('userGetService', function($http, $q) {
	return {
		getData: function(API_URL) {
			var deferred = $q.defer();
				$.ajax({
					type: 'GET',
					url: API_URL,
					dataType: "json",	
					success: function(res) {
					deferred.resolve(res);
					}, 
					error : function(msg, code) {
					deferred.reject(msg);
				}
			});
			return deferred.promise;
		}
	};
});

/* --------------------------------------------- */
app.factory('postService', function($http, $q) {
	return {
		postData: function(API_URL, formData) {
			var deferred = $q.defer();
			$.ajax({
				type: 'POST',
				url: API_URL,
				dataType: "json",	
				data: formData,
				success: function(res) {
					deferred.resolve(res);
				}, error : function(msg, code) {
					deferred.reject(msg);
				}
			});
			return deferred.promise;
		}
	};
});

app.factory('getService', function($http, $q) {
	return {
		getData: function(API_URL) {
			var deferred = $q.defer();
			$.ajax({
				type: 'GET',
				url: API_URL,
				dataType: "json",	
				success: function(res) {
					deferred.resolve(res);
				}, error : function(msg, code) {
					deferred.reject(msg);
				}
			});
			return deferred.promise;
		}
	};
});
/**---------------DIRECTIVE-FOR-NUMBER--------------------------------**/
app.directive('datepicker', function() {
  return {
    require: 'ngModel',
    link: function(scope, el, attr, ngModel) {
      $(el).datepicker({
        onSelect: function(dateText) {
          scope.$apply(function() {
            ngModel.$setViewValue(dateText);
          });
        }
      });
    }
  };
});
/**---------------DIRECTIVE-FOR-NUMBER--------------------------------**/
angular.module('number', [])
    .directive('number', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$parsers.push(function (input) {
                    if (input == undefined) return ''
                    var inputNumber = input.toString().replace(/[^0-9]/g, '');
                    if (inputNumber != input) {
                        ctrl.$setViewValue(inputNumber);
                        ctrl.$render();
                    }
                    return inputNumber;
                });
            }
        };
    });
/**-------------UPLOAD-PROFILE-IMAGE-DIRECTIVE--------------------------------**/
	app.directive('myDirective', function (httpPostFactory) { 

    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {

            element.bind('change', function () {
                var formData = new FormData();	
                formData.append('file', element[0].files[0]);
                formData.append('uid', localStorage.getItem("id"));
                httpPostFactory('http://upworkdevelopers.com/test/prosmy/api/web/dashboard/update_user_details', formData, function (callback) {
                });
            });

        }
    };
});

app.factory('httpPostFactory', function ($http) {
    return function (file, data, callback) {
        $http({
            url: file,
            method: "POST",
            data: data,
            headers: {'Content-Type': undefined}
        }).success(function (response) {
            callback(response);
			
        });
    };
});
