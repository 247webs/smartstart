<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="page_contant_manager")
 * @ORM\Entity
 */
class CMS
{
    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $page_name;

	
	/**
     * @ORM\Column(type="string", length=500)
     */
	protected $page_contanct;
	

	/**
     * @ORM\Column(type="string", length=50)
     */
	protected $created_time;
	
	
	public function setPage_name($page_name){
        $this->page_name = $page_name;
    }
	
	public function getPage_name(){
        return $this->page_name;
    }
	
    public function eraseCredentials(){
        return null;
    }

    public function getId(){
        return $this->id;
    }

    public function setPage_contanct($page_contanct){
        $this->page_contanct = $page_contanct;
    }

    public function getPage_contanct() {
        return $this->page_contanct;
    }


    public function getCreated_time(){
        return $this->created_time;
    }

    public function setCreated_time($created_time){
        $this->created_time = $created_time;
    }
  
	
    public function getSalt(){
        return null;
    }
}