<?php
namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('email', EmailType::class)
            ->add('username', TextType::class)
			->add('phone', TextType::class, array(
				'constraints' => array(
					new NotBlank(),
					new Length(array('min' => 10)),
				),
			))
			->add('dateOfBirth', TextType::class, array(
				'constraints' => array(
					new NotBlank(),	
				),
			))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
        ->add('gender',ChoiceType::class,
            array('choices' => array(
                'Male' => 'male',
                'Female' => 'female'),
				'choices_as_values' => true,'multiple'=>false,'expanded'=>true
			))
		->add('country', ChoiceType::class, array(
			'choices' => array(
				'Afghanistan' => 'Afghanistan',
				'Albania' => 'Albania',
				'Algeria'   => 'Algeria',
				'India' => 'IN'
			),
			'preferred_choices' => array('Algeria', 'IN')
		));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}