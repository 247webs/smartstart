<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DashboardController extends Controller {
	
	public function change_passwordAction(Request $request){
		$userId = $request->request->get('id');
		$old_password = $request->request->get('old_password');
		$new_password = $request->request->get('new_password');
		$confirm_password = $request->request->get('confirm_password');
		
		
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users where users.id = '$userId'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
			if(!empty($result)){
			$res  = $result['0'] ? $result['0'] : $result;
			$password = $res['password'];
			$email = $res['email'];
			$user_name = $res['name'];
			if($old_password == $password){
				$em = $this->getDoctrine()->getManager();
				$RAW_QUERY ="UPDATE users SET `password`='$new_password' WHERE `id`='$userId'";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
			
				$RAW_QUERY = "SELECT * FROM `email_templates` WHERE template_title = 'Change Password'";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
				$result_email = $statement->fetchAll();
				
				if(!empty($result_email)){
					$result  = $result_email['0'] ? $result_email['0'] : $result_email;
					$template = $result['content'];
					$templateRes = sprintf($template,$user_name);
				}
				
				$to = $email;
				$subject = "Change Password";
				$txt = $templateRes;
				$headers = "From: rdwivedi183@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";
				mail($to,$subject,$txt,$headers);
				
				$data = array('status'=>'200','message'=>'success', 'data'=> $result);
				return new JsonResponse($data);
			}else{
				$data = array('status'=>'400','message'=>'failed');
				return new JsonResponse($data);
			}
		}
		
	 }
		
	public function user_detailsAction(Request $request){
		$userId = $request->request->get('id');
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users where id='$userId'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		
		$data = array('status'=>'200','message'=>'success', 'data'=> $result);
		return new JsonResponse($data);
    }

	public function update_user_detailsAction(Request $request){
		$type = $_FILES[file][type];
		$imageType = explode("/",$type);
		$id = $request->request->get('uid');
		$userId = $request->request->get('id');
		if(!empty($userId)){
			$username = $request->request->get('name');
			$date_of_birth = $request->request->get('date_of_birth');
			$newDate = date("Y-m-d", strtotime($date_of_birth));
			$phone = $request->request->get('phone');
			$email = $request->request->get('email');
			$gender = $request->request->get('gender');
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY ="UPDATE users SET `email`='$email',`name`='$username',`gender`='$gender',`date_of_birth`='$newDate',`phone`='$phone' WHERE `id`='$userId'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$userData = array('status'=>'400','message'=>'success');
			return new JsonResponse($userData);
		}else if(!empty($id)){
			if($imageType[0] == 'image'){
				
				$file = $request->files->get('file');
				$filename = $file->guessExtension ();
				$new = time();
				$original_name = $new.$file->getClientOriginalName();
				$path = 'upload';

				$file->move($path,$original_name);
				$dbPath = $path.'/'.$original_name;

				$em = $this->getDoctrine()->getManager();
				$RAW_QUERY = "UPDATE users SET `image`='$dbPath' WHERE `id`='$id'";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
				$userData = array('status'=>'200','message'=>'success');
				return new JsonResponse($userData);
			}else{
				$userData = array('status'=>'400','message'=>'file type');
				return new JsonResponse($userData);
			}
			
		}
		
	}
	 
	 
	 public function getStatusAction(Request $request){	
		$userId = $request->request->get('id');
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users WHERE id ='$userId'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		if(!empty($result)){
			$res  = $result['0'] ? $result['0'] : $result;
			$status = $res['status'];
			$name = $res['name'];
			$data = array('status'=>'200','message'=>'success', 'data'=> $status, 'name' =>$name);
			return new JsonResponse($data);
		}else{
			$data = array('status'=>'200','message'=>'faild');
			return new JsonResponse($data);
		}
	 }
	
}
