<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends Controller {

	/*Sign-up Strat*/
	public function signupAction(Request $request) {
		$user_name = addslashes($request->request->get('name'));
		$email = $request->request->get('email');
		$password = $request->request->get('password');
		$token = substr(md5(uniqid(rand(1,6))), 0, 25);
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users WHERE email ='$email'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		
		if(empty($result)) {
			/**msg email template**/
			$em = $this->getDoctrine()->getManager();
			$RAW_QUERY = "SELECT * FROM email_templates WHERE template_title = 'Signup'";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$statement->execute();
			$result_email = $statement->fetchAll();	
			if(!empty($result_email)){
				$result  = $result_email['0'] ? $result_email['0'] : $result_email;
				$template = $result['content'];
				$templateRes = sprintf($template,$user_name);
			}
			$link = "<a href='http://upworkdevelopers.com/test/prosmy/app/#/login?&token=" .$token ."' target=
			'_blank'>Click here for login</a>";
			
			$to = $email;
			// print_r($to);die;
			$subject = "Confirm Your Registration";
			$txt = $templateRes.'<br>'.$link;
			$headers = "From: rdwivedi183@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";
			mail($to,$subject,$txt,$headers);
			
			$em = $this->getDoctrine()->getManager();
			$image_path = 'upload/user_logo.png';
			$RAW_QUERY = "INSERT INTO users(email, name, token, image, password) VALUES ('$email','$user_name', '$token', '$image_path', '$password')";
			$statement = $em->getConnection()->prepare($RAW_QUERY);
			$result = $statement->execute();
			if($result){
				$data = ['code' => 200, 'message' => 'success'];
				return new JsonResponse($data);
			}else{
				$data = ['code' => 400, 'message' => 'ErrorMessage'];
				return new JsonResponse($data);
			}
		}else{
			$data = ['code' => 400, 'message' => 'emailUnic'];
			return new JsonResponse($data);
		}
	}
	/*Sign-up End*/
	
	/*Login start*/
	public function loginAction(Request $request){
		
		$user_name = $request->request->get('email');
		$password = $request->request->get('password');
		$token =  $request->request->get('token');
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users where users.email = '$user_name' And users.password = '$password'";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		if(!empty($result)){
			$res  = $result['0'] ? $result['0'] : $result;
			$role = $res['role'];
			$status	 = $res['status'];
			$id = $res['id'];
			$db_token = $res['token'];
			$db_password = $res['password'];
			if($db_password == $password){
				
				if(!empty($db_token)){
					if($db_token == $token){
						$RAW_QUERY ="UPDATE users SET is_email_verified = '1', token = '' WHERE id = '$id'";
						$statement = $em->getConnection()->prepare($RAW_QUERY);
						$statement->execute();	
						$is_email_verified = $res['is_email_verified'];
						if($status == 1){
							$id = $res['id'];
							$userData = ['code' => '200', 'id' => $id,'message'=>'success',];
							return new JsonResponse($userData);
						}else{
							$userData = array('status'=>'400','message'=>'failed');
							return new JsonResponse($userData);
						}	
					}else{
						$userData = array('status'=>'400','message'=>'emailVerified');
						return new JsonResponse($userData);
					}
					
				}else{
					$is_email_verified = $res['is_email_verified'];
					if($is_email_verified == 0){
						$userData = array('status'=>'400','message'=>'emailVerified');
						return new JsonResponse($userData);
					}else{
						if($status == 1){
							$id = $res['id'];
							$userData = ['code' => '200', 'id' => $id,'message'=>'success',];
							return new JsonResponse($userData);
						}else{
							$userData = array('status'=>'400','message'=>'failed');
							return new JsonResponse($userData);
						}
					}
					if($is_email_verified == 1){
						if($status == 1){
							$id = $res['id'];
							$userData = ['code' => '200', 'id' => $id,'message'=>'success',];
							return new JsonResponse($userData);
						}else{
							$userData = array('status'=>'400','message'=>'failed');
							return new JsonResponse($userData);
						}
					}else{
						$userData = array('status'=>'400','message'=>'emailVerified');
						return new JsonResponse($userData);
					}	
				}
			}else{
				$userData = array('status'=>'400','message'=>'passNotMatch');
				return new JsonResponse($userData);
			}
		}else{
			$userData = array('status'=>'400','message'=>'emailNotMatch');
			return new JsonResponse($userData);
		}
        return $this->render('registration/login.html.twig');
	}	
	/*Login End*/
	
	/*forgot_password* start*/
	public function forgotPasswordAction(Request $request){
		
		$email = $request->request->get('email');
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users where users.email = '$email' ";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();

		if(!empty($result)){
			$res  = $result['0'] ? $result['0'] : $result;
			$email = $res['email'];
			$user_name = $res['name'];
			$is_email_verified = $res['is_email_verified'];
			if($is_email_verified == 1){
				$token = substr(md5(uniqid(rand(1,6))), 0, 25);
				$em = $this->getDoctrine()->getManager();
				$RAW_QUERY ="UPDATE users SET token='$token' WHERE email='$email'";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();	
				
				$enc_email = base64_encode($email);
				
				$em = $this->getDoctrine()->getManager();
				$RAW_QUERY = "SELECT * FROM email_templates WHERE template_title = 'forgot password'";
				$statement = $em->getConnection()->prepare($RAW_QUERY);
				$statement->execute();
				$result_email = $statement->fetchAll();
				if(!empty($result_email)){
					$result  = $result_email['0'] ? $result_email['0'] : $result_email;
					$template = $result['content'];
					$templateRes = sprintf($template,$user_name);
				}
				
				$link = "<a href='http://upworkdevelopers.com/test/prosmy/app/#/resetpassword?email=" .$enc_email .'&token='.$token."' target='_blank'>Click here for reset password</a>";
				
				$to = $email;
				$subject = "Forgot Password";
				$txt = $templateRes.'<br>'.$link;
				$headers = "From: rdwivedi183@gmail.com" . "\r\n" ."CC: somebodyelse@example.com";
				mail($to,$subject,$txt,$headers);
				
				$data = ['code' => '200', 'message'=>'success', 'template'=>$templateRes];
				return new JsonResponse($data);
			}else{
				$data = ['code' => 200, 'message' => 'emailVerified'];
				return new JsonResponse($data);
			}
		}else{
			$data = ['code' => '400', 'message'=>'failed'];
			return new JsonResponse($data);
		}
	}
	/*forgot_password End*/ 

	/*password_reset Start*/ 
	public function passwordResetAction(Request $request){
		
		$email = $request->request->get('email');
		$dec_email =  base64_decode($email);
		
		$token = $request->request->get('token');
		$password = $request->request->get('password');
	
		$em = $this->getDoctrine()->getManager();
		$RAW_QUERY = "SELECT * FROM users where users.email = '$dec_email' ";
		$statement = $em->getConnection()->prepare($RAW_QUERY);
		$statement->execute();
		$result = $statement->fetchAll();
		if(!empty($result)){
			$res  = $result['0'] ? $result['0'] : $result;
			$email = $res['email'];
			$db_token = $res['token'];
			$is_email_verified = $res['is_email_verified'];
			if($is_email_verified == 1){
				if($token == $db_token){
					$em = $this->getDoctrine()->getManager();
					$RAW_QUERY ="UPDATE users SET password='$password',`token`='' WHERE email='$email'";
					$statement = $em->getConnection()->prepare($RAW_QUERY);
					$statement->execute();	
					
					$em = $this->getDoctrine()->getManager();
					$RAW_QUERY = "SELECT * FROM email_templates WHERE template_title = 'change password'";
					$statement = $em->getConnection()->prepare($RAW_QUERY);
					$statement->execute();
					$result_email = $statement->fetchAll();
					if(!empty($result_email)){
						$result  = $result_email['0'] ? $result_email['0'] : $result_email;
						$template = $result['content'];
					}
					$data = ['code' => 200, 'message' => 'success', 'template' => $template];
					return new JsonResponse($data);
				}else{
					$data = ['code' => 200, 'message' => 'failed'];
					return new JsonResponse($data);
				}
			}else{
				$data = ['code' => 200, 'message' => 'emailVerified'];
				return new JsonResponse($data);
			}
			
		}else{
			$data = ['code' => 400, 'message' => 'failed'];
			return new JsonResponse($data);
		}
	}
	/*password_reset End*/
	
	 public function update_dbAction(Request $request){
		error_reporting(0);
		$path = '../api/config/parameters.yml';
		$host_name = $request->request->get('host_name');
		$data_base_name = $request->request->get('data_base_name');
		$user_name = $request->request->get('user_name');
		$password = $request->request->get('password');
		
		$connection = mysqli_connect($host_name, $user_name, $password, $data_base_name);
		if($connection){
			touch($path);
			$file = fopen($path,"w");	
$database_details = "# This file is auto-generated during the composer install 
parameters:
    database_host: ".$host_name."
    database_port: 3306
    database_name: ".$data_base_name."
    database_user: ".$user_name."
    database_password: ".$password."
    mailer_transport: Swift_SmtpTransport
    mailer_host: smtp.sendgrid.net
    port: 587
    mailer_user: super_dev
    mailer_password: Any@12345
    secret: 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";

			fwrite($file,$database_details);
			fclose($file);
			$data = ['code' => 200, 'message' => 'success'];
			return new JsonResponse($data);
		}else{
			echo'else';
		}
		mysqli_close($connection);
		die;
	}
}