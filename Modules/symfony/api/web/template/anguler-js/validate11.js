$(document).ready(function(){
	// Wait for the DOM to be ready
	$("#frm").validate({
		// Specify validation rules
		rules:{
			citizenship:{
				required:true
			},
			email:{
				required:true
			},
			confirm_email:{
				required:true,
				equalTo:"#email"
			},
			secret_word:{
				required:true,
				/*atLeastOneLowercaseLetter: true,
				atLeastOneUppercaseLetter: true,
				atLeastOneNumber: true,
				atLeastOneSymbol: true,
				maxlength: 40,*/
				minlength: 8
			},
			confirm_secret_word:{
				required:true,
				/*atLeastOneLowercaseLetter: true,
				atLeastOneUppercaseLetter: true,
				atLeastOneNumber: true,
				atLeastOneSymbol: true,
				maxlength: 40,*/
				minlength: 8,
				equalTo:"#secret_word"
			},
			about_us:{
				required:true
			},
			country_of_birth:{
				required:true
			},
			country_of_citizenship:{
				required:true
			},
			country_of_resident:{
				required:true
			},
		},
		messages : {
		},
		/*errorPlacement: function(){
			return false;
		},*/
		/*submitHandler: function(form) {
			/*alert('form submitted');
			location.href = 'personal_information.php';
			return false;
		}*/
	});


	
	
	
$('#frm1').validate({
		rules:{
			title:{
				required:true
			},
			fnm:{
				required:true
			},
			/*mnm:{
				required:true
			},*/
			lnm:{
				required:true
			},
			ssn_code:{
				required:true
			},
			confirm_ssn_code:{
				required:true,
				equalTo:"#ssn_code"
			},
			dob:{
				required:true
			},
			gender:{
				required:true
			},
			status:{
				required:true
			},
			/*no_of_dependent:{
				required:true,
				number:true
			},*/
			gid_no:{
				required:true
			},
			address1:{
				required:true
			},
			/*address2:{
				required:true
			},*/
			city:{
				required:true
			},
			country:{
				required:true
			},
			zip_code:{
				required:true,
				number:true
			},
			state:{
				required:true
			},
			//chk1:{
				//required:true
			//},
			chk_mail_address:{
				required:true
			},
			chk_city:{
				required:true
			},
			chk_country:{
				required:true
			},
			chk_zip_code:{
				required:true
			},
			chk_state:{
				required:true
			},
			phone1:{
				required:true,
				minlength:10,
				maxlength:10,
				number:true
			},
			/*phone2:{
				required:true,
				minlength:10,
				maxlength:10,
				number:true
			},*/
		},
		/*errorPlacement: function(){
			return false;
		},*/
		/*submitHandler: function(form) {
			/*alert('form submit');
			window.location.href = 'financial_information.php';
			return false;
		}*/
	});





$('#frm2').validate({
		rules:{
			employment_status:{
				required:true
			},
			income:{
				required:true
			},
			wealth:{
				required:true
			},
			annual_income:{
				required:true
			},
			liquid_assets:{
				required:true
			},
			net_worth:{
				required:true
			},
			text_bracket:{
				required: true
			},
			occupation: {
				required: true
			},
			year_employed:{
				required: true
			},
			company_name: {
				required: true
			},
			type_of_business: {
				required: true
			},
			business_address: {
				required: true
			},
			city: {
				required: true
			},
			country: {
				required: true
			},
			zip_code: {
				required: true
			},
			state: {
				required: true
			},
			business_phone: {
				required: true
			},
			occupation1: {
				required: true
			},
			year_employed1:{
				required: true
			},
			company_name1: {
				required: true
			},
			type_of_business1: {
				required: true
			},
			business_address1: {
				required: true
			},
			city1: {
				required: true
			},
			country1: {
				required: true
			},
			zip_code1: {
				required: true
			},
			state1: {
				required: true
			},
			business_phone1: {
				required: true
			},
			school_name:{
				required:true
			},
			graduation_year:{
				required:true
			},
			other_income: {
				required: true
			},
			sold_business_name : {
				required : true
			},
			sale_amount : {
				required : true
			},
			date_sold : {
				required : true
			},
			previous_employer : {
				required : true
			},
			occupation_unemployed : {
				required : true
			},
			years_employed : {
				required : true
			},
			last_date : {
				required : true
			},
		},
		/*errorPlacement: function(){
			return false;
		},*/
		/*submitHandler: function(form) {
			/*alert('form submit');
			window.location.href = 'affiliation_information.php';
			return false;
		}*/
	});
	$('#myform').validate({
    rules: {
        para1: {         
			required: true
			},
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function(form) {
			alert('form submit');
			//window.location.href = 'demo4.html';
			return false;
																			}
	});
	$('#myform1').validate({
    rules: {
        select12: {         
			required: true
			},
		},
		errorPlacement: function(){
			return false;
		},
		submitHandler: function(form) {
			/*alert('form submit');*/
			window.location.href = 'demo5.html';
			return false;
		}
	});
	
	$('#myform2').validate({
    rules: {
        your_secret_word:{
			required:true,
			minlength:8,
			},
		email_id:{            
			required:true
			},
		},
		errorPlacement: function(){
			return false;
		},		
		submitHandler: function(form) {
		/*alert('form submit');*/
		window.location.href = 'continue.php';
			return false;
		}
	});
	
	$('#forget-password').validate({
    rules: {
		forget_email:{            
			required:true
			},
		},
		errorPlacement: function(){
			return false;
		},		
		submitHandler: function(form) {
		/*alert('form submit');*/
		window.location.href = '';
			return false;
		}
	});
	
	$('#affiliationform').validate({
    rules: {
		comname:{            
			required:true
			},
		comperson:{
			required:true
			},
		position:{
			required:true
			},
		comname1:{            
			required:true
			},
		comperson1:{
			required:true
			},
		position1:{
			required:true
			},
		agent:{
			required:true
			},
		affiliation_name:{
			required:true
			},
		affiliation_organisation:{
			required:true
			},
		},
		/*errorPlacement: function(){
			return false;
		},*/		
		/*submitHandler: function(form) {
		/*alert('form submit');
		window.location.href = 'invest_profile.php';
			return false;
		}*/
	});
	
	$('#profile_invest').validate({
    rules: {
		exp1:{            
			required:true
			},
		exp2:{
			required:true
			},
		},
		/*errorPlacement: function(){
			return false;
		},*/		
		submitHandler: function(form) {
		/*alert('form submit');*/
		window.location.href = 'invest_experience.php';
			return false;
		}
	});
	
	$('#jowner').validate({
    rules: {
		title:{            
			required:true
			},
		firstname:{
			required:true
			},
		middlename:{            
			required:true
			},
		lastname:{
			required:true
			},
		dob:{            
			required:true
			},
		citizenship_status:{
			required:true
			},
		country_of_birth:{            
			required:true
			},
		code1:{
			required:true
			},
		code2:{            
			required:true,
			equalTo:'#code1'
			},
		address1:{
			required:true
			},
		city:{            
			required:true
			},
		country:{
			required:true
			},
		zip_code:{            
			required:true
			},
		state:{
			required:true
			},
		phone1:{            
			required:true
			},
		employment_status:{
			required:true
			},
		},
		/*errorPlacement: function(){
			return false;
		},*/		
		/*submitHandler: function(form) {
		/*alert('form submit');
		window.location.href = 'invest_profile.php';
			return false;
		}*/
	});
	
	$('#agreementform').validate({
    rules: {
		agree_chk_1:{            
			required:true
			},
		agree_chk_2:{
			required:true
			},
		},
		/*errorPlacement: function(){
			return false;
		},*/		
		submitHandler: function(form) {
		/*alert('form submit');*/
		window.location.href = 'agreement_1.php';
			return false;
		}
	});
	
	$('#agreementform1').validate({
    rules: {
		chk_1:{            
			required:true
			},
		chk_2:{
			required:true
			},
		chk_3:{            
			required:true
			},
		chk_4:{
			required:true
			},
		},
		/*errorPlacement: function(){
			return false;
		},*/		
		submitHandler: function(form) {
		/*alert('form submit');*/
		window.location.href = 'review.php';
			return false;
		}
	});
	
	$('#custodialminor').validate({
    rules: {
		fnm:{            
			required:true
			},
		mnm:{
			required:true
			},
		lnm:{            
			required:true
			},
		ssn_code_1:{
			required:true
			},
		dob:{
			required:true
			},
		},
		/*errorPlacement: function(){
			return false;
		},*/		
		/*submitHandler: function(form) {
		/*alert('form submit');
		window.location.href = 'review.php';
			return false;
		}*/
	});
});





